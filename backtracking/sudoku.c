#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define DIMENSION 9
#define NCELLS DIMENSION*DIMENSION
#define MAXCANDIDATES DIMENSION+1

typedef struct {
	int x, y;
} point;

typedef struct {
	int n[DIMENSION + 1][DIMENSION + 1];
	int freecount;
	point move[NCELLS + 1];
} board;

int finished = 0;
int shuffled = 0;

void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void shuffle(int* items, int n, int rounds) {
	int i1, i2;
	for (int i = 0; i < rounds; i++) {
		i1 = rand() % n;
		i2 = rand() % n;
		swap(&items[i1], &items[i2]);
	}
}

void print(int* items, int start, int n) {
	for (int i = start; i < n; i++) {
		printf("%d ", items[i]);
	}
	printf("\n");
}

board* create_board() {
	board* b = (board *)malloc(sizeof(board));
	for (int i = 1; i <= DIMENSION; i++) {
		for (int j = 1; j <= DIMENSION; j++) {
			b->n[i][j] = 0;
		}
	}

	b->freecount = NCELLS;

	for (int i = 1; i <= NCELLS; i++) {
		b->move[i].x = -1;
		b->move[i].y = -1;
	}
	return b;
}

void free_board(board* b) {
	free(b);
}

void insert_board(board* b, int x, int y, int val) {
	b->n[x][y] = val;
	b->freecount--;
}

void print_board(board* b) {
	for (int i = 1; i <= DIMENSION; i++) {
		for (int j = 1; j <= DIMENSION; j++) {
			printf("%d ", b->n[i][j]);
		}
		printf("\n");
	}
}

void next_square(point* p, board* b) {
	for (int i = 1; i <= DIMENSION; i++) {
		for (int j = 1; j <= DIMENSION; j++) {
			// First choice
			if (b->n[i][j] == 0) {
				p->x = i;
				p->y = j;
				return;
			}
		}
	}
}

void get_sector_index(int x, int y, int* result_x, int* result_y) {
	int i = 0;

	*result_x = -1;
	while (i < x) {
		(*result_x)++;
		i += 3;
	}

	i = 0;
	*result_y = -1;
	while (i < y) {
		(*result_y)++;
		i += 3;
	}
}

int possible_values(point p, board* b, int possible[]) {
	// check row, column, sector
	for (int i = 1; i <= DIMENSION; i++) {
		// row check
		if (b->n[p.x][i] != 0) {
			possible[b->n[p.x][i]] = 0;
		}

		// column check
		if (b->n[i][p.y] != 0) {
			possible[b->n[i][p.y]] = 0;
		}
	}

	int sector_x, sector_y;
	get_sector_index(p.x, p.y, &sector_x, &sector_y);

	// check sector
	for (int i = 1; i <= 3; i++) {
		for (int j = 1; j <= 3; j++) {
			if (b->n[sector_x * 3 + i][sector_y * 3 + j] != 0) {
				possible[b->n[sector_x * 3 + i][sector_y * 3 + j]] = 0;
			}
		}
	}
}

void construct_candidates(int c[], int* nc, int a[], int n, int k, board* b) {
	int i;
	int possible[DIMENSION + 1];
	for (i = 1; i <= DIMENSION; i++) {
		possible[i] = 1;
	}

	next_square(&(b->move[k]), b); // set x, y of cell to fill

	*nc = 0;
	if ((b->move[k].x < 0) && (b->move[k].y < 0)) {
		return;
	}

	possible_values(b->move[k], b, possible); // get possible values for cell

	for (i = 1; i <= DIMENSION; i++) {
		if (possible[i]) {
			c[*nc] = i;
			*nc = *nc + 1;
		}
	}
	if (!shuffled) {
		shuffle(c, *nc, rand() % 100);
		shuffled = 1;
	}
}

int is_solution(int a[], int n, int k, board *b, int *step) {
	(*step)++;
	return b->freecount == 0;
}

void make_move(point p, int x, board* b) {
	//printf("Make move %d %d: %d\n", p.x, p.y, x);
	b->n[p.x][p.y] = x;
	b->freecount--;
}

void unmake_move(point p, board* b) {
	b->n[p.x][p.y] = 0;
	b->freecount++;
}

void backtracking(int a[], int n, int k, board *b, int *step) {
	int c[DIMENSION+1];
	int nc;
	int i;

	if (is_solution(a, n, k, b, step)) {
		printf("Solution found\n");
		finished = 1;
		printf("Solved board:\n");
		print_board(b);
	} else {
		k++;
		construct_candidates(c, &nc, a, n, k, b);
		for (i = 0; i < nc; i++) {
			a[k] = c[i];
			make_move(b->move[k], a[k], b);
			backtracking(a, n, k, b, step);
			unmake_move(b->move[k], b);
			if (finished) return;
		}
	}
}

int main(int argc, char **argv) {
	board* b = create_board();

	// easy board
	//insert_board(b, 1, 2, 6);
	//insert_board(b, 1, 4, 4);
	//insert_board(b, 1, 5, 5);
	//insert_board(b, 1, 7, 2);
	//insert_board(b, 1, 8, 1);
	//insert_board(b, 1, 9, 3);

	//insert_board(b, 2, 1, 4);
	//insert_board(b, 2, 3, 8);
	//insert_board(b, 2, 4, 7);
	//insert_board(b, 2, 8, 9);
	//
	//insert_board(b, 3, 2, 5);
	//insert_board(b, 3, 3, 1);
	//insert_board(b, 3, 4, 3);

	//insert_board(b, 4, 1, 1);
	//insert_board(b, 4, 3, 4);
	//insert_board(b, 4, 6, 6);
	//insert_board(b, 4, 9, 8);

	//insert_board(b, 5, 2, 8);
	//insert_board(b, 5, 6, 7);
	//insert_board(b, 5, 7, 9);
	//insert_board(b, 5, 8, 4);

	//insert_board(b, 6, 1, 6);
	//insert_board(b, 6, 4, 5);
	//insert_board(b, 6, 5, 8);
	//insert_board(b, 6, 6, 4);

	//insert_board(b, 7, 1, 7);
	//insert_board(b, 7, 3, 5);
	//insert_board(b, 7, 5, 2);
	//insert_board(b, 7, 6, 1);

	//insert_board(b, 8, 2, 2);
	//insert_board(b, 8, 3, 9);
	//insert_board(b, 8, 5, 4);
	//insert_board(b, 8, 6, 5);
	//insert_board(b, 8, 7, 8);

	//insert_board(b, 9, 4, 9);
	//insert_board(b, 9, 7, 4);
	//insert_board(b, 9, 8, 5);
	//insert_board(b, 9, 9, 2);

	// hard board
	//insert_board(b, 1, 8, 1);
	//insert_board(b, 1, 9, 2);

	//insert_board(b, 2, 5, 3);
	//insert_board(b, 2, 6, 5);

	//insert_board(b, 3, 4, 6);
	//insert_board(b, 3, 8, 7);

	//insert_board(b, 4, 1, 7);
	//insert_board(b, 4, 7, 3);

	//insert_board(b, 5, 4, 4);
	//insert_board(b, 5, 7, 8);

	//insert_board(b, 6, 1, 1);

	//insert_board(b, 7, 4, 1);
	//insert_board(b, 7, 5, 2);

	//insert_board(b, 8, 2, 8);
	//insert_board(b, 8, 8, 4);

	//insert_board(b, 9, 2, 5);
	//insert_board(b, 9, 7, 6);
	//

	printf("Board:\n");
	print_board(b);

	int a[NCELLS + 1];
	int n = NCELLS;
	int step = 0;

	if (argc == 2) {
		int seed = atoi(argv[1]);
		printf("Seed: %d\n", seed);
		srand(seed);
	}
	else {
		srand(69420);
	}


	backtracking(a, n, 0, b, &step);

	printf("Step count: %d\n", step);

	free_board(b);

	return 0;
}

