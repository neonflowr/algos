#include <stdio.h>
#include <assert.h>

#define MAX 10

void construct_candidates(int c[], int* nc, int a[], int n, int k) {
	int perms[n + 1];
	int i;
	*nc = 0;

	for (i = 1; i <= n; i++) {
		perms[i] = 0;
	}

	for (i = 1; i <= k; i++) {
		perms[a[i]] = 1;
	}

	for (i = 1; i <= n; i++) {
		if (perms[i] == 0) {
			c[*nc] = i;
			(*nc)++;
		}
	}
}

int is_solution(int a[], int n, int k) {
	return k == n;
}

void print(int items[], int n) {
	for (int i = 1; i <= n; i++) {
		printf("%d ", items[i]);
	}
	printf("\n");
}

void backtracking(int a[], int n, int k, int* count) {
	int c[MAX + 1];
	int nc;
	int i;

	construct_candidates(c, &nc, a, n, k);

	if (is_solution(a, n, k)) {
		// process
		//printf("Perm: ");
		//print(a, n);
		(*count)++;
	}
	else {
		k++;

		for (i = 0; i < nc; i++) {
			a[k] = c[i];
			backtracking(a, n, k, count);
		}
	}
}

int factorial(int n) {
	if (n == 1) {
		return n;
	}
	return n * factorial(n - 1);
}

int main() {
	int a[MAX + 1] = { 0 };
	int n = MAX;
	int count = 0;
	backtracking(a, n, 0, &count);
	printf("Count: %d\n", count);
	assert(count == factorial(n));
	return 0;
}

