// Find all subsets of n items

#include <stdio.h>
#include <assert.h>
#include <math.h>
#define MAX 8

int construct_candidates(int c[], int *nc) {
	c[0] = 1;
	c[1] = 0;
	*nc = 2;
}

void print(int* items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

int is_solution(int a[], int n, int k) {
	return k == (n - 1);
}

void backtrack(int a[], int n, int k, int *count) {
	int c[2];
	int nc;
	int i;
	
	if (is_solution(a, n, k)) {
		// process
		printf("Subset: ");
		print(a, n);
		(*count)++;
	}
	else {
		k++;

		// prep next moves
		construct_candidates(c, &nc);

		// make move
		for (i = 0; i < nc; i++) {
			// extend partial solution
			a[k] = c[i];
			backtrack(a, n, k, count);
			// no need to unmake move since making a move overwrites the current move
		}
	}
}

int main() {
	int a[MAX] = {0};
	int n = MAX;
	int count = 0;
	backtrack(a, n, -1, &count);
	printf("Count: %d\n", count);
	assert(count == (1 << n));
	return 0;
}

