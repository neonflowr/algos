#include <stdio.h>

int main() {
	// int x = 0x4500 + 0x41 + 0xf2b5 + 0x8011 + 0x0000 + 0xc0a8 + 0x6613 + 0x808 + 0x808;
	int x = 0x4500 + 0x73 + 0x4000 + 0x4011 + 0x0000 + 0xc0a8 + 0x0001 + 0xc0a8 + 0x00c7;

	printf("Number before 1's complement: %x\n", x);

	int masked = x & 0xFFFF;
	printf("Masked: %x\n", masked);

	int carry_mask = 0xF0000;
	int carry = (x & carry_mask) >> 16;

	printf("Carry: %x\n", carry);

	masked = masked + carry;

	printf("Before 1's complement: %x\n", masked);

	printf("Checksum: %x\n", ~masked & 0xFFFF);

	return 0;
}

