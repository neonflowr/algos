#include "21_02_2023.c"
#include "init.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


void print2DUtil(struct Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print(struct Tree *t) {
  if (t != NULL)
    print2DUtil(t, 0);
  else
    printf("Empty\n");
}

int main(int argc, char *argv[]) {
  struct Tree **node;
  *node = NULL;

  printf("Testing\n");
  insert(node, 2, NULL);
  insert(node, 8, NULL);
  insert(node, 1, NULL);
  insert(node, 10, NULL);
  insert(node, 9, NULL);
  insert(node, 15, NULL);
  insert(node, 4, NULL);
  insert(node, 3, NULL);
  insert(node, 19, NULL);
  insert(node, 18, NULL);
  insert(node, 21, NULL);
  insert(node, 13, NULL);
  insert(node, 14, NULL);

  assert((*node)->data == 2);

  print(*node);

  printf("Delete 4\n");
  *node = delete_node(*node, 4);

  print(*node);

  *node = delete_node(*node, 10);

  print(*node);

  *node = delete_node(*node, 8);

  print(*node);

  assert((*node)->data == 2);

  *node = delete_node(*node, 2);

  print(*node);

  assert((*node)->data == 3);

  return 0;
}
