class tree {
public:
  int data;
  tree *left;
  tree *right;
  tree *parent;

  tree(int data, tree *left, tree *right, tree *parent) {
    this->data = data;
    this->left = left;
    this->right = right;
    this->parent = parent;
  }

  static void insert(tree **t, int data) {
    if (*t == nullptr) {
      tree *node = new tree(data, nullptr, nullptr, nullptr);
      *t = node;
      return;
    }

    if ((*t)->data > data) {
      tree::insert(&(*t)->left, data);
    } else {
      tree::insert(&(*t)->right, data);
    }
  }

  static tree *search(tree *t, int data) {
    if (t == nullptr)
      return nullptr;

    if (t->data > data) {
      return tree::search(t->left, data);
    } else {
      return tree::search(t->right, data);
    }
  }

  static void preorder(tree *t, void (*process)(tree *)) {
    if (t) {
      (*process)(t);
      preorder(t->left, process);
      preorder(t->right, process);
    }
  }

  static void postorder(tree *t, void (*process)(tree *)) {
    if (t) {
      postorder(t->left, process);
      postorder(t->right, process);
      (*process)(t);
    }
  }

  static void inorder(tree *t, void (*process)(tree *)) {
    if (t) {
      inorder(t->left, process);
      (*process)(t);
      inorder(t->right, process);
    }
  }

  tree* min() {
    tree *t = this;
    while(t->left != nullptr) {
      t = t->left;
    }
    return t;
  }

  static tree* delete_node(tree *t, int data) {
    if (t->data > data) {
      t->left = tree::delete_node(t->left, data);
    } else if (t->data < data) {
      t->right = tree::delete_node(t->right, data);
    } else {
      if (t->left == nullptr) {
        tree *tmp = t->right;
        delete t;
        return tmp;
      } else if (t->right == nullptr) {
        tree *tmp = t->left;
        delete t;
        return tmp;
      } else {
        tree *next = t->right->min();
        t->data = next->data;
        t->right = tree::delete_node(t->right, next->data);
      }
    }
    return t;
  }
};
