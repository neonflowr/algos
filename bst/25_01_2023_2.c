#include <stdio.h>
#include <stdlib.h>


#define COUNT 10


typedef struct tree {
  int data;
  struct tree *parent;
  struct tree *left;
  struct tree *right;
} tree;

tree *search(tree *t, int value) {
  if (t == NULL)
	return NULL;
  if (t->data == value)
	return t;

  if (value > t->data)
	return search(t->right, value);
  else
	return search(t->left, value);
}

void insert(tree **t, int value, tree *parent) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->left = NULL;
	node->right = NULL;
	node->parent = parent;
	*t = node;
	return;
  }

  if (value > (*t)->data)
	return insert(&((*t)->right), value, *t);
  else
	return insert(&((*t)->left), value, *t);
}


void traverse(tree *t, void (*process_item)(tree *t)) {
  if (t != NULL) {
	traverse(t->left, process_item);
	(*process_item)(t);
	traverse(t->right, process_item);
  }
}

tree *min(tree *t) {
  if (t == NULL) {
	return NULL;
  }

  tree *node = t;
  while(node->left != NULL) {
	node = node->left;
  }

  return node;
}

tree *max(tree *t) {
  if (t == NULL)
	return NULL;

  tree *node = t;
  while(node->right != NULL) {
	node = node->right;
  }

  return node;
}


tree *delete(tree *t, int value) {
  if (t == NULL)
	return NULL;

  if (value < t->data)
	t->left = delete(t->left, value);
  else if (value > t->data)
	t->right = delete(t->right, value);
  else {
    printf("Delete %d\n", t->data);
    if (t->left == NULL) {
      tree *tmp = t->right;
      free(t);
      return tmp;
	} else if (t->right == NULL) {
	  tree *tmp = t->left;
	  free(t);
	  return tmp;
	}

	tree* next = min(t->right);
	t->data = next->data;
	t->right = delete(t->right, next->data);
  }
  return t;
}

void print2DUtil(struct tree *root, int space) {
  if (root == NULL)
    return;

  space += COUNT;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = COUNT; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print2D(struct tree *root) {
  if (root != NULL)
    print2DUtil(root, 0);
  else
    printf("Empty\n");
}

int main() {
  printf("Testing tree\n");
  tree *t = malloc(sizeof(tree));
  t->data = 2;
  t->left = NULL;
  t->right = NULL;
  t->parent = NULL;

  printf("Init tree\n");
  insert(&t, 1, NULL);
  insert(&t, 4, NULL);
  insert(&t, 3, NULL);
  insert(&t, 10, NULL);
  insert(&t, 8, NULL);
  insert(&t, 6, NULL);
  insert(&t, 7, NULL);
  print2D(t);

  t = delete (t, 4);
  print2D(t);

  t = delete (t, 10);
  print2D(t);

  printf("Min: %d\n", min(t)->data);
  printf("Max: %d\n", max(t)->data);

  t = delete (t, 6);
  print2D(t);

  t = delete (t, 7);
  print2D(t);

  t = delete (t, 2);
  print2D(t);

  t = delete (t, 3);
  print2D(t);

  t = delete (t, 8);
  print2D(t);

  return 0;
}
