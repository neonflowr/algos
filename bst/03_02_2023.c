#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct Tree {
  int data;
  struct Tree *parent;
  struct Tree *left;
  struct Tree *right;
} tree;

void insert(tree **t, int value, tree *parent) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->left = NULL;
	node->right = NULL;
	node->parent = parent;
	*t = node;
	return;
  }

  if (value < (*t)->data) {
	insert(&((*t)->left), value, *t);
  } else {
    insert(&((*t)->right), value, *t);
  }
}

tree *search(tree *t, int value) {
  if (t == NULL)
	return NULL;
  if (t->data == value)
	return t;

  if (value < t->data)
	return search(t->left, value);
  else
	return search(t->right, value);
}

void traverse(tree *t, void (*process_func)(tree *)) {
  if (t != NULL) {
	traverse(t->left, process_func);
	(*process_func)(t);
	traverse(t->right, process_func);
  }
}

tree *min(tree *t) {
  if (t == NULL)
	return NULL;

  while(t->left != NULL) {
	t = t->left;
  }

  return t;
}

tree *delete_node(tree *t, int value) {
  if (t == NULL)
	return NULL;
  if (value < t->data)
	t->left = delete_node(t->left, value);
  else if (value > t->data)
    t->right = delete_node(t->right, value);
  else {
	if (t->left == NULL) {
	  tree *tmp = t->right;
	  free(t);
	  return tmp;
	} else if (t->right == NULL) {
	  tree *tmp = t->left;
	  free(t);
	  return tmp;
	}

	tree *next = min(t->right);
	t->data = next->data;
	t->right = delete_node(t->right, next->data);
  }
  return t;
}
