#include <climits>
#include <iostream>

using namespace std;

#define DEGREE 3

class Node {
  bool is_leaf;
  int *key;
  int size;
  Node **ptr;
  friend class BPTree;

public:
  Node();
};

class BPTree {
  Node *root;
  void _insert     (int, Node *, Node *);
  Node *find_parent(Node *, Node *);

public:
  bool search   (int);
  void insert   (int);
  void display  (Node *);
  Node *get_root();

  BPTree();
};

Node::Node() {
  key = new int[DEGREE];
  ptr = new Node * [DEGREE + 1];
}

BPTree::BPTree() {
  root = nullptr;
}

void BPTree::insert(int x) {
  if (root == nullptr) {
    root = new Node;
    root->key[0] = x;
    root->is_leaf = true;
    root->size = 1;
  } else {
    Node *cursor = root;
    Node *parent;

    // Find correct leaf node to insert
    while(cursor->is_leaf == false) {
      parent = cursor;
      for (int i = 0; i < cursor->size; i++) {
	if (x < cursor->key[i]) {
	  cursor = cursor->ptr[i];
	  break;
	}
	if (i == cursor->size - 1) {
	  cursor = cursor->ptr[i+1];
	  break;
	}
      }
    }

    if (cursor->size < DEGREE) {
      // Insert normally into leaf node
      int i = 0;

      // Find place to insert key
      while (x > cursor->key[i] && i < cursor->size)
	i++;

      // Make space for new key by moving keys one cell to the right
      for (int j = cursor->size; j > i; j--) {
	cursor->key[j] = cursor->key[j-1];
      }

      cursor->key[i] = x;
      cursor->size++;

      // Does something with children pointers
      cursor->ptr[cursor->size] = cursor->ptr[cursor->size-1];
      cursor->ptr[cursor->size-1] = nullptr;
    } else {
      // need to split
      Node *new_leaf = new Node;
      int virtual_node[DEGREE+1];
      for(int i = 0; i < DEGREE; i++) {
	virtual_node[i] = cursor->key[i];
      }
      int i = 0, j;
      while(x > virtual_node[i] && i < DEGREE)
	i++;
      // Make space for new key
      for(j = DEGREE; j > i; j--) {
	virtual_node[j] = virtual_node[j-1];
      }
      virtual_node[i] = x;

      new_leaf->is_leaf = true;

      cursor->size   = (DEGREE + 1) / 2; // Splitted size
      new_leaf->size =  DEGREE + 1 - (DEGREE + 1) / 2;

      cursor->ptr[cursor->size]     = new_leaf;
      new_leaf->ptr[new_leaf->size] = cursor->ptr[DEGREE]; // Is this the sibling pointer? Seems to be a one-way pointer

      cursor->ptr[DEGREE] = nullptr;
      for (i = 0; i < cursor->size; i++) {
	cursor->key[i] = virtual_node[i];
      }
      for (i = 0, j = cursor->size; i < new_leaf->size; i++, j++) {
	new_leaf->key[i] = virtual_node[j];
      }

      if (cursor == root) {
	auto new_root = new Node;
	new_root->key[0]  = new_leaf->key[0];
	new_root->ptr[0]  = cursor;
	new_root->ptr[1]  = new_leaf;
	new_root->is_leaf = false;
	new_root->size    = 1;
	root = new_root;
      } else {
	_insert(new_leaf->key[0], parent, new_leaf);
      }
    }
  }
}

void BPTree::_insert(int x, Node *cursor, Node *child) {
  if (cursor->size < DEGREE) {
    // insert normally into cursor (parent)
    int i = 0;
    while(x > cursor->key[i] && i < cursor->size)
      i++;
    for(int j = cursor->size; j > i; j--) {
      cursor->key[j] = cursor->key[j-1];
    }
    cursor->key[i] = x;
    cursor->size++;
    cursor->ptr[i+1] = child;
  } else {
    auto new_internal = new Node;

    int virtual_key[DEGREE+1];
    Node *virtual_ptr[DEGREE+2];

    // Copy keys and children pointers
    for (int i = 0; i < DEGREE; i++) {
      virtual_key[i] = cursor->key[i];
    }
    for (int i = 0; i < DEGREE+1; i++) {
      virtual_ptr[i] = cursor->ptr[i];
    }

    int i = 0, j;
    while(x > virtual_key[i] && i < DEGREE)
      i++;
    for (j = DEGREE; j > i; j--) {
      virtual_key[j] = virtual_key[j-1];
    }
    virtual_key[i] = x;

    for(j = DEGREE+2; j > i + 1; j--) {
      virtual_ptr[j] = virtual_ptr[j-1];
    }
    virtual_ptr[i+1]= child; // child = new_leaf

    new_internal->is_leaf = false;
    cursor->size = (DEGREE + 1) / 2;
    new_internal->size = DEGREE - (DEGREE + 1) / 2;
    for(i = 0, j = cursor->size + 1; i < new_internal->size; i++, j++) {
      new_internal->key[i] = virtual_key[j];
    }
    for (i = 0, j = cursor->size + 1; i < new_internal->size + 1; i++, j++) {
      new_internal->ptr[i] = virtual_ptr[j];
    }
    if (cursor == root) {
      auto new_root = new Node;
      new_root->key[0]  = cursor->key[cursor->size];
      new_root->ptr[0]  = cursor;
      new_root->ptr[1]  = new_internal;
      new_root->is_leaf = false;
      new_root->size    = 1;
      root              = new_root;
    } else {
      _insert(cursor->key[cursor->size], find_parent(root, cursor), new_internal);
    }
  }
}

Node *BPTree::find_parent(Node *cursor, Node *child) {
  Node *parent;
  if (cursor->is_leaf || cursor->ptr[0]->is_leaf)
    return nullptr;

  for(int i = 0; i < cursor->size + 1; i++) {
    if (cursor->ptr[i] == child) {
      parent =cursor;
      return parent;
    } else {
      parent = find_parent(cursor->ptr[i], child);
      if (parent != nullptr)
	return parent;
    }
  }
  return parent;
}

bool BPTree::search(int x) {
  if (root == nullptr)
    return false;

  auto cursor = root;
  while(cursor->is_leaf == false) {
    for(int i = 0; i < cursor->size; i++) {
      if (x < cursor->key[i]) {
	cursor = cursor->ptr[i];
	break;
      }
      if (i == cursor->size - 1) {
	cursor = cursor->ptr[i+1];
	break;
      }
    }
  }

  // Search within leaf's keys
  for(int i = 0; i < cursor->size; i++) {
    if (cursor->key[i] == x) {
      cout << "Found\n";
      return true;
    }
  }

  return false;
}
