#include <stdio.h>
#include <stdlib.h>

#define COUNT 10

typedef struct tree {
  int data;
  struct tree *parent;
  struct tree *left;
  struct tree *right;
} tree;

tree *search(tree *t, int value) {
  if (t == NULL) {
    return NULL;
  }

  if (t->data == value) {
    return t;
  }

  if (value > t->data) {
    return search(t->right, value);
  } else {
    return search(t->left, value);
  }
};

tree* insert(tree **t, int value, tree* parent) {
  if (*t == NULL) {
    tree *node = malloc(sizeof(tree));
    node->data = value;
    node->left = NULL;
    node->right = NULL;
    node->parent = parent;
    *t = node;
    return node;
  }

  if (value > (*t)->data) {
    return insert(&((*t)->right), value, *t);
  } else {
    return insert(&((*t)->left), value, *t);
  }
}

void traverse(tree *t, void (*process_item)(tree *item)) {
  if (t != NULL) {
    traverse(t->left, process_item);
    (*process_item)(t);
    traverse(t->right, process_item);
  }
}

void print_tree_item(tree *t) { printf("%d ", t->data); }

void print_tree(tree *t) {
  printf("Tree: ");
  traverse(t, print_tree_item);
  printf("\n");
}

tree *min(tree *t) {
  if (t == NULL)
    return NULL;

  tree *l = t;
  while (l->left != NULL) {
    l = l->left;
  }

  return l;
};

tree *max(tree *t) {
  if (t == NULL)
    return NULL;

  tree *r = t;
  while (r->right != NULL) {
    r = r->right;
  }

  return r;
};

void print2DUtil(struct tree *root, int space) {
  if (root == NULL)
	return;

  space += COUNT;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = COUNT; i < space; i++)
	printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print2D(struct tree *root) {
  if (root != NULL)
	print2DUtil(root, 0);
  else
	printf("Empty\n");
}

void delete(tree** t) {
  printf("Delete %d\n", (*t)->data);

  if ((*t)->left == NULL && (*t)->right == NULL) {
	if ((*t)->parent->data < (*t)->data)
	  (*t)->parent->right = NULL;
	else
	  (*t)->parent->left = NULL;
	free(*t);
	*t = NULL;
  } else if ((*t)->left == NULL || (*t)->right == NULL) {
	tree *child;
	if ((*t)->left != NULL)
	  child = (*t)->left;
	else
	  child = (*t)->right;

	if ((*t)->parent->data < (*t)->data)
	  (*t)->parent->right = child;
	else
	  (*t)->parent->left = child;

	child->parent = (*t)->parent;

	free(*t);
	*t = NULL;
  } else {
	tree *next_node = min((*t)->right);

	int val = next_node->data;

	delete (&next_node);

	(*t)->data = val;
  }
}

int main() {
  printf("Testing tree\n");
  tree *t = malloc(sizeof(tree));
  t->data = 2;
  t->left = NULL;
  t->right = NULL;
  t->parent = NULL;

  printf("Init tree\n");
  tree* one_node = insert(&t, 1, NULL);
  tree* four_node = insert(&t, 4, NULL);
  insert(&t, 3, NULL);
  insert(&t, 10, NULL);
  insert(&t, 8, NULL);
  insert(&t, 6, NULL);
  insert(&t, 7, NULL);
  print2D(t);

  delete(&four_node);
  print2D(t);

  tree *ten_node = search(t, 10);

  delete (&ten_node);
  print2D(t);

  tree *eight_node = search(t, 8);
  delete(&eight_node);
  print2D(t);

  printf("Min: %d\n", min(t)->data);
  printf("Max: %d\n", max(t)->data);

  tree *six_node = search(t, 6);
  delete(&six_node);
  print2D(t);

  tree *two_node = search(t, 2);
  delete(&two_node);
  print2D(t);

  tree *seven_node = search(t, 7);
  delete (&seven_node);
  print2D(t);

  return 0;
}
