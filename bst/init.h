struct Tree;
struct Tree *create();
void traverse();
void insert();
struct Tree *delete_node();
struct Tree *search();
void print();
struct Tree *min();
struct Tree *max();
