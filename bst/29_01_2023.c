#include "init.h"
#include <stdio.h>
#include <stdlib.h>

struct Tree {
  int data;
  struct Tree *parent;
  struct Tree *left;
  struct Tree *right;
};

struct Tree *search(struct Tree *t, int value) {
  if (t == NULL)
    return NULL;
  if (t->data == value)
    return t;

  if (value < t->data)
    return search(t->left, value);
  else
    return search(t->right, value);
}

void insert(struct Tree **t, int value, struct Tree *parent) {
  if (*t == NULL) {
    struct Tree *node = malloc(sizeof(struct Tree));
    node->data = value;
    node->left = NULL;
    node->right = NULL;
    node->parent = parent;
    *t = node;
    return;
  }

  if (value < (*t)->data) {
    insert(&((*t)->left), value, *t);
  } else {
    insert(&((*t)->right), value, *t);
  }
}

void traverse(struct Tree *t, void (*process_func)(struct Tree *)) {
  if (t != NULL) {
    traverse(t->left, process_func);
    (*process_func)(t);
    traverse(t->right, process_func);
  }
}

struct Tree *min(struct Tree *t) {
  if (t == NULL)
    return NULL;

  while (t->left != NULL) {
    t = t->left;
  }
  return t;
}

struct Tree *delete_node(struct Tree *t, int value) {
  if (t == NULL)
    return NULL;
  if (value < t->data)
    t->left = delete_node(t->left, value);
  else if (value > t->data)
    t->right = delete_node(t->right, value);
  else {
	printf("Delete %d\n", value);
    if (t->left == NULL) {
      struct Tree *tmp = t->right;
      free(t);
      return tmp;
    } else if (t->right == NULL) {
      struct Tree *tmp = t->left;
      free(t);
      return tmp;
    }

    struct Tree *next = min(t->right);
    t->data = next->data;
    t->right = delete_node(t->right, next->data);
  }

  return t;
}

struct Tree *create(int value) {
  struct Tree *t = malloc(sizeof(struct Tree));
  t->data = value;
  t->left = NULL;
  t->right = NULL;
  t->parent = NULL;
  return t;
}

void print2DUtil(struct Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print(struct Tree *t) {
  if (t != NULL)
    print2DUtil(t, 0);
  else
    printf("Empty\n");
}
