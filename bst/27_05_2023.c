#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct Tree {
	int data;
	struct Tree *next;
	struct Tree *left;
} tree;

void insert(tree** t, int value) {
	if (*t == NULL) {
		tree* node = malloc(sizeof(tree));
		node->data = value;
		node->next = node->right = NULL;
		*t = node;
		return;
	}

	if ((*t)->data > value) {
		insert(&(*t)->left, value);
	} else {
		insert(&(*t)->right, value);
	}
}

tree* search(tree *t, int value) {
	if (t == NULL) {
		return;
	}
	if (t->data == value) {
		return t;
	}
	if (t->data > value) {
		return search(t->left, value);
	} else {
		return search(t->right, value);
	}

}

void traverse(tree *t, void (process_node*)(tree*)) {
	if (t != NULL) {
		traverse(t->left, process_node);
		(*process_node)(t);
		traverse(t->right, process_node);
	}
}

tree* min(tree* t) {
	if (t == NULL) return;
	while (t->left != NULL) {
		t = t->left;
	}
	return t;
}

tree* delete_node(tree* t, int value) {
	if (t == NULL) {
		return t;
	}

	if (t->data > value) {
		t->left = delete_node(t->left, value);
	}
	else if (t->data < value) {
		t->right = delete_node(t->right, value);
	}
	else {
		if (t->left == NULL) {
			tree* tmp = t->right;
			free(t);
			return tmp;
		}
		else if (t->right == NULL) {
			tree* tmp = t->left;
			free(t);
			return tmp;
		}
		else {
			tree* next = min(t->right);
			t->data = next->data;
			t->right = delete_node(t->right, next->data);
		}


	}
	return t;
}


