#include <stdio.h>
#include <stdlib.h>

typedef struct tree {
  int data;
  struct tree *parent;
  struct tree *left;
  struct tree *right;
} tree;

tree *search(tree *t, int item);
void insert(tree **l, int item, tree *parent);
void travese(tree *t);
void delete(tree *t, int item);

tree *find_min(tree *t);
tree *find_max(tree *t);


void insert(tree **l, int item, tree *parent) {
  if (*l == NULL) {
	tree *p = malloc(sizeof(tree));
	p->data = item;
	p->left = p->right = NULL;
	p->parent = parent;

	*l = p;
	return;
  }

  if (item < (*l)->data) {
	return insert(&((*l)->left) , item, *l);
  } else {
	return insert(&((*l)->right), item, *l);
  }
}

void traverse(tree *t, void (*process_item)(int item)) {
  // It's important to have a function like this since you can't easily just for loop to list all elements like an array or a linked list
  if (t != NULL) {
	traverse(t->left, process_item); // This will run way to the leftmost node of the tree, so that will be the first item to be processed
	(*process_item)(t->data);
	traverse(t->right, process_item);
  }
}

tree *search(tree *t, int item) {
  if (t == NULL) {
	return NULL;
  }

  if (t->data == item) {
	return t;
  }

  if (item > t->data) {
	return search(t->right, item);
  } else {
	return search(t->left, item);
  }
}

tree *find_min(tree *t) {
  if (t == NULL) {
	return NULL;
  }

  tree *min = t;

  while(t->left != NULL) {
	min = t->left;
  }

  return min;
}


tree *find_max(tree *t) {
  if (t == NULL) {
    return NULL;
  }

  tree *max = t;

  while (t->right != NULL) {
    max = t->right;
  }

  return max;
}
