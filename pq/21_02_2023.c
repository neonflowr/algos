#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define HEAP_BASE 1

typedef struct PQ {
  int *items;
  int size;
  int top;
  unsigned int capacity;
} pq;

pq *create(unsigned int capacity) {
  pq *q = malloc(sizeof(pq));
  q->items = malloc(capacity * sizeof(int));
  q->top = 0;
  q->size = 0;
  q->capacity = capacity;
  return q;
}


int is_full(pq *q) {
  return q->size == q->capacity;
}
int is_empty(pq *q) {
  return q->size == 0;
}

int parent(int index) {
  return index / 2;
}

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int is_valid(pq *q, int index) {
  if (index == HEAP_BASE) return 1;

  if (q->items[parent(index)] > q->items[index]) {
	return 0;
  }

  return 1;
}


void bubble_up(pq *q, int index) {
  if (is_valid(q, index)) return;
  int p = parent(index);
  swap(&q->items[p], &q->items[index]);
  bubble_up(q, p);
}

void insert(pq *q, int value) {
  if (is_full(q)) return;
  q->items[++q->top] = value;
  q->size++;
  bubble_up(q, q->top);
}

void *find_min(pq *q) {
  if(is_empty(q)) return NULL;
  return &q->items[HEAP_BASE];
}

void bubble_down(pq *q, int index) {
  int min = index;
  int child = 2 * index;
  for (int i = 0; i <= 1; i++) {
	child = child + i;
	if (child <= q->top && q->items[child] < q->items[min])
	  min = child;
  }

  if (min == index)
	return;

  swap(&q->items[min], &q->items[index]);
  bubble_down(q, min);
}


void delete_min(pq *q) {
  if (is_empty(q)) return;

  q->items[HEAP_BASE] = q->items[q->top];
  q->items[q->top--] = 0;
  q->size--;

  bubble_down(q, HEAP_BASE);
}

void print(pq *q) {
  for (int i = HEAP_BASE; i <= q->top; i++) {
	printf("%d ", q->items[i]);
  }
  printf("\n");
}

void free_pq(pq *q) {
  free(q->items);
  free(q);
}
