#define SIZE 100

class pq {
private:
	void swap(int* x, int* y) {
		int tmp = *x;
		*x = *y;
		*y = tmp;
	}

	int parent(int x) {
		return x / 2;
	}

	bool is_valid(int i) {
		if (i == 0) return true;
		return this->items[this->parent(i)] < this->items[i];
	}

	void bubble_up(int i) {
		if (this->is_valid(i)) return;
		int p = this->parent(i);
		swap(&this->items[i], &this->items[p]);
		this->bubble_up(p);
	}

	void bubble_down(int index) {
		int child = index * 2 + 1;
		int min = index;
		for (int i = 0; i < 2; i++) {
			child = child + i;
			if (child < this->size && this->items[child] < this->items[min]) {
				min = child;
			}
		}

		if (min == index) return;

		swap(&this->items[min], &this->items[index]);
		bubble_down(min);
	}

public:
	int items[SIZE] = { 0 };
	int size = 0;
	int capacity = SIZE;

	bool is_full() {
		return this->size == this->capacity;
	}

	bool is_empty() {
		return this->size == 0;
	}

	void insert(int x) {
		if (this->is_full()) return;
		this->items[this->size] = x;
		bubble_up(this->size++);
	}

	int pop() {
		if (this->is_empty()) return 0;
		int val = this->items[0];
		this->swap(&this->items[0], this->items[--this->size]);
		this->bubble_down(0);
		return val;
	}

	int min() {
		if (is_empty()) return 0;
		return this->items[0];
	}
};
