struct PQ;
struct PQ *create();
void insert();
void * find_min();
void delete_min();
void print();
void free_pq();
