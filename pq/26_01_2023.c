// 26-01-2023
// ソートされない配列によって優先度付きキューを実装練習

#include <stdio.h>
#include <stdlib.h>

struct PQ {
  int *items;
  int top;
  unsigned int capacity;
  int *max;
  int *min;
};

void insert(struct PQ *q, int value) {
  if (q->top == q->capacity - 1)
    return;

  q->items[++q->top] = value;

  if (q->min == NULL && q->max == NULL) {
    q->min = q->max = &q->items[q->top];
  }

  if (value < *(q->min))
    q->min = &q->items[q->top];

  if (value > *(q->max))
    q->max = &q->items[q->top];
}

/* int pop_max(struct PQ *q) { */
// Same as pop_min but reversed
/* } */

int pop_min(struct PQ *q) {
  if (q->top == -1) {
    printf("cAn't pop empty PQ\n");
    return -1;
  }
  // Replace the min with the top of the array and decrement the top counter
  int tmp = *(q->min);
  *(q->min) = q->items[q->top--];

  q->min = q->items;

  // Need to find new min
  for (int i = 0; i <= q->top; i++) {
    if (q->items[i] < *(q->min)) {
      q->min = &q->items[i];
    }
  }

  printf("Popped: %d\n", tmp);

  return tmp;
}

int find_max(struct PQ *q) { return *q->max; }

int find_min(struct PQ *q) { return *q->min; }

struct PQ *create(unsigned int capacity) {
  struct PQ *q = malloc(sizeof(struct PQ));
  q->items = (int *)malloc(capacity * sizeof(int));
  q->top = -1;
  q->capacity = capacity;
  q->min = NULL;
  q->max = NULL;
  return q;
}

void print_pq(struct PQ *q) {
  if (q->top == -1) {
    printf("Empty PQ\n");
    return;
  }
  for (int i = 0; i <= q->top; i++) {
    printf("%d ", q->items[i]);
  }
  printf("\n");
}

int main() {
  struct PQ *q = create(10);

  printf("Initial PQ\n");
  insert(q, 5);
  insert(q, 8);
  insert(q, 2);
  insert(q, 1);
  insert(q, 95);
  insert(q, 5198899);
  print_pq(q);

  printf("Min: %d\n", find_min(q));
  printf("Pop min\n");
  pop_min(q);

  print_pq(q);

  pop_min(q);
  pop_min(q);

  print_pq(q);

  printf("Min: %d\n", find_min(q));

  pop_min(q);
  pop_min(q);

  print_pq(q);

  pop_min(q);

  print_pq(q);

  pop_min(q);

  print_pq(q);

  return 0;
}
