#include "init.h"
#include "21_02_2023.c"
#include <stdio.h>
#include <assert.h>

int main() {
  struct PQ *q = create(10);

  printf("Initial PQ\n");
  insert(q, 5);
  insert(q, 8);
  insert(q, 2);
  insert(q, 1);
  insert(q, 95);
  insert(q, 519);

  print(q);

  assert(*(int *)find_min(q) == 1);

  printf("Min: %d\n", *(int *)find_min(q));

  printf("Pop min\n");
  delete_min(q);
  assert(*(int *)find_min(q) == 2);

  print(q);

  delete_min(q);
  assert(*(int *)find_min(q) == 5);
  print(q);
  delete_min(q);
  assert(*(int *)find_min(q) == 8);

  print(q);

  printf("Min: %d\n", *(int *)find_min(q));

  delete_min(q);
  assert(*(int *)find_min(q) == 95);

  print(q);

  delete_min(q);
  assert(*(int *)find_min(q) == 519);
  print(q);

  delete_min(q);

  print(q);

  assert((int *)find_min(q) == NULL);

  free_pq(q);

  return 0;
}
