#include <stdio.h>
#include <stdlib.h>
#include "init.h"

#define HEAP_BASE 1

void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

typedef struct PQ {
	int* items;
	int size;
	int capacity;
	int top;
} pq;

struct PQ* create(int capacity) {
	pq* p = malloc(sizeof(pq));
	p->items = malloc(sizeof(int) * capacity);
	p->size = 0;
	p->capacity = capacity;
	p->top = 0;
	return p;
}

int is_full(pq* q) {
	return q->size == q->capacity;
}

int is_empty(pq* q) {
	return q->size == 0;
}

int parent(int x) {
	return x / 2;
}

int child(int x) {
	return 2 * x;
}

int right_child(int x) {
	return 2 * x + 1;
}

int is_valid(pq* q, int i) {
	return i == HEAP_BASE ? 1 : q->items[parent(i)] < q->items[i];
}

void bubble_up(pq* q, int i) {
	if (is_valid(q, i))
		return;
	int p = parent(i);
	swap(&q->items[p], &q->items[i]);
	bubble_up(q, p);
}

void insert(pq* q, int val) {
	if (is_full(q))
		return;
	q->items[++q->top] = val;
	q->size++;
	bubble_up(q, q->top);
}

void* find_min(pq* q) {
	if (is_empty(q))
		return NULL;
	return &q->items[HEAP_BASE];
}


void bubble_down(pq* q, int i) {
	int ci = child(i);
	int min = i;
	for (int i = 0; i <= 1; i++) {
		if (ci <= q->top && q->items[min] > q->items[ci + i]) {
			min = ci + i;
		}
	}

	if (min == i)
		return;
	swap(&q->items[min], &q->items[i]);
	bubble_down(q, min);
}

void delete_min(pq *q) {
	if (is_empty(q))
		return;
	q->items[HEAP_BASE] = q->items[q->top--];
	q->size--;
	bubble_down(q, HEAP_BASE);
}




void print();
void free_pq();
