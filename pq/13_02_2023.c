#include "init.h"
#include <stdlib.h>
#include <stdio.h>

typedef struct Tree {
  int data;
  struct Tree *left;
  struct Tree *right;
} tree;

void insert_tree(tree **t, int value) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->left = node->right = NULL;
	*t = node;
	return;
  }

  if ((*t)->data < value) {
	insert(&(*t)->right, value);
  } else {
    insert(&(*t)->left, value);
  }
}

tree *min(tree *t) {
  if (t == NULL) return NULL;
  while(t->left != NULL) {
	t = t->left;
  }
  return t;
}

void traverse(tree *t, void (*process_func)(tree *)) {
  if (t != NULL) {
	traverse(t->left, process_func);
	(*process_func)(t);
	traverse(t->right, process_func);
  }
}

void free_node(tree *t) {
  free(t);
}

void free_tree(tree *t) {
  traverse(t, free_node);
}

tree *delete_node(tree *t, int value) {
  if (t == NULL) return NULL;

  if (t->data < value) {
	t->right = delete_node(t->right, value);
  } else if (t->data > value) {
	t->left = delete_node(t->left, value);
  } else {
	if (t->left == NULL) {
	  tree *tmp = t->right;
	  free(t);
	  t = NULL;
	  return tmp;
	} else if (t->right == NULL) {
	  tree *tmp = t->left;
	  free(t);
	  t = NULL;
	  return tmp;
	}

	tree *next = min(t->right);
	t->data = next->data;
	t->right = delete_node(t->right, next->data);
  }
  return t;
}


void print_node(tree *t) {
  if (t != NULL) {
	printf("%d ", t->data);
  }
}

    typedef struct PQ {
  tree *items;
} pq;

pq *create() {
  pq *q = malloc(sizeof(pq));
  q->items = NULL;
  return q;
}

void insert(pq *q, int value) {
  insert_tree(&q->items, value);
}

void *find_min(pq *q) {
  tree *min_node = min(q->items);
  if (min_node == NULL) return NULL;
  return &min_node->data;
}

void delete_min(pq *q) {
  tree *min_node = min(q->items);
  printf("min node %d\n", min_node->data);
  if (min_node == NULL) return;
  q->items = delete_node(q->items, min_node->data);
}

void print(pq *q) {
  traverse(q->items, print_node);
  printf("\n");
}

void free_pq(pq *q) {
  free_tree(q->items);
  free(q);
}
