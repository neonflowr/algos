// 1月30日2023年
// ソートされない配列によって優先度付きキューを実装練習
#include "init.h"
#include <stdio.h>
#include <stdlib.h>

struct PQ {
  int *items;
  int top;
  int *min;
  unsigned int capacity;
};

struct PQ *create(unsigned int capacity) {
  struct PQ *q = malloc(sizeof(struct PQ));
  q->items = malloc(capacity * sizeof(int));
  q->top = -1;
  q->min = NULL;
  q->capacity = capacity;
  return q;
}

void insert(struct PQ *q, int value) {
  if (q->top == q->capacity - 1)
    return;

  q->top++;
  q->items[q->top] = value;

  if (q->min == NULL || q->items[q->top] < *q->min)
    q->min = &q->items[q->top];
}

void *find_min(struct PQ *q) {
  if (q->top == -1) {
    return NULL;
  }
  return q->min;
}

void delete_min(struct PQ *q) {
  if (q->top == -1)
    return;

  *q->min = q->items[q->top--];

  for (int i = 0; i <= q->top; i++) {
    if (q->items[i] < *q->min)
      q->min = &q->items[i];
  }
}

void print(struct PQ *q) {
  if (q->top == -1) {
    printf("Empty\n");
    return;
  }
  for (int i = 0; i <= q->top; i++) {
    printf("%d ", q->items[i]);
  }
  printf("\n");
}
