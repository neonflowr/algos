#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct _tree {
  int data;
  struct _tree *parent;
  struct _tree *left;
  struct _tree *right;
} Tree;

void insert_bst(Tree **t, int value, Tree *parent) {
  if (*t == NULL) {
	Tree *node = malloc(sizeof(Tree));
	node->data= value;
	node->left = NULL;
	node->right = NULL;
	node->parent = parent;
	*t = node;
	return;
  }

  if (value < (*t)->data) {
	insert(&((*t)->left), value, *t);
  } else {
    insert(&((*t)->right), value, *t);
  }
}

Tree *min_bst(Tree *t) {
  if (t == NULL)
	return NULL;
  while(t->left != NULL) {
	t = t->left;
  }
  return t;
}

Tree *delete_bst(Tree *x, int value) {
  if (x == NULL)
	return NULL;
  if (value < x->data) {
	x->left = delete_bst(x->left, value);
  } else if (value > x->data) {
	x->right = delete_bst(x->right, value);
  } else {
	if (x->left == NULL) {
	  Tree *tmp = x->right;
	  free(x);
	  return tmp;
	} else if (x->right == NULL) {
	  Tree *tmp = x->left;
	  free(x);
	  return tmp;
	}

	Tree *next = min_bst(x->right);
	x->data = next->data;
	x->right = delete_bst(x->right, next->data);
  }
  return x;
}

void print2DUtil(Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print_tree(Tree *t) {
  if (t != NULL)
    print2DUtil(t, 0);
  else
    printf("Empty\n");
}

typedef struct PQ {
  struct _tree *items;
} PQ;

PQ * create() {
  PQ *p = malloc(sizeof(PQ));
  p->items = NULL;
  return p;
}

void insert(PQ *q, int value) {
  printf("Insert %d\n", value);
  insert_bst(&q->items, value, NULL);
}

void *find_min(PQ *q) {
  Tree *min = min_bst(q->items);
  if (min != NULL)
	return &min->data;
  return NULL;
}

void delete_min(PQ *q) {
  Tree *min = min_bst(q->items);
  if (min != NULL) {
    printf("Delete %d\n", min->data);
    q->items = delete_bst(q->items, min->data);
  }
}

void print(PQ *q) {
  printf("Tree: ");
  if (q != NULL)
    print_tree(q->items);
  printf("\n");
}
