#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define HEAP_BASE 1

typedef struct PQ {
  int *items;
  int top;
  unsigned int capacity;
} pq;

pq *create(unsigned int capacity) {
  pq *q = malloc(sizeof(pq));
  q->items = malloc(capacity * sizeof(int));
  q->top = 0;
  q->capacity = capacity;
  return q;
}

void free_pq(pq *q) {
  free(q->items);
  q->items = NULL;
  free(q);
  q = NULL;
}

int parent(int index) { return index / 2; }
int leftchild(int index) { return index * 2; }

int is_full(pq *q) { return q->top == q->capacity - 1; }

int is_empty(pq *q) { return q->top == 0; }

int is_valid(pq *q, int index) {
  if (is_empty(q))
    return 1;

  if (index == HEAP_BASE)
    return 1;

  if (q->items[index] < q->items[parent(index)]) {
    return 0;
  } else {
    return 1;
  }
}

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void bubble_up(pq *q, int index) {
  if (is_valid(q, index))
    return;

  int parent_index = parent(index);
  swap(&q->items[index], &q->items[parent_index]);

  bubble_up(q, parent_index);
}

void insert(pq *q, int value) {
  if (is_full(q))
    return;

  q->items[++q->top] = value;
  bubble_up(q, q->top);
}

void *find_min(pq *q) {
  if (is_empty(q))
    return NULL;
  return &q->items[HEAP_BASE];
}

void bubble_down(pq *q, int index) {
  int min_index = index;
  int child_index = 2 * index;
  for (int i = 0; i <= 1; i++) {
    child_index = child_index + i;
    if (child_index < q->top && q->items[child_index] < q->items[min_index])
      min_index = child_index;
  }

  if (min_index == index)
    return;

  swap(&q->items[index], &q->items[min_index]);

  bubble_down(q, min_index);
}

void print(pq *q) {
  for (int i = 1; i <= q->top; i++) {
    printf("%d ", q->items[i]);
  }
  printf("\n");
}

void delete_min(pq *q) {
  if (is_empty(q))
    return;

  swap(&q->items[HEAP_BASE], &q->items[q->top--]);

  bubble_down(q, HEAP_BASE);
}
