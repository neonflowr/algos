// 二分探索木によって優先度付きキューを実装練習
// 26-01-2023

#include <stdio.h>
#include <stdlib.h>

struct Tree {
  int data;
  struct Tree *parent;
  struct Tree *left;
  struct Tree *right;
};

struct PQ {
  struct Tree *items;
  struct Tree *min;
};

// BST

struct Tree *insert_bst(struct Tree **t, int value, struct Tree *parent) {
  if (*t == NULL) {
    struct Tree *new_tree = malloc(sizeof(struct Tree));
    new_tree->data = value;
    new_tree->left = NULL;
    new_tree->right = NULL;
    new_tree->parent = parent;
    *t = new_tree;
    return new_tree;
  }

  if (value < (*t)->data) {
    return insert_bst(&((*t)->left), value, *t);
  } else {
    return insert_bst(&((*t)->right), value, *t);
  }
}

struct Tree *min_bst(struct Tree *t) {
  if (t == NULL) {
    return NULL;
  }

  struct Tree *node = t;
  while (node->left != NULL) {
    node = node->left;
  }

  return node;
}

struct Tree *delete_bst(struct Tree *t, int value) {
  if (t == NULL)
    return NULL;

  if (value < t->data)
    t->left = delete_bst(t->left, value);
  else if (value > t->data)
    t->right = delete_bst(t->right, value);
  else {
    if (t->left == NULL) {
      struct Tree *child = t->right;
      free(t);
      return child;
    } else if (t->right == NULL) {
      struct Tree *child = t->left;
      free(t);
      return child;
    }

    struct Tree *next = min_bst(t->right);
    t->data = next->data;
    t->right = delete_bst(t->right, next->data);
  }

  return t;
}

void traverse_bst(struct Tree *t, void (*process_item)(struct Tree *t)) {
  if (t != NULL) {
    traverse_bst(t->left, process_item);
    (*process_item)(t);
    traverse_bst(t->right, process_item);
  }
}

// PQ

void insert_pq(struct PQ *q, int value) {
  struct Tree *new_node = insert_bst(&q->items, value, NULL);
  printf("Insert %d\n", new_node->data);
  if (q->min == NULL || value < q->min->data) {
    q->min = new_node;
  }
}

struct Tree *find_min(struct PQ *q) {
  return q->min;
}

void delete_min(struct PQ *q) {
  struct Tree *min = find_min(q);
  printf("Delete min: %d\n", min->data);
  q->items = delete_bst(q->items, min->data);
  q->min = min_bst(q->items);
}

struct PQ *create_pq() {
  struct PQ *q = malloc(sizeof(struct PQ));
  q->items = NULL;
  q->min = NULL;
  return q;
}

// PRINT UTILS

void print2DUtil(struct Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print2D(struct Tree *root) {
  if (root != NULL)
    print2DUtil(root, 0);
  else
    printf("Empty\n");
}

void print_pq(struct PQ *q) {
  if (q->items == NULL) {
    printf("Empty PQ\n");
    return;
  }

  print2D(q->items);
}

int main() {
  struct PQ *q = create_pq();
  printf("Init pq\n");
  insert_pq(q, 6);
  insert_pq(q, 15);
  insert_pq(q, 11);
  insert_pq(q, 4);
  insert_pq(q, 2);
  insert_pq(q, 9);
  insert_pq(q, 3);
  insert_pq(q, 1);

  print_pq(q);

  printf("Min: %d\n", find_min(q)->data);

  printf("Pops\n");
  delete_min(q);
  delete_min(q);
  delete_min(q);
  delete_min(q);
  delete_min(q);
  print_pq(q);
  printf("Min: %d\n", find_min(q)->data);

  printf("Pop again\n");
  delete_min(q);
  delete_min(q);
  print_pq(q);
  printf("Min: %d\n", find_min(q)->data);

  delete_min(q);
  print_pq(q);
  printf("Min: %p\n", find_min(q));

  return 0;
}
