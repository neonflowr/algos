// LRU cache implementation

#include <iostream>
#include <limits.h>
#include <stdio.h>

using namespace std;

#define SIZE 10

class _ci {
public:
	int key = INT_MIN;
	int value = INT_MIN;

	_ci() {

	}

	_ci(int key, int value) {
		this->key = key;
		this->value = value;
	}
};

class _cache {
public:
	_ci items[SIZE];
	int capacity;
	int size;

	int last_random;

	_cache() {
		this->capacity = SIZE;
		for (int i = 0; i < this->capacity; i++) {
			this->items[i].key = INT_MIN;
			this->items[i].value = INT_MIN;
		}
		this->size = 0;
		this->seed();
	}

	void print() {
		for (int i = 0; i < this->capacity; i++) {
			printf("%d ", this->items[i].value, this->items[i].key);
		}
		printf("\n");
	}

	int hash(int key) {
		return (127 * key + 557) % capacity;
	}

	int seed() {
		this->last_random = 11317;
	}

	int rando() {
		last_random = (127 * last_random + 523) % 41213;
		return last_random;
	}

	bool is_full() {
		return this->size == this->capacity;
	}

	bool key_exists(int key) {
		int h = hash(key);
		return !(this->items[h].key == INT_MIN && this->items[h].value == INT_MIN);
	}

	bool get(int key, int *result) {
		int h = this->hash(key);
		if (!this->key_exists(key)) {
			return false;
		}
		*result = this->items[h].value;
		return true;
	}

	bool insert(int key, int value) {
		printf("Insert %d %d\n", key, value);
		int h = this->hash(key);

		if (this->is_full()) {
			// evict random item
			h = this->rando() % this->capacity;
			printf("Evict %d %d\n", h, this->items[h].value);

			this->items[h].key = key;
			this->items[h].value = value;
			return true;
		}

		if (this->key_exists(key)) {
			printf("exists\n");
			this->items[h].key = key;
			this->items[h].value = value;
			return true;
		}

		this->items[h].key = key;
		this->items[h].value = value;
		this->size++;
		this->print();
		return true;
	}
};

int main() {
	_cache* c = new _cache();

	for (int i = 0; i < c->capacity * 10; i++) {
		c->insert(c->rando(), c->rando() % 17);
	}
	printf("Final cache\n");
	c->print();

	delete c;

	return 0;
}
