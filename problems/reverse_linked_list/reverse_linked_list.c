#include <stdio.h>
#include <stdlib.h>

struct list {
  int data;
  struct list *next;
};

void insert(struct list **l, int value) {
  struct list *node = malloc(sizeof(struct list));
  node->data = value;
  node->next = *l;
  *l = node;
}

void print_list(struct list *l) {
  while (l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}

void reverse(struct list **l, struct list *current) {
  if (current == NULL) {
	current = (*l)->next;
	(*l)->next = NULL;
  }
  struct list *tmp = current->next;
  current->next = *l;
  *l = current;
  if (tmp != NULL)
	reverse(l, tmp);
}

void reverse_loop(struct list **l) {
  struct list *next;
  struct list *current = *l;
  struct list *prev;
  while(current != NULL) {
	next = current->next;
	current->next = prev;
	prev = current;
	current = next;
  }
  *l = prev;
}

int main() {
  printf("Reverse linked list\n");
  struct list *i = NULL;
  insert(&i, 1);
  insert(&i, 2);
  insert(&i, 3);
  insert(&i, 4);
  insert(&i, 5);
  insert(&i, 6);

  print_list(i);

  printf("Rverse\n");
  reverse_loop(&i);
  print_list(i);

  printf("Rverse\n");
  reverse_loop(&i);
  print_list(i);

  printf("Rverse\n");
  reverse_loop(&i);
  print_list(i);

  return 0;
}
