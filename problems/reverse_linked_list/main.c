#include "init.h"
#include "12_02_2023.c"
#include <stdio.h>
#include <assert.h>

int main() {
  struct List **head;
  *head = NULL;

  insert(head, 1);
  insert(head, 2);
  insert(head, 3);
  insert(head, 4);
  insert(head, 6);
  insert(head, 16);

  assert( (*head)->data == 16 ) ;

  print(*head);

  reverse(head);

  assert((*head)->data == 1);

  print(*head);

  reverse(head);

  assert((*head)->data == 16);

  print(*head);

  reverse(head);

  assert((*head)->data == 1);

  print(*head);

  return 0;
}
