#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
	int data;
	struct List* next;
} list;

void insert(list** head, int value) {
	list* l = malloc(sizeof(list));
	l->data = value;
	l->next = *head;
	*head = l;
}

void reverse(list** head) {
	list* current = *head;
	list* prev = NULL;
	list* next = NULL;
	while (current != NULL) {
		printf("Lopping\n");
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}

	printf("Final\n");
	*head = prev;
}

void print(list* l) {
	while (l != NULL) {
		printf("%d ", l->data);
		l = l->next;
	}
	printf("\n");
}

