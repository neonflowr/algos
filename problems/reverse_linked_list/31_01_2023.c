#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List  {
  int data;
  struct List *next;
} list;

void insert(list **l, int value) {
  list *n = malloc(sizeof(list));
  n->data = value;
  n->next = *l;
  *l = n;
}

void reverse(list **l) {
  list *current = *l;
  list *prev = NULL;
  list *next = NULL;
  while(current != NULL) {
	next = current->next;
	current->next = prev;
	prev = current;
	current = next;
  }

  *l = prev;
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
