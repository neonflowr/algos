#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

void reverse(list **l) {
  list *current = *l;
  list *prev = NULL;
  list *next = NULL;

  while(current != NULL) {
	next = current->next;
	current->next = prev;
	prev = current;
	current = next;
  }

  *l = prev;
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
