#include "init.h"
#include "21_02_2023.c"
#include <assert.h>

int main(int argc, char **argv) {
  printf("Find middle node in linked list\n");

  list *l = malloc(sizeof(list));
  l->data = 1;
  l->next = NULL;
  list **head = &l;

  insert(head, 2);
  insert(head, 3);
  insert(head, 4);
  insert(head, 5);
  insert(head, 6);
  insert(head,7);

  print(*head);

  struct List *middle = find_middle(*head);

  assert(middle->data == 4);

  struct List* four = search(*head, 4);
  assert(four != NULL);
  delete_node(head, &four);
  assert(four == NULL);

  middle = find_middle(*head);

  assert(middle->data == 5);

  return 0;
}
