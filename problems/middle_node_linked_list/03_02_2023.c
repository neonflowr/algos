#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List  {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data= value;
  l->next = *head;
  *head = l;
}

list *find_middle(list *l) {
  list *tortoise = l;
  list *hare = l;

  while (hare != NULL) {
	if (hare->next != NULL) {
	  tortoise = tortoise->next;
	  if (hare->next->next != NULL) {
		hare = hare->next->next;
	  } else {
		hare = hare->next;
	  }
	} else {
	  break;
	}
  }

  printf("Middle node: %d\n", tortoise->data);

  return tortoise;
}

void print(list *l) {
  while (l != NULL) {
	printf("%d " ,l->data);
	l = l->next;
  }
  printf("\n");
}
