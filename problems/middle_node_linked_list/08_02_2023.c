#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

list *search(list *x, int value) {
  if (x == NULL)
	return NULL;
  if (x->data == value)
	return x;

  return search(x->next, value);
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}

list *item_ahead(list *x, list *y) {
  if (x == NULL)
	return NULL;
  while(x->next != y) {
	x = x->next;
  }
  return x;
}

void delete_node(list **x, list **y) {
  list *tmp = *x;
  list *pred = item_ahead(*x, *y);
  if (pred == NULL) {
	*x = (*x)->next;
	free(tmp);
	tmp = NULL;
  } else {
	pred->next = (*y)->next;
	free(*y);
	*y = NULL;
  }
}

list * find_middle(list *l) {
  list *slow = l;
  list *fast = l;

  while(fast->next != NULL) {
	fast = fast->next;
	if (fast->next)
	  fast = fast->next;
	else
	  break;
	slow = slow->next;
  }

  return slow;
}
