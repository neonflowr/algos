#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
	int data;
	struct List* next;
} list;

struct List* find_middle(list *l) {
	list* fast = l;
	list* slow = l;
	
	while (fast->next != NULL) {
		fast = fast->next;
		if (fast->next) {
			fast = fast->next;
		}
		else {
			break;
		}

		slow = slow->next;
	}

	return slow;
}
