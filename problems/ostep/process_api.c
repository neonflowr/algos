#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int p1() {
  int x = 10;
  int *xp = &x;
  int rc = fork();
  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    *xp = 12;
    char *something[3];
    something[0] = "echo";
    something[1] = "something";
    something[2] = NULL;
    execvp(something[0], something);
    printf("Child's x: %d\n", *xp);
  } else {
    wait(NULL);
    printf("Parent's x: %d\n", *xp);
    *xp = 13;
    printf("Parent's x changed: %d\n", *xp);
  }
  return 0;
}

void p2() {
  // Conlusion: Child process can access the same file descriptor as the parent
  // process

  int file = open("something.ostep", O_CREAT | O_WRONLY);

  int rc = fork();
  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    printf("Writing as child process\n");
    const char *t = "CHILD PROCESS WRITE\n";
    write(file, t, strlen(t));
  } else {
    printf("Writing as parent process\n");
    const char *t = "PARENT WRITE\n";
    write(file, t, strlen(t));
  }
  close(file);
}

void p3() {
  // So there are ways, just giga complicated ones

  int rc = fork();

  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    printf("Hello\n");
  } else {
    printf("Goodbye\n");
  }
}

void p4() {
  int rc = fork();
  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    char *args[3];
    args[0] = "ls";
    args[1] = "/home/pypypy/Downloads";
    args[2] = NULL;

    // So these variants basically deal with PATH, arguments, environments
    // differently. p seems to be PATH related
    /* execl("/bin/ls", args[1]); */
    /* execle("/bin/ls", args[1]); */
    /* execlp(args[0], args[1]); */
    /* execv("/bin/ls", args); */
    execvp(args[0], args);
  } else {
    wait(NULL); // Apparent you have to wait?
    printf("Parent\n");
  }
}

void p5() {
  // So if we wait in the child, the child will wait for the parent to finish
  // If we put wait in both, then the wait for the parent has higher precedence,
  // so the child will always execute first wait returns the process ID

  int rc = fork();

  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    printf("Hello\n");
    wait(NULL);
    printf("CHild finished waiting\n");
  } else {
    __pid_t pid = wait(NULL);
    printf("Child process ID: %d\n", pid);
    printf("Goodbye\n");
  }
}

void p6() {
  // If we are creating many fork() processes, waitpid() would be useful for
  // specifying which process we want to wait for
  __pid_t rc = fork();

  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    printf("Hello\n");
    printf("Child finished\n");
  } else {
    printf("Child process ID rc: %d\n", rc);
    __pid_t pid = waitpid(rc, NULL, 0);
    printf("Resume parent\n");
    printf("Child process ID rc: %d\n", rc);
    printf("Child process ID: %d\n", pid);
    printf("Goodbye\n");
  }
}

void p7() {
  // Apparently if we close stdout in the child, only the child's output aren't
  // shown. The parent process logs still work fine.

  __pid_t rc = fork();

  if (rc < 0) {
    printf("Failed to create child process\n");
    exit(1);
  } else if (rc == 0) {
    printf("Closing stdout in child....\n");
    close(STDOUT_FILENO);
    printf("Printing in child with stdout closed...\n");
    printf("AAAAAAAAAAAAAAAAAAAA\n");
  } else {
    printf("Parent\n");
    wait(NULL);
    printf("Parent resume\n");
    printf("Parent finished\n");
  }
}

void p8() {
  // Connect 2 forked process using pipe()
  // So apparently creating multiple forks is a very fiddly thing that can go out of hand very quick
  // 2 fork() does NOT result in just 2 child process. It results in 3 child process, totaling 4 processes including the parent.

  int pipes[2];
  if (pipe(pipes) == -1) {
    // Create 2 pipes
    // pipefd[0] being the read end
    // pipefd[1] being the write end
    perror("pipe");
    exit(EXIT_FAILURE);
  }

  __pid_t rc = fork(); // outputs to rc2
  /* __pid_t rc2 = fork(); // receive's input from rc */

  if (rc < 0) {
    perror("Fork");
    exit(EXIT_FAILURE);
  } else if (rc == 0) {

    /*
     * Receives input
     */

    char buf;

    printf("RC input\n");

    close(pipes[1]); // Close unused write end

    while (read(pipes[0], &buf, 1) > 0) {
      write(STDOUT_FILENO, &buf, 1);
    }

    write(STDOUT_FILENO, "\n", 1);
    close(pipes[0]);
    exit(EXIT_SUCCESS);
  } else {
    /*
          Outputs
    */
    printf("RC output\n");

    close(pipes[0]); // close unused read end

    char *data = "tucking hell";
    write(pipes[1], data, strlen(data));

    close(pipes[1]);

    wait(NULL);

    exit(EXIT_SUCCESS);
  }
}

int main() {
  p8();
  return 0;
}
