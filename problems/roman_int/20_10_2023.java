
class Solution {
    private static int mapping[] = new int[100];
    private static int flag_mapping[] = new int[100];
    private static char flag;

    private static void set_flag(char a) {
        switch(a) {
        case 'C':
            flag_mapping['D'] = 10;
            flag_mapping['M'] = 10;
            flag = a;
            break;
        case 'I':
            flag_mapping['V'] = 1;
            flag_mapping['X'] = 1;
            flag = a;
            break;
        case 'X':
            flag_mapping['L'] = 100;
            flag_mapping['C'] = 100;
            flag = a;
            break;
        default:
            flag = 0;
            break;
        }
    }

    private static void clear_flags() {
        flag_mapping['D'] = 0;
        flag_mapping['M'] = 0;
        flag_mapping['V'] = 0;
        flag_mapping['X'] = 0;
        flag_mapping['L'] = 0;
        flag_mapping['C'] = 0;
    }

    public Solution() {
        mapping['I'] = 1;
        mapping['V'] = 5;
        mapping['X'] = 10;
        mapping['L'] = 50;
        mapping['C'] = 100;
        mapping['D'] = 500;
        mapping['M'] = 1000;
        clear_flags();
    }

    public int romanToInt(String s) {
        char[] ch = s.toCharArray();

        int sum = 0;
        int current;
        char current_ch;
        
        for(int i = 0; i < ch.length; i++) {
            current_ch = ch[i];
            current = mapping[current_ch];
            if (flag_mapping[current_ch] == 0) {
                set_flag(current_ch);
            } else {
                current -= mapping[flag];
                sum -= mapping[flag];
                clear_flags();
            }
            sum += current;
        }
        
        return sum;
    }
}
