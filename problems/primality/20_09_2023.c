#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int is_integer(float n) {
	int truncated = (int) n;
	return truncated == n;
}

int main(int argc, char** argv) {
	if (argc < 2) {
		printf("Usage: [number]\n");
		return 1;
	}

	float n = (float)atoi(argv[1]);
	float div;

	for (float i = 2; i <= ceilf(sqrtf(n)); i++) {
		div = n / i;
		printf("%f / %f = %f\n", n, i, div);
		if (is_integer(div)) {
			printf("%f is not prime\n", n);
			return 0;
		}
	}

	printf("%d is prime\n", (int)n);

	return 0;
}
