#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#define COUNT 5

int is_integer(float n) {
	int truncated = (int) n;
	return truncated == n;
}

void print(long* items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

uint64_t ipow(int a, int b) {
	uint64_t result = a;
	for (int i = 1; i < b; i++) {
		result = result * a;
	}
	return result;
}

int main(int argc, char** argv) {
	if (argc < 2) {
		printf("Usage: [number]\n");
		return 1;
	}

	srand(69420);

	long in = atol(argv[1]);
	float n = (float) in;

	long nums[COUNT];
	int num;
	int count = 0;
	while(count < COUNT) {
		num = (rand() % in);
		if (num == 0 || is_integer(n / num)) {
			continue;
		}
		nums[count++] = num;
	}
	printf("Nums: ");
	print(nums, COUNT);

	long result;
	double powed;
	uint64_t ipowed;

	ipowed = UINT64_MAX;
	printf("MAX: %llu\n", ipowed);


	for (int i = 0; i < count; i++) {
		powed = powl(nums[i], in - 1);
		
		printf("%ld^%ld mod %ld\n", nums[i], in - 1, in);
		ipowed = ipow(nums[i], in - 1);

		printf("Double Powed %f\n", powed);
		printf("Powed %llu\n", ipowed);
		result = ipowed % in;
		printf("Result: %ld\n", result);


		if (result != 1) {
			printf("%ld is not prime\n", in);
			return 0;
		}
	}
	powf
	printf("%ld is prime\n", in);

	return 0;
}
k
