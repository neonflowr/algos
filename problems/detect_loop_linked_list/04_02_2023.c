#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List  {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

list *search(list *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;
  return search(l->next, value);
}

int has_loop(list *l) {
  list *tortoise = l;
  list *hare = l;

  while(hare->next != NULL) {
    tortoise = tortoise->next;
    hare = hare->next;
    if (hare != NULL && hare->next != NULL) {
      hare = hare->next;
	}
	if (hare == tortoise)
	  break;
  }

  printf("Hare: %d\n", hare->data);
  printf("Tortoise: %d\n", tortoise->data);

  return hare == tortoise;
}

void print(list *l) {
  int count = 0;
  while (l != NULL && count < 100) {
	printf("%d ", l->data);
	l = l->next;
	count++;
  }
  printf("\n");
}
