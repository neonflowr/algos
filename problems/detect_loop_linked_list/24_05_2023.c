#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
	int data;
	struct List* next;
} list;
void insert(struct List** head, int data) {
	struct List* node = malloc(sizeof(struct List));
	node->data = data;
	node->next = *head;
	*head = node;
}

struct List* search(struct List* l, int val) {
	if (l == NULL)
		return NULL;
	if (l->data == val)
		return l;
	return search(l->next, val);
}

int has_loop(struct List* l) {
	struct List* fast = l;
	struct List* slow = l;

	while (fast->next || slow != fast) {
		fast = fast->next ? fast->next : fast;
		fast = fast->next ? fast->next : fast;
		slow = slow->next;
	}

	return slow == fast;
}

void print(struct List* l) {
	while (l) {
		printf("%d ", l->data);
		l = l->next;
	}
	printf("\n");
}

