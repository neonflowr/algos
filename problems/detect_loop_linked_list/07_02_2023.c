#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **l, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;
  node->next = *l;
  *l = node;
}

list * search(list *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;
  return search(l->next, value);

}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}

int has_loop(list *l) {
  list *slow = l;
  list *fast = l;

  while(fast->next != NULL) {
	printf("Fast: %d\n", fast->data);
	fast = fast->next;
	if (fast->next != NULL) {
          printf("Fast next: %d\n", fast->data);
          fast = fast->next;
	} else {
	  break;
	}
	slow = slow->next;

	if (fast == slow)
	  break;
  }

  return fast == slow;
}
