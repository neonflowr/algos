#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

list *search(list *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;
  return search(l->next, value);
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}

int has_loop(list *l) {
  list *fast = l;
  list *slow = l;

  while(fast->next != NULL) {
	fast = fast->next;
	if (fast->next) {
	  fast = fast->next;
	} else {
	  break;
	}
	slow = slow->next;

	if (fast == slow) {
	  break;
	}
  }

  return fast == slow;
}
