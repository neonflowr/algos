#include "init.h"
#include "16_02_2023.c"
#include <stdio.h>
#include <assert.h>


int main(int argc, char **argv) {
  list **head;
  *head = NULL;

  insert(head, 1);
  insert(head, 2);
  insert(head, 3);
  insert(head, 4);
  insert(head, 5);
  insert(head, 6);
  insert(head, 7);

  // Create loop
  list *l1 = search(*head, 5);
  list *l2 = search(*head, 6);
  list *l3 = search(*head, 4);
  l1->next = l2;

  int looping = has_loop(*head);
  assert(looping);

  if (looping) {
	printf("Has loop\n");
  } else {
    printf("No loop\n");
  }

  l1->next = l3;

  looping = has_loop(*head);
  assert(!looping);

  print(*head);

  return 0;
}
