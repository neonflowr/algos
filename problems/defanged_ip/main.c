// 29/05/2023
// https://leetcode.com/problems/defanging-an-ip-address/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* defangIPaddr(char* address) {
	char* numbers[4];
	int i = 0, j = 0;
	char *buf = malloc(sizeof(char) * 4); 

	int total_length = 10;

	while(*address != '\0') {
		if (*address != '.') {
			// add to buf
			buf[j++] = *address;
		} else {
			// process buf 
			buf[j] = '\0';
			numbers[i] = malloc(sizeof(char) * (j + 1));
			total_length += j;

			for (int k = 0; k <= j; k++) {
				numbers[i][k] = buf[k];
			}

			j = 0;
			i++;
		}
		address++;
	}

	// deal with the last number
	buf[j] = '\0';
	numbers[i] = malloc(sizeof(char) * (j + 1));
	total_length += j;

	for (int k = 0; k <= j; k++) {
		numbers[i][k] = buf[k];
	}

	// got 4 numbers in the numbers array
	// now just to create a string containing those with defanged periods

	char *final = malloc(sizeof(char) * total_length);
	final[0] = '\0'; // because strcat starts from the null terminator

	char *defanged = "[.]";

	for (i = 0; i < 3; i++) {
		strcat(final, numbers[i]);
		strcat(final, defanged);
		free(numbers[i]);
	}
	strcat(final, numbers[i]);
	free(numbers[i]);

	free(buf);

	return final;
}

