#include "init.h"
#include "04_02_2023.c"
#include <stdio.h>
#include <assert.h>


void print2DUtil(struct Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print(struct Tree *t) {
  if (t != NULL)
    print2DUtil(t, 0);
  else
    printf("Empty\n");
}

int main() {
  printf("Compare tree\n");

  tree *i = malloc(sizeof(tree));
  i->data = 2;

  insert(&i, 3, NULL);
  insert(&i, 1, NULL);

  tree *j = malloc(sizeof(tree));
  j->data = 2;
  insert(&j, 3, NULL);
  insert(&j, 1, NULL);

  printf("i tree\n");
  print(i);
  printf("j tree\n");
  print(j);

  int is_identical_val = is_identical(i, j);

  assert(is_identical_val == 1);

  if (is_identical_val == 0)
    printf("Is different\n");
  else
    printf("Identical tree\n");

  insert(&j, 4, NULL);

  printf("i tree\n");
  print(i);
  printf("j tree\n");
  print(j);

  is_identical_val = is_identical(i, j);

  assert(is_identical_val == 0);

  if (is_identical_val == 0)
    printf("Is different\n");
  else
    printf("Identical tree\n");

  tree *k = malloc(sizeof(tree));
  k->data = 1;
  insert(&k, 2, NULL);
  insert(&k, 3, NULL);

  printf("i tree\n");
  print(i);
  printf("k tree\n");
  print(k);

  printf("Compare i & k trees\n");
  is_identical_val = is_identical(i, k);

  assert(is_identical_val == 0);

  if (is_identical_val == 0)
    printf("Is different\n");
  else
    printf("Identical tree\n");

  return 0;
}
