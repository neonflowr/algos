#include <stdio.h>
#include <stdlib.h>

typedef struct Tree {
  int data;
  struct Tree *left;
  struct Tree *right;
  struct Tree *parent;
} tree;

void insert(tree **x, int value, tree *parent) {
  if (*x == NULL) {
	tree *t = malloc(sizeof(tree));
	t->data = value;
	t->left = NULL;
	t->right = NULL;
	t->parent = NULL;
	*x = t;
	return;
  }

  if (value < (*x)->data)
	insert( &((*x)->left), value, *x);
  else
    insert(&((*x)->right), value, *x);
}

int is_identical(tree *x, tree *y) {
  if (x == NULL && y == NULL)
	return 1;

  if (x == NULL || y == NULL)
	return 0;

  return x->data == y->data && is_identical(x->left, y->left) && is_identical(x->right, y->right);
}


void print2DUtil(struct Tree *root, int space) {
  if (root == NULL)
    return;

  space += 10;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = 10; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print(struct Tree *t) {
  if (t != NULL)
    print2DUtil(t, 0);
  else
    printf("Empty\n");
}
