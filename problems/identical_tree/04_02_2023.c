#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct Tree {
  int data;
  struct Tree *parent;
  struct Tree *left;
  struct Tree *right;
} tree;

void insert(tree **t, int value, tree *parent) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->left = NULL;
	node->right = NULL;
	node->parent = parent;
	*t = node;
	return;
  }

  if (value < (*t)->data) {
	insert(&((*t)->left), value, *t);
  } else {
    insert(&((*t)->right), value, *t);
  }
}

int is_identical(tree *x, tree *y) {
  if (x == NULL && y == NULL)
	return 1;
  if (x == NULL || y == NULL)
	return 0;

  return x->data == y->data && is_identical(x->left, y->left) && is_identical(x->right, y->right);
}
