#include <stdio.h>
#include <stdlib.h>


#define COUNT 10


typedef struct tree {
  int data;
  struct tree *parent;
  struct tree *left;
  struct tree *right;
} tree;

tree *search(tree *t, int value) {
  if (t == NULL)
	return NULL;
  if (t->data == value)
	return t;

  if (value > t->data)
	return search(t->right, value);
  else
	return search(t->left, value);
}

void insert(tree **t, int value, tree *parent) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->left = NULL;
	node->right = NULL;
	node->parent = parent;
	*t = node;
	return;
  }

  if (value > (*t)->data)
	return insert(&((*t)->right), value, *t);
  else
	return insert(&((*t)->left), value, *t);
}


void traverse(tree *t, void (*process_item)(tree *t)) {
  if (t != NULL) {
	traverse(t->left, process_item);
	(*process_item)(t);
	traverse(t->right, process_item);
  }
}

tree *min(tree *t) {
  if (t == NULL) {
	return NULL;
  }

  tree *node = t;
  while(node->left != NULL) {
	node = node->left;
  }

  return node;
}

tree *max(tree *t) {
  if (t == NULL)
	return NULL;

  tree *node = t;
  while(node->right != NULL) {
	node = node->right;
  }

  return node;
}


tree *delete(tree *t, int value) {
  if (t == NULL)
	return NULL;

  if (value < t->data)
	t->left = delete(t->left, value);
  else if (value > t->data)
	t->right = delete(t->right, value);
  else {
    printf("Delete %d\n", t->data);
    if (t->left == NULL) {
      tree *tmp = t->right;
      free(t);
      return tmp;
	} else if (t->right == NULL) {
	  tree *tmp = t->left;
	  free(t);
	  return tmp;
	}

	tree* next = min(t->right);
	t->data = next->data;
	t->right = delete(t->right, next->data);
  }
  return t;
}

void print2DUtil(struct tree *root, int space) {
  if (root == NULL)
    return;

  space += COUNT;

  print2DUtil(root->right, space);

  printf("\n");
  for (int i = COUNT; i < space; i++)
    printf(" ");
  printf("%d\n", root->data);

  print2DUtil(root->left, space);
}

void print2D(struct tree *root) {
  if (root != NULL)
    print2DUtil(root, 0);
  else
    printf("Empty\n");
}

void compare(tree *i, tree *j, int *is_different) {
  if (i == NULL || j == NULL) {
	if (i != NULL || j != NULL) {
	  *is_different = 1;
	}
	return;
  }
  if (i->data != j->data) {
	*is_different = 1;
  }
  compare(i->left, j->left, is_different);
  compare(i->right, j->right, is_different);
}

int main() {
  printf("Compare tree\n");

  tree *i = malloc(sizeof(tree));
  i->data = 2;

  insert(&i, 3, NULL);
  insert(&i, 1, NULL);

  tree *j = malloc(sizeof(tree));
  j->data = 2;
  insert(&j, 3, NULL);
  insert(&j, 1, NULL);

  printf("i tree\n");
  print2D(i);
  printf("j tree\n");
  print2D(j);

  int is_different = 0;
  compare(i, j, &is_different);

  if (is_different == 1)
	printf("Is different\n");
  else
    printf("Identical tree\n");

  return 0;
}
