#include <string>
#include <stdio.h>
using namespace std;
class Solution {
public:
    bool squareIsWhite(string coordinates) {
      return (coordinates[1] - 0x60) % 2 == 0 ? (coordinates[0] - 0x30) % 2 != 0 : (coordinates[0] - 0x30) % 2 == 0;
    }
};
