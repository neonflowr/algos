#include <stdio.h>
#define MAXLINE 1000

int kr_getline(char line[], int max);
int strindex(char source[], char searchfor[]);
int strrindex(char source[], char searchfor[]);

char pattern[] = "an";

int main() {
  char line[MAXLINE];
  int found = 0;

  printf("Finding matching lines for pattern: %s\n", pattern);

  while (kr_getline(line, MAXLINE) > 0) {
    int rightmost_occurence = strrindex(line, pattern);
    if (rightmost_occurence >= 0) {
      printf("Matched rightmost occurence: %d for line %s",
             rightmost_occurence, line);
      found++;
    }
  }

  printf("Number of matching lines is: %d\n", found);

  return found;
}

// get line into s and return length
int kr_getline(char s[], int lim) {
  int c, i;
  i = 0;

  // Read stdin into s
  while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
    s[i++] = c;

  if (c == '\n')
    s[i++] = c;

  s[i] = '\0';

  return i;
}

// Return index of t in s, -1 if none
int strindex(char s[], char t[]) {
  int i, j, k;

  for (i = 0; s[i] != '\0'; i++) {
    for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
      ;
    if (k > 0 && t[k] == '\0') {
      return i;
    }
  }
  return -1;
}

// Return the index of the rightmost occurence of t in s, -1 if none
int strrindex(char s[], char t[]) {
  int i, j, k;

  int r_index = -1;

  for (i = 0; s[i] != '\0'; i++) {
    for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
      ;
    if (k > 0 && t[k] == '\0') {
      r_index = i;
    }
  }

  return r_index;
}
