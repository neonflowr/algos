/*
  2月6日2023年
  https://leetcode.com/problems/palindrome-number/description/
*/

#include <stdbool.h>

bool isPalindrome(int x) {
  if (x < 0)
    return false;
  if (x < 10)
    return true;

  long int counter = 1;
  int i = 1;
  while (counter <= x) {
    counter = counter * 10;
    i++;
  }
  int digit = i - 1;

  long int j = 10;
  int is_palindrome = 1;
  while (counter > j) {
    long int current_num = x % counter;
    long int rightstuff = x % (counter / 10);
    int leftdigit = (current_num - rightstuff) / (counter / 10);

    long int current_num_right = x % j;
    long int rightstuff_right = x % (j / 10);
    int rightdigit = (current_num_right - rightstuff_right) / (j / 10);

    j = j * 10;
    counter = counter / 10;

    if (leftdigit != rightdigit) {
      is_palindrome = 0;
      break;
    }
  }

  return is_palindrome;
}
