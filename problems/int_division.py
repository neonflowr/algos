"""
Integer division without using / or * operators.

Still can't specify number of decimal points though.
"""

max_decimal_iteration: int = 1

def divide(x: int, y: int, iteration: int = 0) -> float:
    if x == 0: raise Exception()

    # Find the largest x up until y
    largest = 0
    rem = None

    division_value = 0

    while True:
        largest += x

        rem = y - largest

        if rem == 0:
            division_value += 1
            return division_value
        if rem < 0:
            # The value before largest overtook y
            largest -= x
            rem = y - largest

            i = iteration
            decimal_point = None
            while rem != 0 and i < max_decimal_iteration:
                i += 1
                incremented_rem = rem
                for _ in range(9):
                    incremented_rem += rem
                decimal_point = divide(x, incremented_rem, i)

            return f"{division_value}.{decimal_point}" if decimal_point else division_value
        division_value += 1

input = (3, 33333333)
print(f"divide {input}")
print(divide(*input))
