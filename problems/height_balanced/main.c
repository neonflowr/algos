/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
#include <math.h>

struct TreeNode {
	int val;
	struct TreeNode* left;
	struct TreeNode* right;
};


int find_height(struct TreeNode* tree, int level) {
	if (!tree)
		return level;

	level++;

	if (!tree->left && !tree->right) {
		return level;
	}

	return fmax(find_height(tree->left, level), find_height(tree->right, level));
}


void check(struct TreeNode* tree, int *balanced) {
	if (!tree)
		return;

	if (*balanced == 0)
		return;

	int left = find_height(tree->left, 0);
	int right = find_height(tree->right, 0);
	if (fabs(left - right) > 1) {
		*balanced = 0;
		return;
	}
	else {
		check(tree->left, balanced);
		check(tree->right, balanced);
	}
}

int isBalanced(struct TreeNode* root) {
	int balanced = 1;
	check(root, &balanced);
	return balanced;
}

