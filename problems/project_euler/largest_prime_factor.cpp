#include <iostream>
#include <stdio.h>
#include <math.h>

using namespace std;

bool is_prime(long long x) {
  long long limit = (long long)sqrt(x);
  for(long long i = 2; i <= limit; i++) {
    if (x % i == 0) return false;
  }
  return true;
}

int main() {
  long long max   = 600851475143;
  long long limit = (long long)sqrt(max);
  long long result = -1;
  for(long long i = 2; i <= limit; i++) {
    if (max % i == 0) {
      printf("%lld divides\n", i);
      if (is_prime(i)) {
	printf("new max %lld\n", i);
	result = i;
      }
    }
  }
  printf("max %lld\n", result);
    
  return 0;
}
