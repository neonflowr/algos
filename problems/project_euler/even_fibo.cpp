#include <iostream>
#include <stdio.h>

using namespace std;

int main() {
  long long prev1, prev2;
  prev1 = 0;
  prev2 = 1;
  long long max = 4000000;
  long long sum = 0;
  long long num = 1;

  // for(int i = 1; i < max; i++) {
  while(prev1 + prev2 < max) {
    num   = prev1 + prev2;
    printf("%lld ", num);
    if (num % 2 == 0) {
      // printf("even num %lld\n", num);
      sum += num;
    }
    prev1 = prev2;
    prev2 = num;
  }

  printf("Even sum: %lld\n", sum);

  return 0;
}
