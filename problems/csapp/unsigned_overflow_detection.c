#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int uadd_ok_attempt(unsigned x, unsigned y) {
	// Returns 1 if x + y <= 2^33
	// unsigned = 32 bits
	// If 2^32 <= sum < 2^33

	unsigned max = 0xFFFFFFFF;

	unsigned diff = max - x;
	if (y < diff) {
		printf("can add\n");
		return 1;
	}

	printf("cannot add\n");

	return 0;
}

int uadd_ok(unsigned x, unsigned y) {
	unsigned sum = x + y;
	return sum >= x;
}

int main() {
	unsigned x, y;

	printf("Input x, y:\n");
	scanf("%d %d", &x, &y);
	uadd_ok(x, y);

	return 0;
}

