#include <stdio.h>
#include <stdint.h>

int tadd_ok(int x, int y) {
	int8_t sum = x + y;
	printf("Sum signed %d\n", sum);
	printf("SUm smaller than 0: %d %d %d\n", x > 0, y > 0, sum <= 0);
	if (x > 0 && y > 0 && sum <= 0) {
		printf("Positive overflow\n");
		return 0;
	}
	if (x < 0 && y < 0 && sum >= 0) {
		printf("Negative overflow\n");
		return 0;
	}
	return 1;
}

int tsub_ok(int x, int y) {
	int sum = x + y;
	if (x > 0 && y < 0 && sum < 0)
		return 0;
	if (x < 0 && y > 0 && sum > 0)
		return 0;
	return 1;
}

int tmult_ok(int x, int y) {


	int p = x * y;
	return !x || p / x == y;
}

int main() {
	int8_t x, y;
	x = -126;
	y = -5;
	printf("x: %d\n", x);
	printf("y: %d\n", y);
	if (!tadd_ok(x, y)) {
		printf("overflow\n");
	}
	else {
		printf("not overflow\n");
	}

	getchar();

	return 0;
}

