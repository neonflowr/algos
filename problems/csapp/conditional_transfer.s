	.file	"conditional_transfer.c"
	.text
	.globl	_test
	.def	_test;	.scl	2;	.type	32;	.endef
_test:
LFB0:
	.cfi_startproc
	movl	4(%esp), %edx
	movl	8(%esp), %ecx
	testw	%dx, %dx
	js	L6
	cmpw	$10, %cx
	jle	L7
	movswl	%dx, %eax
	movswl	%cx, %ecx
	cltd
	idivl	%ecx
L1:
	rep ret
L6:
	movl	%ecx, %eax
	orl	%edx, %eax
	cmpw	%dx, %cx
	jle	L1
	movl	%edx, %eax
	imull	%ecx, %eax
	ret
L7:
	leal	12(%ecx), %eax
	ret
	.cfi_endproc
LFE0:
	.ident	"GCC: (MinGW.org GCC-6.3.0-1) 6.3.0"
