#include <stdio.h>
#include <stdint.h>

int32_t div16(int32_t x) {
	// 2 cases
	// when x is negative, we need to have a bias
	// but we can only detect if x is negative using bit operations


	// x < 0 => ( x + (1 << k) - 1 ) >> k
	// (x + 15) >> k
	// If x > 0, we want to get rid of the 15
	// If x < 0, the 32th bit is gonna be 1

	// We know x has 32 bits


	// we know that (1 << 4) - 1 = 15 is gonna be the bias

	// If we 

	int bias = x >> 31 & 0xF;
	return (x+bias) >> 4;
}

int main() {
	int32_t x = -32;

	printf("Result of 16 division: %d\n", div16(x));
	getchar();
	return 0;
}

