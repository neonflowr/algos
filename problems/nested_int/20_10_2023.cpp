// 2023�N10��20��
// https://leetcode.com/problems/flatten-nested-list-iterator/?envType=daily-question&envId=2023-10-20

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */

class NestedIterator {
private:
  vector<int> nums;
  int iter = 0;
public:
  void add_num(NestedInteger num) {
    if (num.isInteger()) {
      this->nums.push_back(num.getInteger());
    } else {
      vector<NestedInteger> sublist = num.getList();
      int n = sublist.size();
      for(int i = 0; i < n; i++) {
	add_num(sublist[i]);
      }
    }
  }

  NestedIterator(vector<NestedInteger> &nestedList) {
    int n = nestedList.size();
    for(int i = 0; i < n; i++) {
      add_num(nestedList[i]);
    }
  }
    
  int next() {
    return this->nums[this->iter++];
  }
    
  bool hasNext() {
    return this->iter < this->nums.size();
  }
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */
