#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

class Solution {
public:
  bool repeatedSubstringPattern(string s) {
    // brute force
    int n = s.length();
    char pattern[n];

    for (int i = 0; i < n; i++) {
      // Create pattern
      for (int k = 0; k <= i; k++) {
	pattern[k] = s[k];
      }

      int j = i + 1;
      int len = j;
      while(j <= n - len) {
	int k = 0;
	while(k < len) {
	  if(s[j] == pattern[k]) {
	    j++;
	    k++;
	  } else {
	    break;
	  }
	}
	if (k != len) break;
	if (j == n) return true;
      }
    }

    return false;
  }
};

