#include <iostream>
#include <vector>

using namespace std;

class Solution {
  void print(vector<int> items) {
    for (int i = 0; i < items.size(); i++) {
      cout << items[i] << " ";
    }
    cout << "\n";
  }

  inline bool is_solution(vector<int> &a, int target) { return target == 0; }

  void backtrack(vector<vector<int>> &result, vector<int> &candidates,
                 vector<int> &a, int target, int k) {
    if (this->is_solution(a, target)) {
      // check for perms
      cout << "Found solution\n";
      print(a);
      result.push_back(a);
    } else {
      int i = k;
      int n = candidates.size();
      while (i < n) {
        if (target >= candidates[i]) {
            a.push_back(candidates[i]);
            backtrack(result, candidates, a, target - candidates[i], i);
            a.pop_back();
        }
        i++;
      }
    }
  }

public:
  vector<vector<int>> combinationSum(vector<int> &candidates, int target) {
    vector<vector<int>> result;
    vector<int> a;
    backtrack(result, candidates, a, target, 0);
    return result;
  }
};

int main() {
  Solution *s = new Solution();
  vector<int> candidates = {8, 7, 4, 3};
  int target = 11;
  s->combinationSum(candidates, target);
  return 0;
}
