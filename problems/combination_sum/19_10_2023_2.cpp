#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
  void print(vector<int> items) {
    for (int i = 0; i < items.size(); i++) {
      cout << items[i] << " ";
    }
    cout << "\n";
  }

  inline bool is_solution(vector<int> &a, int target) { return target == 0; }

  void backtrack(vector<vector<int>> &result, vector<int> &candidates,
                 vector<int> &a, int target, int k) {
    if (this->is_solution(a, target)) {
      cout << "Found solution\n";
      print(a);
      result.push_back(a);
    } else {
      int n, i;
      i = k;
      n = candidates.size();
      int move;
      while(i < n) {
        if (target >= candidates[i]) {
          move = candidates[i];
          a.push_back(move);
          backtrack(result, candidates, a, target - move, i+1);
          a.pop_back();
        }
        i++;
      }
      
      // for (i = k; i < n; i++) {
      //   if (target >= candidates[i]) {
      //     moves.push_back(candidates[i]);
      //   }
      // }

      // n = moves.size();
      // for (i = 0; i < n; i++) {
      //   a.push_back(moves[i]);
      //   k++;
      //   backtrack(result, candidates, a, target - moves[i], k);
      //   a.pop_back();
      // }
    }
  }

  // void remove_duplicate(vector<int> items, vector<int> &result) {
  //   unordered_map<int, int> m;
  //   int n = items.size();
  //   for (int i = 0; i < n; i++) {
  //     if (!m[items[i]]) {
  //       m[items[i]] = 1;
  //       result.push_back(items[i]);
  //     }
  //   }
  // }

public:
  vector<vector<int>> combinationSum2(vector<int> &candidates, int target) {
    vector<vector<int>> result;
    vector<int> a;
    sort(candidates.begin(), candidates.end());
    print(candidates);
    backtrack(result, candidates, a, target, 0);
    return result;
  }
};

int main() {
  Solution *s = new Solution();
  vector<int> candidates = {10, 1, 2, 7, 6, 1, 5};
  int target = 8;
  s->combinationSum2(candidates, target);
  return 0;
}
