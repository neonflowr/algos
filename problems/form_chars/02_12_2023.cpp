#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

class Solution {
public:
    int countCharacters(vector<string>& words, string chars) {
      int cmap[255] = {0};

      for(auto s : chars) {
	cmap[s]++;
      }

      int result = 0;
      int n = chars.size();

      for(auto w : words) {
	int local_map[255] = {0};

	bool is_good = true;

	if (w.size() > n) continue;
	
	for(char c : w) {
	  if (cmap[c] <= 0 || local_map[c] + 1 > cmap[c]) {
	    is_good = false;
	    break;
	  }
	  local_map[c]++;
	}

	if (is_good) {
	  result += w.size();
	}
      }

      return result;
    }
};
