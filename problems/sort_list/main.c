#include "init.h"
#include "25_02_2023.c"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int main(int argc, char *argv[]) {
  printf("マージソートをテスト中\n");
  struct List **head = malloc(sizeof(list *));
  *head = NULL;

  insert(head, 234);
  insert(head, 324);
  insert(head, 31);
  insert(head, 1323);
  insert(head, 123);
  insert(head, 423);
  insert(head, 533);
  insert(head, 5);
  insert(head, 6);
  insert(head, 12);
  insert(head, 214);
  insert(head, 1);
  insert(head, 3);
  insert(head, 2);
  insert(head, 4);

  printf("Initial list\n");
  print(*head);

  assert((*head)->data == 4);

  sort(head);

  printf("Sorted list\n");
  print(*head);

  assert((*head)->data == 1);

  free(*head);
  *head = NULL;

  free(head);

  return 0;
}
