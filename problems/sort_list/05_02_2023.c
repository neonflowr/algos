// 2月5日2023年
// リートコードの問題を解いてみる
// https://leetcode.com/problems/sort-list/

#include <stdlib.h>
#include <stdio.h>

struct ListNode {
  int val;
  struct ListNode *next;
};

struct ListNode *find_middle(struct ListNode *head) {
  struct ListNode *tortoise = head;
  struct ListNode *hare = head;

  while(hare->next) {
	hare = hare->next;
	if (hare->next)
	  hare = hare->next;
	else
	  break;
	tortoise = tortoise->next;
  }

  return tortoise;
}

struct ListNode *merge(struct ListNode *list1, struct ListNode *list2) {
  struct ListNode *i;

  if (!list1)
	return list2;
  else if (!list2)
	return list1;

  if (list1->val < list2->val) {
	i = list1;
	i->next = merge(list1->next, list2);
  } else {
	i = list2;
	i->next = merge(list1, list2->next);
  }

  return i;
}

struct ListNode *sortList(struct ListNode *head) {
  if (head == NULL || head->next == NULL)
	return head;

  // Split the list into 2
  struct ListNode *list1, *list2;
  struct ListNode *middle = find_middle(head);
  list1 = head;
  list2 = middle->next;
  middle->next = NULL;

  list1 = sortList(list1);
  list2 = sortList(list2);

  return merge(list1, list2);
}
