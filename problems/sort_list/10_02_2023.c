#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

void print(list *head) {
  while(head != NULL) {
	printf("%d ", head->data);
	head = head->next;
  }
  printf("\n");
}

list *find_middle(list *l) {
  list *fast = l;
  list *slow = l;

  while(fast->next != NULL) {
	fast = fast->next;
	if (fast->next != NULL)
	  fast = fast->next;
	else
	  break;
	slow = slow->next;
  }

  return slow;
}

list *merge(list *x, list *y) {
  if (x == NULL)
	return y;
  if (y == NULL)
	return x;

  if (x->data < y->data) {
	x->next = merge(x->next, y);
	return x;
  } else {
    y->next = merge(x, y->next);
    return y;
  }
}

void sort(list **head) {
  if (*head == NULL || (*head)->next == NULL)
	return;

  // Split list into 2
  list *middle = find_middle(*head);
  list *l1 = *head;
  list *l2 = middle->next;
  middle->next = NULL;

  sort(&l1);
  sort(&l2);

  *head = merge(l1, l2);
}
