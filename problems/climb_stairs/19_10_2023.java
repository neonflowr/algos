class Solution {
    public int climbStairs(int n) {
        int pprev, prev, current = 0;
        pprev = 0;
        prev = 1;

        for(int i = 0; i < n; i++) {
            current = pprev + prev;
            pprev = prev;
            prev = current;
        }
        
        return current;
    }
}
