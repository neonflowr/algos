// 2/6/2023
// https://leetcode.com/problems/detonate-the-maximum-bombs/

#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

#define MAXV 100

typedef struct Graph {
	int edges[MAXV][MAXV];
	int degree[MAXV];
	int nedges;
	int nvertices;
	bool directed;
} graph;

graph* create(bool directed, int n) {
	graph* g = malloc(sizeof(graph));
	g->nedges = 0;
	g->nvertices = n;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			g->edges[i][j] = 0;
		}
	}
	g->directed = directed;
	return g;
}

bool has_connection(graph* g, int x, int y) {
	return g->edges[x][y];
}

void insert_edge(graph* g, int x, int y, bool directed) {
	g->edges[x][y] = 1;
	if (!directed) {
		insert_edge(g, y, x, true);
	}
	else {
		g->nedges++;
		g->degree[x]++;
	}
}

double find_distance(int* bomb1, int* bomb2) {
	double x = fabs(bomb1[0] - bomb2[0]);
	double y = fabs(bomb1[1] - bomb2[1]);
	return sqrt(x * x + y * y);
}

void dfs_subroutine(graph* g, int v, bool* discovered, bool* processed, int* parent, int* depth) {
	discovered[v] = true;
	*depth = *depth + 1;

	int edge;
	for (int i = 0; i < g->nvertices; i++) {
		edge = g->edges[v][i];
		if (edge == 0) continue;
		if (!discovered[i]) {
			parent[i] = v;
			dfs_subroutine(g, i, discovered, processed, parent, depth);
		}
	}

	processed[v] = true;
}

int* dfs(graph* g) {
	int* depth = calloc(g->nvertices, sizeof(int));

	for (int i = 0; i < g->nvertices; i++) {
		bool discovered[MAXV] = { false };
		bool processed[MAXV] = { false };
		int parent[MAXV] = { 0 };

		if (!discovered[i]) {
			dfs_subroutine(g, i, discovered, processed, parent, &depth[i]);
		}
	}
	return depth;
}

int maximumDetonation(int** bombs, int bombsSize, int* bombsColSize) {
	graph* g = create(true, bombsSize);

	int x, y, z1, z2;

	double distance;

	for (int i = 0; i < bombsSize; i++) {
		for (int j = i + 1; j < bombsSize; j++) {
			x = bombs[i][0];
			y = bombs[i][1];
			z1 = bombs[i][2];
			z2 = bombs[j][2];

			distance = find_distance(bombs[i], bombs[j]);

			if (distance <= z1) {
				insert_edge(g, i, j, true);
			}
			if (distance <= z2) {
				insert_edge(g, j, i, true);
			}
		}
	}

	int* depth = dfs(g);
	int max = depth[0];

	for (int i = 1; i < g->nvertices; i++) {
		if (depth[i] > max) max = depth[i];
	}

	free(depth);
	free(g);

	return max;
}

