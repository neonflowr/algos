#include <stdlib.h>
#include <stdio.h>

int *twoSum(int *nums, int numsSize, int target, int *returnSize);

int main() {
  printf("Two sum testing...\n");

  int nums[4] = { 2, 7, 11, 15 };
  int numsSize = 4;
  int target = 9;

  int *returnSize;
  *returnSize = 2;

  int *result = twoSum(nums, numsSize, target, returnSize);

  if ((result)) {
    printf("Final return value:\n");
    printf("%d\n", result[0]);
    printf("%d\n", result[1]);
  } else {
    printf("NULL\n");
  }

  return 0;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *twoSum(int *nums, int numsSize, int target, int *returnSize) {
  // Remove numbers larger than target
  int validNums[numsSize];
  int validNumIndex = 0;

  for (int i = 0; i < numsSize; i++) {
    if (nums[i] < target) {
      validNums[validNumIndex++] = nums[i];
    }
  }

  printf("valid nums:\n");
  for (int i = 0; i < validNumIndex; i++) {
    printf("%d ", validNums[i]);
  }
  printf("\n");
  printf("valid num index: %d\n", validNumIndex);

  // Brute force
  int *result = malloc(sizeof(int[2]));

  for (int i = 0; i < validNumIndex; i++) {
    for (int j = i + 1; j < validNumIndex; j++) {
      printf("current i: %d\n", i);
      printf("current j: %d\n", j);

      printf("valid i: %d\n", validNums[i]);
      printf("valid j: %d\n", validNums[j]);
      if (validNums[i] + validNums[j] == target) {
        printf("Done\n");
        printf("Final i: %d\n", i);
        printf("Final j: %d\n", j);
        result[0] = i;
        result[1] = j;
        return result;
      }
    }
  }

  return NULL;
}
