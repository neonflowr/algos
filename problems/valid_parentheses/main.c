// 27/05/2023
// https://leetcode.com/problems/valid-parentheses/

#include <stdio.h>
#include <stdlib.h>

typedef struct List {
	int data;
	struct List* next;
} list;

void freel(list* l) {
	list* tmp;
	while (l) {
		tmp = l->next;
		free(l);
		l = tmp;
	}
}

enum Type {
	PAREN,
	BRACKET,
	SQUARE_BRACKET
};

typedef struct stack {
	list* top;
	int size;
} stack;

stack* creates() {
	stack* s = malloc(sizeof(stack));
	s->top = NULL;
	s->size = 0;
	return s;
}

void push(stack* s, int value) {
	list* node = malloc(sizeof(list));
	node->data = value;
	node->next = NULL;

	if (!s->top) {
		s->top = node;
		return;
	}
	node->next = s->top;
	s->top = node;
}


int is_empty(stack* s) {
	return s->top == NULL;
}
int pop(stack* s, int value) {
	if (is_empty(s) || s->top->data != value)
		return -1;

	list* tmp = s->top;

	s->top = tmp->next;
	int val = tmp->data;
	free(tmp);
	return val;
}

void frees(stack* s) {
	freel(s->top);
	free(s);
}

bool isValid(char* s) {
	stack* parens = creates();

	int is_valid = 1;

	while (*s != '\0' && is_valid) {
		switch (*s) {
		case '(':
			push(parens, 1);
			break;
		case ')':
			if (pop(parens, 1) == -1) {
				is_valid = 0;
			}
			break;
		case '[':
			push(parens, 2);
			break;
		case ']':
			if (pop(parens, 2) == -1) {
				is_valid = 0;
			}
			break;
		case '{':
			push(parens, 3);
			break;
		case '}':
			if (pop(parens, 3) == -1) {
				is_valid = 0;
			}
			break;
		default:
			break;
		}
		s++;
	}

	int result = is_valid && is_empty(parens);

	frees(parens);

	return result;
}

