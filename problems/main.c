#include <stdio.h>

int count_blank_spaces()
{
  int c, n, prev;

  while((c = getchar()) != '\n')
    {
    // ' ' instead of " " because single quotes converts the character to its integer representation (ASCII, etc.) and getchar() returns an int
      if (c == ' ')
      {
        if (prev != c)
          {
            ++n;
          }
      }
      prev = c;
    }

  return n;
}

void main()
{
  printf("hello world\n");
  printf("世界、こんにちは！！！\n");
  printf("%d is EOF\n", EOF);

  printf("Counting blank spaces\n");
  printf("Input has %d blank spaces\n", count_blank_spaces());
}
