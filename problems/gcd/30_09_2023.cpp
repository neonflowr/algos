#include <stdio.h>
#include <stdlib.h>

int gcd(int x, int y) {
	if (y == 0) return x;
	return gcd(y, x % y);
}

int lcm(int x, int y) {
	return (x * y) / gcd(x, y);
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		return 0;
	}
	int x, y, result;
	x = atoi(argv[1]);
	y = atoi(argv[2]);

	result = gcd(x, y);
	printf("GCD: %d\n", result);

	result = lcm(x, y);
	printf("LCM: %d\n", result);

	return 0;
}

