"""
Find the shortest cycle tour that visits each point in s with the nearest neighbor heuristic
"""

from random import randint

def _distance(p1: int, p2: int) -> int:
    return abs(p2 - p1)

def nearest_neighbor(s: list) -> list:
    # Start from a random point in the set
    p: int = s[randint(0, len(s) - 1)]

    i: int = 0

    route: list = [p]
    total_dist: int = 0

    while(i < len(s) - 1):
        # Find the shortest path to a neighboring point
        min: int = None
        next_point: int = None
        for j in s:
            if j != p and j not in route:
                d: int = _distance(p, j)
                if not min or d < min:
                    min = d
                    next_point = j

        # Go there
        p = next_point
        route.append(p)
        total_dist += min

        i += 1

    # Handle the cycle
    total_dist += _distance(route[-1], route[0])
    route.append(route[0])

    return route, total_dist
