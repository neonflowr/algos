#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class Solution {
private:
  inline bool valid(char t) {
    return (t >= 'a' && t <= 'z') || (t >= '0' && t <= '9');
  }
public:
  bool isPalindrome(string s) {
    int i = 0;
    int k = s.length() - 1;
    int result = 0;

    char ci, ck;

    while(i <= k) {
      ci = s[i] | ' ';
      ck = s[k] | ' ';

      if (!valid(ci)) [[unlikely]] {
	i++;
	continue;
      }
      if (!valid(ck)) [[unlikely]]{
	k--;
	continue;
      }

      result += ci ^ ck;

      i++;
      k--;
    }

    return result == 0;
  }
};

int main(int argc, char *argv[]) {
  if (argc < 2) {
    cout << "Not enough arguments\n";
    return 0;
  }

  string input = argv[1];

  cout << "Input: " << input << "\n";

  Solution* s = new Solution;
  cout << "Is palindrome: " << s->isPalindrome(input) << "\n";
  delete s;
  return 0;
}

