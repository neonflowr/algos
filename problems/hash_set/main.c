// 30/05/2023
// https://leetcode.com/problems/design-hashset/

#define TABLE_SIZE 1000001

typedef struct HashItem {
	int key;
	int value;
} hi;

typedef struct {
	hi items[TABLE_SIZE + 1];
	int capacity;
} MyHashSet;


int hash(int key) {
	return key % TABLE_SIZE;
}

int rehash(int key) {
	return (key + 1) % TABLE_SIZE;
}

int is_cell_empty(MyHashSet* t, int cell) {
	return t->items[cell].key == INT_MIN;
}

MyHashSet* myHashSetCreate() {
	MyHashSet* t = malloc(sizeof(MyHashSet));
	for (int i = 0; i < TABLE_SIZE + 1; i++) {
		t->items[i].key = INT_MIN;
		t->items[i].value = INT_MIN;
	}
	t->capacity = TABLE_SIZE + 1;
	return t;
}

void insert_subroutine(MyHashSet* t, int hashed, int key, int value) {
	if (is_cell_empty(t, hashed)) {
		t->items[hashed].key = key;
		t->items[hashed].value = value;
	}
	else {
		if (t->items[hashed].key == key) {
			t->items[hashed].value = value;
		}
		else {
			insert_subroutine(t, rehash(hashed), key, value);
		}
	}
}


void myHashSetAdd(MyHashSet* obj, int key) {
	int hashed = hash(key);
	insert_subroutine(obj, hashed, key, key);
}


void delete_subroutine(MyHashSet* t, int hashed, int key, int value) {
	if (is_cell_empty(t, hashed))
		return;

	if (t->items[hashed].key == key) {
		t->items[hashed].key = INT_MIN;
		t->items[hashed].value = INT_MIN;
	}
	else {
		delete_subroutine(t, rehash(hashed), key, value);
	}
}

void myHashSetRemove(MyHashSet* obj, int key) {
	int hashed = hash(key);

	delete_subroutine(obj, hashed, key, key);
}

bool get_subroutine(MyHashSet* t, int hashed, int key) {
	if (is_cell_empty(t, hashed)) {
		return false;
	}
	if (t->items[hashed].key == key) {
		return true;
	}
	return get_subroutine(t, rehash(hashed), key);
}

bool myHashSetContains(MyHashSet* obj, int key) {
	int hashed = hash(key);
	return get_subroutine(obj, hashed, key);
}

void myHashSetFree(MyHashSet* obj) {
	free(obj);
}

