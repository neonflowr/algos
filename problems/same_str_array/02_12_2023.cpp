#include <vector>
#include <string>
#include <stdio.h>

using namespace std;

class Solution {
public:
    bool arrayStringsAreEqual(vector<string>& word1, vector<string>& word2) {
      int h1, h2;
      h1 = h2 = 0;

      const int p = 53;
      const int m = 1e9 + 9;

      int n1 = word1.size(), n2 = word2.size();

      long sum   = 0;
      long ppow  = 1;
      for(int i = 0; i < n1; i++) {
	int n = word1[i].size();
	for(int k = 0; k < n; k++) {
	  sum = (sum + (word1[i][k] - 'a' + 1) * ppow) % m;
	  ppow = (ppow * p) % m;
	}
      }
      h1 = sum;

      sum   = 0;
      ppow  = 1;
      for(int i = 0; i < n2; i++) {
	int n = word2[i].size();
	for(int k = 0; k < n; k++) {
	  sum = (sum + (word2[i][k] - 'a' + 1) * ppow) % m;
	  ppow = (ppow * p) % m;
	}
      }
      h2 = sum;

      printf("H1 h2: %d %d\n", h1, h2);
        
      return h1 == h2;
    }
};
