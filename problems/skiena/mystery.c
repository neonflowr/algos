// Skiena chapter 2 exercise 2-2

// Function for this
// (n^3 + 3 * n^2 + 2n) / 3

#include <stdio.h>

int pesky(int n) {
  int r = 0;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= i; j++) {
      for (int k = j; k <= i + j; k++) {
        r++;
      }
    }
  }
  return r;
}

int main() {
  printf("2 return %d\n", pesky(2));
  printf("3 return %d\n", pesky(3));
  printf("4 return %d\n", pesky(4));
  printf("5 return %d\n", pesky(5));

  return 0;
}
