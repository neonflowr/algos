#include <stdio.h>
#include <unordered_map>
#include <vector>
#include <queue>
#include <utility>

using namespace std;

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
  void traverse(TreeNode* node, unordered_map<int,int>& mapping, vector<int>& distincts) {
    if (node) {
      traverse(node->left, mapping, distincts);
      if (mapping.find(node->val) == mapping.end()) {
	mapping[node->val] = 1;
	distincts.push_back(node->val);
      } else {
	mapping[node->val] = mapping[node->val] + 1;
      }
      traverse(node->right, mapping, distincts);
    }
  }
  
  vector<int> findMode(TreeNode* root) {
    unordered_map<int,int> mapping;

    vector<int> distincts;
    traverse(root, mapping, distincts);

    int n = distincts.size();

    if (n <= 1) return distincts;

    vector<int> results;

    priority_queue<pair<int, int>> q;

    for(auto &i : distincts) {
      q.push(make_pair(mapping[i], i));
    }

    auto max= q.top();
    results.push_back(get<1>(max));
    q.pop();

    while(!q.empty() && get<0>(q.top()) == get<0>(max)) {
      max = q.top();
      results.push_back(get<1>(max));
      q.pop();
    }

    return results;
  }
};
