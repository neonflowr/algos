#include <iostream>
#include <vector>
#include <stdio.h>

#define MAXN 100

using namespace std;

void print2d(bool items[][MAXN+1], int n, int k) {
	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= k; j++) {
			cout << items[i][j] << " ";
		}
		cout << "\n";
	}
}

void report_subset(int parent[][MAXN+1], int n, int k) {
	if (k == 0 || n < 0) return;

	if (parent[n][k] == -1) {
		report_subset(parent, n - 1, k);
	}
	else {
		report_subset(parent, n - 1, parent[n][k]);
		cout << k - parent[n][k] << " ";
	}
}

bool subset_sum(int a[], int n, int k) {
	bool dp[MAXN + 1][MAXN + 1];
	int parent[MAXN + 1][MAXN + 1];
	int i, j;
	for (i = 0; i <= MAXN; i++) {
		for (j = 0; j <= MAXN; j++) {
			dp[i][j] = false;
			parent[i][j] = -1;
		}
	}
	dp[0][0] = true;

	for (i = 1; i <= n; i++) {
		for (j = 0; j <= k; j++) {
			dp[i][j] = dp[i - 1][j];
			parent[i][j] = -1;
			if (j >= a[i - 1] && dp[i - 1][j - a[i - 1]]) {
				dp[i][j] = true;
				parent[i][j] = j - a[i - 1];
			}
		}
	}
	printf("dp table\n");
	print2d(dp, n, k);

	printf("Subset: ");
	report_subset(parent, n, k);
	printf("\n");

	return dp[i - 1][j - 1];
}


int main(int argc, char *argv[]) {
	int k;
	if (argc < 2) {
		k = 33;
	}
	else {
		k = atoi(argv[1]);
	}

	int a[4] = {21, 1, 15, 4};

	printf("Possible for k = %d: %d", k, subset_sum(a, 4, k));

	return 0;
}

