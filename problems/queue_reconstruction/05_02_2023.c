/*
  2月5日2023年
  https://leetcode.com/problems/queue-reconstruction-by-height/
 *
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume
 * caller calls free().
 */

// an is_valid() loop that works like bubble sort?
// need to split into whether the current person count is greater or lesser than the detected count

#include <stdio.h>
#include <stdlib.h>

void swap(int **x, int **y) {
  int *tmp = *x;
  *x = *y;
  *y = tmp;
}

int is_valid(int **people, int *person, int person_index) {
  int height = person[0];
  int in_front_count = person[1];
  int count = 0;

  for (int i = 0; i < person_index; i++) {
	if (people[i][0] >= height)
	  count++;
  }

  if (count > in_front_count)
	return 0;
  else if (count < in_front_count)
	return -1;
  else
	return 1;
}

void print(int **people, int n) {
  for (int i = 0; i < n; i++) {
	printf("[%d, %d], ", people[i][0], people[i][1]);
  }
  printf("\n");
}
void printarr(int *items, int n) {
  for (int i = 0; i < n; i++) {
    printf("%d, ", items[i]);
  }
  printf("\n");
}

int **reconstructQueue(int **people, int peopleSize, int *peopleColSize,
                       int *returnSize, int **returnColumnSizes) {
  printf("people size %d\n", peopleSize);
  printf("people col size %d\n", *peopleColSize);
  printf("Return sizse %d\n", *returnSize);

  // Copy
  int **queue = malloc(*returnSize * sizeof(int *));

  int **column_sizes = malloc(*returnSize * sizeof(int *));

  for (int i = 0; i < peopleSize; i++) {
    queue[i] = malloc(*peopleColSize * sizeof(int));
    queue[i][0] = people[i][0];
    queue[i][1] = people[i][1];

    column_sizes[i] = malloc(*peopleColSize * sizeof(int));
    column_sizes[i][0] = *peopleColSize;
    column_sizes[i][1] = *peopleColSize;
  }

  *returnSize = peopleSize;
  returnColumnSizes = column_sizes;
  printf("Return col\n");
  print(column_sizes, peopleSize);

  printf("Initial\n");
  print(queue, peopleSize);

  int swapped = 1;
  while(swapped == 1) {
	swapped = 0;
    for (int i = 0; i < peopleSize; i++) {
      int is_position_valid = is_valid(queue, queue[i], i);
      if (is_position_valid == 1) {
        continue;
      } else if (is_position_valid == 0) {
        // Too many taller people in front so need to bubble to the front
        swap(&queue[i - 1], &queue[i]);
		swapped = 1;
      } else {
        // Not enough people in front, need to bubble to the back
        swap(&queue[i], &queue[i + 1]);
        swapped = 1;
      }
    }
  }

  printf("Done\n");
  print(queue, peopleSize);

  return queue;
}

/* int main() { */
/*   int peopleColsize = 2; */
/*   int returnSize = 6; */

/*   int *pColsize = &colsize; */
/*   int peopleSize =6; */

/*   // Copy */
/*   int peoplearr[6][2] = {{6, 0}, {5, 0}, {4, 0}, {3, 2}, {2, 2}, {1, 4}}; */
/*   int **people = malloc(peopleSize * sizeof(int *)); */
/*   for (int i = 0; i < peopleSize; i++) { */
/*     people[i] = malloc(colsize * sizeof(int)); */
/* 	people[i][0] = peoplearr[i][0]; */
/* 	people[i][1] = peoplearr[i][1]; */
/*   } */

/*   printf("INit\n"); */
/*   print(people, peopleSize); */

/*   people = reconstructQueue(people, 6, &peopleColsize, &returnSize, &pColsize); */

/*   printf("Final\n"); */
/*   print(people, peopleSize); */

/*   return 0; */
/* } */
