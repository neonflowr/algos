
#include <stdio.h>
#include <vector>

#define MAX_POSSIBLE 8.0
#define MAX_SIZE 25

using namespace std;

class Cell {
public:
  int x;
  int y;
  bool is_in_board = true;
  double prob = 0;
  Cell(int x, int y) {
    this->x = x;
    this->y = y;
  }
  Cell(int x, int y, bool in_board) {
    this->x = x;
    this->y = y;
    this->is_in_board = in_board;
  }
};

class Solution {
private:
  // 0-indexed board
  inline bool is_in_board(int x, int y, int board_size) {
    return x >= 0 && x < board_size && y >= 0 && y < board_size;
  }

  void get_moves(Cell current, vector<Cell> &possible, double *pcount,
                 int board_size) {
    // probably better as a priority queue
    possible = {
        {current.x + 1, current.y + 2}, {current.x + 1, current.y - 2},
        {current.x - 1, current.y + 2}, {current.x - 1, current.y - 2},
        {current.x + 2, current.y + 1}, {current.x + 2, current.y - 1},
        {current.x - 2, current.y + 1}, {current.x - 2, current.y - 1},
    };
    double initial_count = MAX_POSSIBLE;
    for (int i = 0; i < MAX_POSSIBLE; i++) {
      if (!is_in_board(possible[i].x, possible[i].y, board_size)) {
        possible[i].is_in_board = false;
        initial_count--;
      }
    }
    *pcount = initial_count;
  }

  void print(vector<Cell> items) {
    for (int i = 0; i < items.size(); i++) {
      if(items[i].is_in_board) {
        printf("Move: %d %d %d\n", items[i].x, items[i].y, items[i].is_in_board);
      }
    }
  }
  void print(vector<double> items) {
    for (int i = 1; i < items.size(); i++) {
      printf("%lf ", items[i]);
    }
    printf("\n");
  }

  void pick_move(Cell *current, vector<Cell> possible) {
    for (int i = 0; i < 8; i++) {
      if (possible[i].is_in_board) {
        current->x = possible[i].x;
        current->y = possible[i].y;
        printf("Picked move: %d %d\n", possible[i].x, possible[i].y);
        return;
      }
    }
  }
  void print2d(double items[][MAX_SIZE], int n) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        printf("%lf ", items[i][j]);
      }
      printf("\n");
    }
  }

public:
  double knightProbability(int n, int k, int row, int column) {
    Cell current = Cell(row, column);

    vector<Cell> possible;
    vector<Cell> possible_child;
    double pcount = 8;
    double pcount_child = 8;

    double dp[MAX_SIZE][MAX_SIZE];
    double refdp[MAX_SIZE][MAX_SIZE];
    for (int i = 0; i < MAX_SIZE; i++) {
      for (int j = 0; j < MAX_SIZE; j++) {
        dp[i][j] = 0;
        refdp[i][j] = 0;
      }
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        current.x = i;
        current.y = j;
        get_moves(current, possible, &pcount, n);
        // print(possible);
        dp[current.x][current.y] = pcount / MAX_POSSIBLE;
        refdp[current.x][current.y] = pcount / MAX_POSSIBLE;
      }
    }
    current.x = row;
    current.y = column;
    double sum;
    double count;
    double prob = dp[current.x][current.y];

      printf("ref\n");
      print2d(refdp, n);

    for (int i = 1; i < k; i++) {
      pcount = 8;
      get_moves(current, possible, &pcount, n);
      print(possible);
      sum = 0;
      count = 0;
      for (int j = 0; j < MAX_POSSIBLE; j++) {
        if (possible[j].is_in_board) {
          sum += refdp[possible[j].x][possible[j].y];
          count++;
        }
      }

      sum = sum / count;
      printf("Sum %lf\n", sum);

      for (int j = 0; j < MAX_POSSIBLE; j++) {
        if (possible[j].is_in_board) {
          dp[possible[j].x][possible[j].y] = sum;
        }
      }
      printf("current prob: %lf\n", prob);
      prob *= sum;
      printf("new prob: %lf\n", prob);
      pick_move(&current, possible);
      printf("updaetd\n");
      print2d(dp, n);
    }

    return prob;
  }
};
