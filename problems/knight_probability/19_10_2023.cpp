#include <stdio.h>
#include <vector>

#define MAX_POSSIBLE 8.0
#define MAX_SIZE 25

using namespace std;

class Cell {
public:
  int x;
  int y;
  bool is_in_board = true;
  double prob = 0;
  Cell(int x, int y) {
    this->x = x;
    this->y = y;
  }
  Cell(int x, int y, bool in_board) {
    this->x = x;
    this->y = y;
    this->is_in_board = in_board;
  }
};

class Solution {
private:
  // 0-indexed board
  inline bool is_in_board(int x, int y, int board_size) {
    return x >= 0 && x < board_size && y >= 0 && y < board_size;
  }

  void get_moves(Cell current, vector<Cell> &possible, double *pcount,
                 int board_size) {
    // probably better as a priority queue
    possible = {
        {current.x + 1, current.y + 2}, {current.x + 1, current.y - 2},
        {current.x - 1, current.y + 2}, {current.x - 1, current.y - 2},
        {current.x + 2, current.y + 1}, {current.x + 2, current.y - 1},
        {current.x - 2, current.y + 1}, {current.x - 2, current.y - 1},
    };
    int initial_count = MAX_POSSIBLE;
    for (int i = 0; i < MAX_POSSIBLE; i++) {
      if (!is_in_board(possible[i].x, possible[i].y, board_size)) {
        possible[i].is_in_board = false;
        initial_count--;
      }
    }
    *pcount = initial_count;
  }

  void get_moves(int x, int y, vector<Cell> &possible, double *pcount,
                 int board_size) {
    // probably better as a priority queue
    possible = {
        {x + 1, y + 2}, {x + 1, y - 2},
        {x - 1, y + 2}, {x - 1, y - 2},
        {x + 2, y + 1}, {x + 2, y - 1},
        {x - 2, y + 1}, {x - 2, y - 1},
    };
    int initial_count = MAX_POSSIBLE;
    for (int i = 0; i < MAX_POSSIBLE; i++) {
      if (!is_in_board(possible[i].x, possible[i].y, board_size)) {
        possible[i].is_in_board = false;
        initial_count--;
      }
    }
    *pcount = initial_count;
  }

  void print(vector<Cell> items) {
    for (int i = 0; i < items.size(); i++) {
      printf("Move: %d %d %d\n", items[i].x, items[i].y, items[i].is_in_board);
    }
  }
  void print(vector<double> items) {
    for (int i = 1; i < items.size(); i++) {
      printf("%lf ", items[i]);
    }
    printf("\n");
  }

  void pick_move(Cell* current, vector<Cell> possible) {
    for(int i = 0; i < 8; i++) {
      if (possible[i].is_in_board) {
        current->x = possible[i].x;
        current->y = possible[i].y;
        printf("Picked move: %d %d\n", possible[i].x, possible[i].y);
        return;
      }
    }
  }

public:
  double knightProbability(int n, int k, int row, int column) {
    Cell current = Cell(row, column);

    vector<Cell> possible;
    vector<Cell> possible_child;
    double pcount = 8;
    double pcount_child = 8;
    double prob = 1;

    double max;
    double sum;
    double avg;

    double oldp = 1;

    for (int i = 1; i < k; i++) {
      get_moves(current, possible, &pcount, n);
      // print(possible);

      sum = 0;
      max = 0;

      for(int j = 0; j < MAX_POSSIBLE; j++) {
        if (possible[j].is_in_board == true) {
          get_moves(possible[j], possible_child, &pcount_child, n);
          printf("pcount child update %lf\n", pcount_child);
          // possible[j].prob = pcount_child / MAX_POSSIBLE;
          sum += pcount_child;
          max++;
        }
      }

      // average of next move
      avg = (sum / max) / MAX_POSSIBLE;
      printf("%lf %lf, avg %lf\n", sum, max, avg);

      printf("pcount %lf\n", pcount);

      prob *= (pcount / MAX_POSSIBLE);
      pick_move(&current, possible);
    }

    return prob;
  }
};
