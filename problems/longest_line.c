#include <stdio.h>
#define MAXLINE 1000 // Maximum input line size

// Declarations
int kr_getline(char line[], int maxline);
void copy(char to[], char from[]);

// Print longest input line
int main() {
  int len; // current line length
  int max; // max line length so far
  char line[MAXLINE]; // track current input line
  char longest[MAXLINE]; // track longest line

  max = 0;
  while((len = kr_getline(line, MAXLINE)) > 0) {
    if (len > max) {
      max = len;
      copy(longest, line);
    }
  }

  if (max > 0)
    printf("The longest line is:\n%s", longest);

  return 0;
}

// Read a line into `line`, return length
int kr_getline(char line[], int maxline) {
  int c, i;

  // Read from stdin and track the line through `line`, ignoring newline characters, only ending when EOF is encountered (that's Control+D for input)
  for (i=0; i<maxline-1 && (c=getchar()) != EOF && c != '\n'; ++i)
    line[i] = c;
  if (c=='\n') {
    line[i] = c;
    ++i;
  }
  line[i] = '\0';
  return i;
}

// Copy `from` into `to`; assuming `to` is big enough
void copy(char to[], char from[]) {
  int i;
  i = 0;

  while ((to[i] = from[i]) != '\0') // \0 means the end of a string, so this while will run until the end of the from string
    ++i;
}


// C has the 'extern' keyword that allows functions to access global variables, kinda like the 'global' keyword in Python
