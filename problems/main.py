import time
from nearest_neighbor import nearest_neighbor
from closest_pair import closest_pair
from random import randint

INPUT_SIZE: int = 50

input: list = []
for i in range(INPUT_SIZE):
    val = randint(-INPUT_SIZE, INPUT_SIZE)
    if val not in input:
        input.append(val)

print(f"Find the shortest cycle tour that visits each point in {input}")

print("NEAREST NEIGHBOR")
print("----------------")
t1 = time.time()
path, dist = nearest_neighbor(input)
t2 = time.time()
print(f"Path is {path}")
print(f"Dist is {dist}")
print(f"Time={t2-t1}")

print("\n----------------\n")

print("CLOSEST PAIR")
print("----------------")
t3 = time.time()
closest_path, closest_dist = closest_pair(input)
t4 = time.time()
print(f"Path is {closest_path}")
print(f"Dist is {closest_dist}")
print(f"Time={t4-t3}")
