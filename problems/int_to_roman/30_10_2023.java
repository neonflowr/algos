class Solution {
    public static int mapping[] = new int[100];
    public static char flag_mapping[] = new char[100];
    public static char alt_mapping[] = new char[100];
    public static char nine_mapping[] = new char[100];
    public static char four_mapping[] = new char[100];

    public static void ini() {
	mapping['I'] = 1;
	mapping['V'] = 5;
	mapping['X'] = 10;
	mapping['L'] = 50;
	mapping['C'] = 100;
	mapping['D'] = 500;
	mapping['M'] = 1000;

	flag_mapping['M'] = 'C';
	flag_mapping['D'] = 'C';
	flag_mapping['L'] = 'X';
	flag_mapping['C'] = 'X';
	flag_mapping['X'] = 'I';
	flag_mapping['V'] = 'I';

	alt_mapping['C'] = 'D';
	alt_mapping['X'] = 'L';
	alt_mapping['I'] = 'V';

	nine_mapping['I'] = 'X';
	nine_mapping['X'] = 'C';
	nine_mapping['C'] = 'M';

	four_mapping['I'] = 'V';
	four_mapping['X'] = 'L';
	four_mapping['C'] = 'D';
    }

    public static String getn(int num, char level) {
	String result = new String();
	boolean flag = false;

	if (alt_mapping[level] != 0 && num >= mapping[alt_mapping[level]])  {
	    result += alt_mapping[level];
	    num -= mapping[alt_mapping[level]];
	    flag = true;
	}
	
	int rem = num / mapping[level];

	if (rem == 4 && flag) {
	    String s = new String();
	    s += level;
	    s += nine_mapping[level];
	    return s;
	} else if (rem == 4 && !flag) {
	    result += level;
	    result += four_mapping[level];
	    return result;
	}
	
	for(int i = 0; i < rem; i++) {
	    result += level;
	}
	return result;
    }

    public static String intToRoman(int num) {
	ini();
	
	String result = new String();

	result = result.concat(getn(num, 'M'));
	num = num % mapping['M'];

	result = result.concat(getn(num, 'C'));
	num = num % mapping['C'];

	result = result.concat(getn(num, 'X'));
	num = num % mapping['X'];

	result = result.concat(getn(num, 'I'));
	
	return result;
    }
}
