// 29/05/2023
// https://leetcode.com/problems/design-parking-system/

typedef struct {
	int small;
	int medium;
	int big;
} ParkingSystem;

enum CarType {
	BIG = 1,
	MEDIUM,
	SMALL
};

ParkingSystem* parkingSystemCreate(int big, int medium, int small) {
	ParkingSystem* p = malloc(sizeof(ParkingSystem));
	p->big = big;
	p->medium = medium;
	p->small = small;
	return p;
}


bool parkingSystemAddCar(ParkingSystem* obj, int carType) {
	if (carType == BIG) {
		if (obj->big == 0)
			return false;
		obj->big--;
	}
	else if (carType == MEDIUM) {
		if (obj->medium == 0)
			return false;
		obj->medium--;
	}
	else {
		if (obj->small == 0)
			return false;
		obj->small--;
	}

	return true;
}

void parkingSystemFree(ParkingSystem* obj) {
	free(obj);
}

/**
 * Your ParkingSystem struct will be instantiated and called as such:
 * ParkingSystem* obj = parkingSystemCreate(big, medium, small);
 * bool param_1 = parkingSystemAddCar(obj, carType);

 * parkingSystemFree(obj);
*/