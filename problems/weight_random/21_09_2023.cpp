#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define SIZE 10

void print(int* items, int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", items[i]);
	}
	printf("\n");
}

int pick(int* weights, int n) {
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum += weights[i];
	}

	int rnd = rand() % sum;

	for (int i = 0; i < n; i++) {
		if (rnd < weights[i])
			return i;
		rnd -= weights[i];
	}
}

int main() {
	srand(time(NULL));
	int weights[SIZE];
	for (int i = 0; i < SIZE; i++) {
		weights[i] = (rand() % 10) + 1;
	}
	printf("Weights\n");
	print(weights, SIZE);

	int counts[10] = { 0 };

	int result;
	for (int i = 0; i < 100; i++) {
		result = pick(weights, SIZE);
		//printf("Picked %d\n", result);
		counts[result]++;
	}

	printf("Counts\n");
	print(counts, 10);

	return 0;
}