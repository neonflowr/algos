#include <iostream>
#include <stdlib.h>

int rand7() {
	return (rand() % 7) + 1;
}

int last_rand = 12417;

int rand10() {
	int n = ((101 * rand7() + 173) % 10) + 1;
	return n;
}

int main() {
	srand(12417);
	printf("Rand7\n");
	for (int i = 0; i < 100; i++) {
		printf("%d ", rand7());
	}
	printf("\n");

	printf("Rand10\n");
	for (int i = 0; i < 100; i++) {
		printf("%d ", rand10());
	}
	printf("\n");

	return 0;
}