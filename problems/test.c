#include <stdio.h>

int main() {
  printf("test\n");

  int i = 15;
  int *p = &i;

  printf("First Pointer deref: %d\n", *p);
  printf("First Pointer address: %p\n", p);

  int **pp = &p;

  printf("Second pointer deref: %p\n", *pp);
  printf("Second pointer address: %p\n", pp);


  // Change through pp
  printf("Changed\n");

  int new_val = 69;
  int *new_p = &new_val;

  printf("New pointer deref: %d\n", *new_p);
  printf("New pointer address: %p\n", new_p);

  *pp = new_p;

  printf("Second pointer deref: %p\n", *pp);
  printf("Second pointer address: %p\n", pp);

  printf("First Pointer deref: %d\n", *p);
  printf("First Pointer address: %p\n", p);
}
