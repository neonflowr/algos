"""
Find the shortest cycle tour that visits each point in s with the closest pair heuristic
"""

def _dist(p1: list, p2: list) -> int:
    return abs(p2[-1] - p1[-1])

def closest_pair(s: list) -> tuple:
    # Build initial vertex chain
    distinct_chains: list[list[int]] = []
    for i in s:
        distinct_chains.append([i])

    for i in range(len(s) - 1):
        # Create a connection for n - 1 values in the set
        min = None
        s = None
        t = None

        for j in range(len(distinct_chains)):
            # Find the closest connection possible
            for k in range(len(distinct_chains)):
                if j == k: continue
                d = _dist(distinct_chains[j], distinct_chains[k])
                if not min or d <= min:
                    s = j
                    t = k
                    min = d

        # Create connection
        if distinct_chains[s][-1] > distinct_chains[t][-1]:
            distinct_chains[s] = distinct_chains[t] + distinct_chains[s]
        else:
            distinct_chains[s] = distinct_chains[s] + distinct_chains[t]

        distinct_chains.pop(t)

    # Handle the cycle
    distinct_chains = distinct_chains[-1]
    distinct_chains.append(distinct_chains[0])

    total_dist: int = 0
    for i in range(len(distinct_chains)):
        if i != 0:
            total_dist += abs(distinct_chains[i] - distinct_chains[i-1])

    return distinct_chains, total_dist
