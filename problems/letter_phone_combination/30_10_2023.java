import java.util.List;
import java.util.ArrayList;

class Solution {
    public static String mapping[] = new String[100];

    public static void createMapping() {
	mapping['2'] = "abc";
	mapping['3'] = "def";
	mapping['4'] = "ghi";
	mapping['5'] = "jkl";
	mapping['6'] = "mno";
	mapping['7'] = "pqrs";
	mapping['8'] = "tuv";
	mapping['9'] = "wxyz";
	// mapping['a'] = 2;
	// mapping['b'] = 2;
	// mapping['c'] = 2;
	// mapping['d'] = 3;
	// mapping['e'] = 3;
	// mapping['f'] = 3;
	// mapping['g'] = 4;
	// mapping['h'] = 4;
	// mapping['i'] = 4;
	// mapping['j'] = 5;
	// mapping['k'] = 5;
	// mapping['l'] = 5;
	// mapping['m'] = 6;
	// mapping['n'] = 6;
	// mapping['o'] = 6;
	// mapping['p'] = 7;
	// mapping['q'] = 7;
	// mapping['r'] = 7;
	// mapping['s'] = 7;
	// mapping['t'] = 8;
	// mapping['u'] = 8;
	// mapping['v'] = 8;
	// mapping['w'] = 9;
	// mapping['x'] = 9;
	// mapping['y'] = 9;
	// mapping['x'] = 9;
    }

    public static boolean isSolution(String digits, int k) {
	return k == digits.length();
    }

    public static char[] getMoves(String digits, int k) {
	char digit = digits.charAt(k);
	String available = mapping[digit];
	System.out.printf("Moves for digit %s: %s\n", digit, available);
	char[] moves = available.toCharArray();
	return moves;
    }

    public static void backtrack(String digits, List<Character> a, List<String> results, int k) {
	if (isSolution(digits, k)) {
	    StringBuilder sb = new StringBuilder();
	    for (char ch: a) {
		System.out.println("Append: " + ch);
		sb.append(ch);
	    }
	    String res = sb.toString();
	    if (res == "") return;
	    results.add(res);
	    System.out.println("Solution: " + res);
	} else {
	    char[] moves = getMoves(digits, k);
	    for (int i = 0; i < moves.length; i++) {
		// make move
		a.set(k, moves[i]);
		backtrack(digits, a, results, k+1);
		// unmake
	    }
	}
    }
    
    public static List<String> letterCombinations(String digits) {
	createMapping();
	List<String> results = new ArrayList<String>();
	List<Character> a = new ArrayList<Character>();
	for(int i = 0; i < digits.length(); i++) {
	    a.add('0');
	}
	backtrack(digits, a, results, 0);
	return results;
    }

    // public static void main(String[] args) {
    // 	letterCombinations(args[0]);
    // 	return;
    // }
}
