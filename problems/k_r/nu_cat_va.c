#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>

void error(char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  fprintf(stderr, "error: ");
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");
  va_end(args);
  exit(1);
}

int main(int argc, char **argv) {
  if (argc < 2)
    error("Usage: ncat f1 f2 ...");

  for(int i = 1; i < argc; i++) {
    printf("Print: %s\n", argv[i]);
    int fd, n;
    char buf[BUFSIZ];

    if ((fd = open(argv[i], O_RDONLY, 0)) == -1)
      error("ncat: can't open file");

    while((n = read(fd, buf, BUFSIZ)) > 0) {
      write(STDOUT_FILENO, buf, n);
    }

    close(fd);
  }

  return 0;
}
