#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int is_lower = 1;

  if (argc == 2) {
	if (strcmp(argv[1], "lower") == 0) {
	  is_lower = 1;
	} else if (strcmp(argv[1], "upper") == 0) {
	  is_lower = 0;
	} else {
	  printf("Arg unrecognized\n");
	  return 0;
	}
  }

  int c;
  if (is_lower == 1) {
    while ((c = getchar()) != EOF) {
      putchar(tolower(c));
    }
  } else {
    while ((c = getchar()) != EOF) {
      putchar(toupper(c));
    }
  }

  return 0;
}
