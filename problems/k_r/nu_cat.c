#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>

void error(char *fmt, ...) {
  va_list args;
  va_start(args, fmt);

  fprintf(stderr, "error: ");
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");
  va_end(args);
  exit(1);
}


int main(int argc, char **argv) {
  if (argc < 2)
    error("Usage: ncat filename");

  int f1, f2, n;
  char buf[BUFSIZ];

  if ((f1 = open(argv[1], O_RDONLY, 0)) == -1)
    error("ncat: can't open\n");

  while((n = read(f1, buf, BUFSIZ)) > 0) {
    write(STDOUT_FILENO, buf, n);
  }

  return 0;
}

