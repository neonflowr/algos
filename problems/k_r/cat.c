#include <stdio.h>

void filecopy(FILE *s, FILE *t) {
    int c;
    while((c = getc(s)) != EOF) {
        putc(c, t);
    }
}

// 猫のプログラム
int main(int argc, char *argv[]) {
    FILE *f;
    printf("cat\n");
    printf("argc %d\n", argc);

    if (argc == 1) {
        printf("Copy stdin to stdout\n");
        filecopy(stdin, stdout);
        return 1;
    } else {
        printf("Copy file to stdout\n");
        while(--argc > 0) {
            if ((f = fopen(*(++argv), "r")) == NULL) {
                printf("Could not open %s\n", *argv);
                return 1;
            } else {
                printf("Opening %s...\n", *argv);
                filecopy(f, stdout);
                fclose(f);
            }
        }
    }
    return 0;
}
