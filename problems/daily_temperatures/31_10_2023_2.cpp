#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
public:
  vector<int> dailyTemperatures(vector<int> &temperatures) {
    int n = temperatures.size();
    vector<int> result;
    result.resize(n);
    int lasti, last;
    lasti = last = -1;
    int current;
    for (int i = 0; i < n; i++) {
      current = temperatures[i];

      if (last != -1 && lasti > i && last > current) {
        result[i] = lasti;
        continue;
      }
      cout << "running\n";

      for (int j = i + 1; j < n; j++) {
        if (temperatures[j] > current) {
          lasti = j;
          last = temperatures[j];
          result[i] = j - i;
          break;
        }
      }
    }
    return result;
  }
};
