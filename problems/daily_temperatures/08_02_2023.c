/*
 * https://leetcode.com/problems/daily-temperatures/
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct Tree {
  int data;
  int *indexes;
  int current_index;
  int count;
  struct Tree *left;
  struct Tree *right;
} tree;


void insert(tree **t, int value, int index, int items_size) {
  if (*t == NULL) {
	tree *node = malloc(sizeof(tree));
	node->data = value;
	node->indexes = malloc(items_size * sizeof(int));
	node->indexes[0] = index;
	node->current_index = 0;
	node->count = 1;
	node->left = NULL;
	node->right = NULL;
	*t = node;
	return;
  }

  if ((*t)->data < value) {
	insert(&(*t)->right, value, index, items_size);
  } else if ((*t)->data > value) {
    insert(&(*t)->left, value, index, items_size);
  } else {
	(*t)->indexes[(*t)->count++] = index; // Add index
  }
}

tree *search(tree *t, int value) {
  if (t == NULL)
	return NULL;
  if (t->data == value) {
    return t;
  }
  if (t->data < value) {
    return search(t->right, value);
  }
  else {
    return search(t->left, value);
  }
}

void find_closest_node(tree *t, int index, tree **closest) {
  if (t == NULL)
    return;
  if (*closest == NULL) {
    *closest = t;
  } else {
    int current_index = t->indexes[t->current_index];
    if (current_index < (*closest)->indexes[(*closest)->current_index]) {
      *closest = t;
    }
  }

  find_closest_node(t->left, index, closest);
  find_closest_node(t->right, index, closest);
}

int get_days_until_next_warmer(tree *t, int temp) {
  tree *node = search(t, temp);
  if (node == NULL)
	return 0;

  int current_index = node->indexes[node->current_index];


  // Descend the right tree, get count of any node with index larger than current_index
  tree **closest = malloc(sizeof(tree *));
  *closest = NULL;
  find_closest_node(node->right, current_index, closest);

  if (*closest != NULL) {
	int val = (*closest) ->indexes[(*closest)->current_index] - current_index++;
	free(closest);
	return val;
  } else {
    free(closest);
    return 0;
  }
  return 0;
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int *dailyTemperatures(int *temperatures, int temperaturesSize,
                       int *returnSize) {
  printf("Return size %d\n", *returnSize);

  tree **head = malloc(sizeof(tree *));
  *head = NULL;

  // Build tree
  for (int i = 0; i < temperaturesSize; i++) {
	insert(head, temperatures[i], i, temperaturesSize);
  }

  int *result = malloc(temperaturesSize * sizeof(int));

  for (int i = 0; i < temperaturesSize; i++) {
    printf("Check tmp %d\n", temperatures[i]);
    int days = get_days_until_next_warmer(*head, temperatures[i]);
    printf("Days %d\n", days);
    result[i] = days;
  }

  for (int i = 0; i < temperaturesSize; i++) {
	printf("%d ", result[i]);
  }

  free(*head);

  return result;
  }
