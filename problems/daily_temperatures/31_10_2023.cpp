#include <vector>

using namespace std;

class Solution {
public:
  vector<int> dailyTemperatures(vector<int> &temperatures) {
    vector<int> result;
    int n = temperatures.size();

    result.resize(n);

    int current;
    int prev = -1;

    int min;
    for (int i = 0; i < n; i++) {
      current = temperatures[i];
      min = 1000000000;

      if (prev != -1) {
        if (current < prev) {
          if (result[i - 1] != 0) {
            for (int k = 1; k < result[i - 1]; k++) {
              if (temperatures[i + k] > current) {
                result[i] = k;
                break;
              }
            }
            prev = current;
            continue;
          }
        } else if (current == prev) {
          result[i] = result[i - 1] == 0 ? result[i - 1] : result[i - 1] - 1;
          prev = current;
          continue;
        }
      }

      for (int j = i + 1; j < n; j++) {
        if (temperatures[j] > current) {
          result[i] = j - i;
          break;
        }
      }

      prev = current;
    }

    return result;
  }
};
