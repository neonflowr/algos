#include <winsock2.h>
#include <stdlib.h>

int* encode(char* c, int n) {
	int* buf = malloc(sizeof(int) * n);
	for (int i = 0; i < n; i++) {
		*buf = *c;
		buf++;
		c++;
	}
	return buf;
}

int main(int argc, int ) {
	int sockfd = socket(AF_NET, SOCK_DGRAM); 

	char data[5] = "mars";

	char* hostname = "hostname";
	int serverport = 12000;

	int* bytes = encode(data, 5);

	sockfd.sendto(bytes, hostname, serverport);

	int* message = sockfd.recvfrom(2048);

	sockfd.close();

	free(bytes);

	return 0;
}
