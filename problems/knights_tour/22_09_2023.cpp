#include <vector>
#include <stdio.h>

using namespace std;

struct point {
    int x, y;
};

class Solution {
public:
    vector<point> moves;

    point current_pos;

    void print2d(vector<vector<int>> items) {
        for (int i = 0; i < items.size(); i++) {
            for (int j = 0; j < items.size(); j++) {
                cout << items[i][j] << " ";
            }
            cout << "\n";
        }
    }

    void print_moves(vector<point> moves) {
        for (int i = 0; i < moves.size(); i++) {
            printf("%d %d | ", moves[i].x, moves[i].y);
        }
        printf("\n");
    }

    bool is_solution(vector<vector<int>> a, int k, int dimension) {
        return k == (dimension * dimension) - 1;
    }

    bool can_knight_move(vector<vector<int>> a, point current, int x, int y, int dimension) {
        // printf("Check move %d %d from %d %d\n", x, y, current.x, current.y);
        if (x < 0 || x >= dimension) return false;
        if (y < 0 || y >= dimension) return false;

        if (a[x][y] != -1) return false;

        return true;
    }

    void get_moves(vector<vector<int>> a, point current, int dimension, vector<point>& c) {
        vector<point> possible = {
            {current.x + 1, current.y + 2},
            {current.x + 1, current.y - 2},
            {current.x - 1, current.y + 2},
            {current.x - 1, current.y - 2},
            {current.x + 2, current.y + 1},
            {current.x + 2, current.y - 1},
            {current.x - 2, current.y + 1},
            {current.x - 2, current.y - 1},
        };
        for (int i = 0; i < 8; i++) {
            if (can_knight_move(a, current, possible[i].x, possible[i].y, dimension)) {
                // printf("Pushing move %d %d\n", possible[i].x, possible[i].y);
                c.push_back(possible[i]);
                // printf("pushed\n");
            }
        }
    }

    void construct_candidates(vector<vector<int>> a, int k, int dimension, vector<point>& c) {
        get_moves(a, this->current_pos, dimension, c);
    }

    bool compare2d(vector<vector<int>> grid, vector<vector<int>> a, int dimension) {
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (grid[i][j] != a[i][j]) {
                    return false;
                }
                if (a[i][j] == -1 && grid[i][j] != 0) {
                    return false;
                }

            }
        }
        return true;
    }

    void backtracking(vector<vector<int>> grid, vector<vector<int>> a, int dimension, int k, bool* finished, bool* result) {
        vector<point> c;

        // if (false) {
        //     // // printf("Solution found\n");
        //     // if (compare2d(grid, a, dimension)) {
        //     //     // printf("Result found\n");
        //     //     *finished = true;
        //     //     *result = true;
        //     // }
        // } else {
        k++;
        construct_candidates(a, k, dimension, c);
        // print_moves(c);
        if (c.size() == 0) {
            printf("Solution found\n");
            print2d(a);
            // compare a and grid
            if (compare2d(grid, a, dimension)) {
                printf("Result found\n");
                *finished = true;
                *result = true;
            }
            return;
        }

        for (int i = 0; i < c.size(); i++) {
            int old_x, old_y;
            old_x = this->current_pos.x;
            old_y = this->current_pos.y;

            a[c[i].x][c[i].y] = k;

            // printf("Make move %d %d\n", c[i].x, c[i].y);
            // print2d(a);

            this->current_pos.x = c[i].x;
            this->current_pos.y = c[i].y;

            backtracking(grid, a, dimension, k, finished, result);
            // printf("UnMake move %d %d\n", c[i].x, c[i].y);

            a[c[i].x][c[i].y] = -1;
            this->current_pos.x = old_x;
            this->current_pos.y = old_y;

            if (*finished) return;
        }
        // }

    }

    bool checkValidGrid(vector<vector<int>>& grid) {
        int dimension = grid.size();
        vector<vector<int>> a;
        a.resize(dimension);
        for (int i = 0; i < dimension; i++) {
            a[i].resize(dimension);
            for (int j = 0; j < dimension; j++) {
                a[i][j] = -1;
            }
        }
        a[0][0] = 0;
        this->current_pos.x = 0;
        this->current_pos.y = 0;

        bool finished = false;
        bool result = false;
        backtracking(grid, a, dimension, 0, &finished, &result);

        return result;
    }
};