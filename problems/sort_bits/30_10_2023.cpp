class Solution {
public:
  inline void swap(int *x, int *y) {
    int tmp = *x;
    *x = *y;
    *y = tmp;
  }

  int get_one_bits(int x) {
    int count = 0;
    while(x > 0) {
      count++;
      x = x & (x - 1);
    }
    return count;
  }

  inline int compare(int x, int y) {
    int xb = get_one_bits(x);
    int yb = get_one_bits(y);
    return yb == xb ? y - x : yb - xb;
  }

  inline int median_of_three(vector<int>& items, int low, int high) {
    int middle = (low + high) / 2;
    if (compare(items[low], items[high]) < 0)
      swap(&items[low], &items[high]);
    if (compare(items[low], items[middle]) < 0)
      swap(&items[low], &items[middle]);
    if (compare(items[middle], items[high]) < 0)
      swap(&items[middle], &items[high]);
    return middle;
  }
  
  int partition(vector<int>& items, int low, int high) {
    int p = median_of_three(items, low, high);
    int fh = low;

    for(int i = low; i <= high; i++) {
      if (compare(items[i], items[p]) > 0) {
	if (fh == p) p = i;
	swap(&items[i], &items[fh]);
	fh++;
      }
    }

    swap(&items[p], &items[fh]);

    return fh;
  }

  void qsort(vector<int>& items, int low, int high) {
    if (low < high) {
      int p = partition(items, low, high);
      qsort(items, low, p - 1);
      qsort(items, p + 1, high);
    }
  }
  
  vector<int> sortByBits(vector<int>& arr) {
    qsort(arr, 0, arr.size() - 1);
    return arr;
  }
};

