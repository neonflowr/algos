#include <string>
#include <vector>
#include <stdio.h>
#include <iostream>

#define MATCH 0
#define INSERT 1
#define DELETE 2

#define MAXLEN 20

using namespace std;

typedef struct {
	int cost;
	int parent;
} cell;

int edit_distance(string s1, string s2, int i, int j, int *step) {
	int opt[3];
	int lowest;

	*step = *step + 1;

	if (i == 0) {
		return j * 1;
	}
	if (j == 0) {
		return i * 1;
	}

	lowest = s1[i - 1] == s2[j - 1] ? 0 : 1;

	opt[MATCH] = edit_distance(s1, s2, i - 1, j - 1, step) + lowest;
	opt[INSERT] = edit_distance(s1, s2, i, j - 1, step) + 1;
	opt[DELETE] = edit_distance(s1, s2, i - 1, j, step) + 1;

	lowest = opt[MATCH];
	for (int i = INSERT; i <= DELETE; i++) {
		if (opt[i] < lowest) lowest = opt[i];
	}

	return lowest;
}

void print2d(cell items[MAXLEN+1][MAXLEN+1], int n) {
	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= n; j++) {
			cout << items[i][j].cost << "(" << items[i][j].parent << ") ";
		}
		cout << "\n";
	}
}

int edit_distance_dp(string s1, string s2, cell m[MAXLEN+1][MAXLEN+1], int *step) {
	int i, j, k;
	int opt[3];

	for (i = 1; i <= MAXLEN; i++) {
		for (j = 1; j <= MAXLEN; j++) {
			m[i][j].cost   = 0;
			m[i][j].parent = 0;
		}
	}
	for (i = 0; i <= MAXLEN; i++) {
		m[0][i].cost = i;
		m[0][i].parent = INSERT;

		m[i][0].cost = i;
		m[i][0].parent = DELETE;
	}

	for (i = 1; i <= s1.length(); i++) {
		for (j = 1; j <= s2.length(); j++) {
			*step = *step + 1;
			opt[MATCH]  = m[i - 1][j - 1].cost + (s1[i - 1] != s2[j - 1]);
			opt[INSERT] = m[i][j - 1].cost + 1;
			opt[DELETE] = m[i - 1][j].cost + 1;

			m[i][j].cost   = opt[MATCH];
			m[i][j].parent = MATCH;

			for (k = INSERT; k <= DELETE; k++) {
				if (opt[k] < m[i][j].cost) {
					m[i][j].cost   = opt[k];
					m[i][j].parent = k;
				}
			}
		}
	}

	return m[i-1][j-1].cost;
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		printf("Usage: [str1] [str2]");
		return 1;
	}
	string str1 = argv[1];
	string str2 = argv[2];

	cout << str1 << " " << str2 << "\n";

	int step = 0;
	cout << "Edit distance: " << edit_distance(str1, str2, str1.length(), str2.length(), &step) << "\n";
	cout << "step: " << step << "\n";

	cell m[MAXLEN + 1][MAXLEN + 1];

	step = 0;
	cout << "Edit distance DP: " << edit_distance_dp(str1, str2, m, &step) << "\n";
	cout << "step: " << step << "\n";

	return 0;
}

