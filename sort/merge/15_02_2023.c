#include <stdio.h>

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

void merge(int *items, int low, int middle, int high) {
  int i, j, k;
  int n1 = middle - low +1;
  int n2 = high - middle;
  int buffer1[n1], buffer2[n2];
  j = k = 0;

  for (i = low; i <= middle; i++) {
	buffer1[j++] = items[i];
  }
  for (i = middle + 1; i <= high; i++) {
	buffer2[k++] = items[i];
  }

  j = k = 0;
  i = low;

  while(j < n1 && k < n2) {
	if (buffer1[j] < buffer2[k]) {
	  items[i++] = buffer1[j++];
	} else {
	  items[i++] = buffer2[k++];
	}
  }

  while(j < n1) {
    items[i++] = buffer1[j++];
  }
  while(k < n2) {
    items[i++] = buffer2[k++];
  }
}

void sort(int *items, int low, int high) {
  if (low < high) {
	int middle = (low + high) / 2;
	sort(items, low, middle);
	sort(items, middle + 1, high);
	merge(items, low, middle, high);
  }
}
