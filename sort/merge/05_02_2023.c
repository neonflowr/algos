#include <stdio.h>
#include "init.h"

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void merge(int *items, int low, int middle, int high) {
  int i;
  int j, k; // index for buffers
  int size1 = middle - low + 1;
  int size2 = high - middle;
  j = k = -1;
  int x[size1], y[size2];

  for (i = low; i <= middle; i++) {
    x[++j] = items[i];
  }
  for (i = middle + 1; i <= high; i++) {
	y[++k] = items[i];
  }

  i = low;
  j = 0;
  k = 0;

  // Merge
  while(j < size1 && k < size2) {
	if (x[j] < y[k]) {
	  items[i++] = x[j++];
	} else {
	  items[i++] = y[k++];
	}
  }

  while(j < size1) {
	items[i++] = x[j++];
  }
  while (k < size2) {
    items[i++] = y[k++];
  }

}

void sort(int *items, int low, int high) {
  if (low < high) {
	int middle = (low + high) / 2;
	sort(items, low, middle);
	sort(items, middle + 1, high);
	merge(items, low, middle, high);
  }
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}
