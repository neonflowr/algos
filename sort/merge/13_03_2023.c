#include <stdio.h>
#include "init.h"

void merge(int* items, int low, int middle, int high) {
	int i;
	int j, k;

	j = k = 0;

	int n1 = middle - low + 1;
	int n2 = high - middle;

	int b1[n1], b2[n2];

	for (i = low; i <= middle; i++) {
		b1[j++] = items[i];
	}
	for (i = middle + 1; i <= high; i++) {
		b2[k++] = items[i];
	}

	j = k = 0;
	i = low;

	// Merge
	while (j < n1 && k < n2) {
		if (b1[j] < b2[k]) {
			items[i++] = b1[j++];
		}
		else {
			items[i++] = b2[k++];
		}
	}

	while (j < n1) {
		items[i++] = b1[j++];
	}

	while (k < n2) {
		items[i++] = b2[k++];
	}
}

void sort(int* items, int low, int high) {
	if (low < high) {
		int middle = (low + high) / 2;
		sort(items, low, middle);
		sort(items, middle + 1, high);
		merge(items, low, middle, high);
	}
}

void print(int* items, int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", items[i]);
	}
	printf("\n");
}

