#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **l, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;
  node->next = *l;
  *l = node;
}

void print_list(list *l) {
  while (l != NULL) {
	printf("%d ", l->data);
	l = l->next;
 }
  printf("\n");
}

list *find_middle(list *l) {
  list *slow = l;
  list *fast = l;

  while(fast->next != NULL) {
	fast = fast->next;
	if (fast->next)
	  fast = fast->next;
	else
	  break;
	slow = slow->next;
  }

  return slow;
}

list * merge(list *left, list *right) {
  if (!left)
	return right;
  if (!right)
	return left;

  if (left->data < right->data) {
	left->next = merge(left->next, right);
	return left;
  } else {
    right->next = merge(left, right->next);
	return right;
  }
}

void sort_list(list **head) {
  if (*head == NULL || (*head)->next == NULL)
    return;

  // Split into 2 lists
  list *middle = find_middle(*head);
  list *left = *head;
  list *right = middle->next;
  middle->next = NULL;

  sort_list(&left);
  sort_list(&right);

  *head = merge(left, right);
}
