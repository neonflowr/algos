void merge(int items[], int low, int middle, int high) {
  int i, j, k;
  j = k = 0;
  int n1 = middle - low + 1;
  int n2 = high - middle;

  int buf1[n1], buf2[n2];

  for (i = low; i <= middle; i++) {
    buf1[j++] = items[i];
  }
  for (i = middle + 1; i <= high; i++) {
    buf2[k++] = items[i];
  }

  j = k = 0;
  i = low;

  while(j < n1 && k < n2) {
    if (buf1[j] < buf2[k]) {
      items[i++] = buf1[j++];
    } else {
      items[i++] = buf1[k++];
    }
  }

  while (j < n1) {
    items[i++] = buf1[j++];
  }

  while (k < n2) {
    items[i++] = buf2[k++];
  }
}


void sort(int items[], int low, int high) {
  if (low < high) {
    int middle = (low + high) / 2;
    sort(items, low, middle);
    sort(items, middle + 1, high);
    merge(items, low, middle, high);
  }
}
