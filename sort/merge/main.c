#include "13_03_2023.c"
#include "init.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

void array();
void linked_list();

int main(int argc, char *argv[]) {
  printf("マージソートをテスト中\n");
  array();
  /* linked_list(); */
  getchar();
  return 0;
}

/* void linked_list() { */
/*   struct List **head; */
/*   *head = NULL; */

/*   insert(head, 234); */
/*   insert(head, 324); */
/*   insert(head, 31); */
/*   insert(head, 1323); */
/*   insert(head, 123); */
/*   insert(head, 423); */
/*   insert(head, 533); */
/*   insert(head, 5); */
/*   insert(head, 6); */
/*   insert(head, 12); */
/*   insert(head, 214); */
/*   insert(head, 1); */
/*   insert(head, 3); */
/*   insert(head, 2); */
/*   insert(head, 4); */

/*   printf("Initial list\n"); */
/*   print_list(*head); */

/*   sort_list(head); */

/*   printf("Sorted list\n"); */
/*   print_list(*head); */

/*   free(*head); */
/*   *head = NULL; */
/* } */

void array() {
  printf("配列でテスト\n");
  int items[15] = {4,   2,   3,   1,    214, 12,  6,  5,
                   533, 423, 123, 1323, 31,  324, 234};

  print(items, 15);

  sort(items, 0, 14);

  print(items, 15);

  assert(items[0] == 1);;
  assert(items[1] == 2);;
  assert(items[13] == 533);;
  assert(items[14] == 1323);
}
