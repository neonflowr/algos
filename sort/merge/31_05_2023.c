#include <stdlib.h>
#include "init.h"

typedef struct List {
	int data;
	struct List* next;
} list;

void insert(list** head, int value) {
	list* l = malloc(sizeof(list));
	l->data = value;
	l->next = *head;
	*head = l;
}

void find_middle(list* l) {
	list* fast = l;
	list* slow = l;

	while (fast->next != NULL) {
		fast = fast->next ? fast->next : fast;
		if (fast->next)
			fast = fast->next;
		else
			break;
		slow = slow->next;
	}

	return slow;
}

list* merge(list* left, list* right) {
	if (!left)
		return right;
	if (!right)
		return left;

	if (left->data < right->data) {
		left->next = merge(left->next, right);
		return left;
	}
	else {
		right->next = merge(left, right->next);
		return right;
	}
}



void sort(list** head) {
	if (!*head || !*head->next) {
		return;
	}

	list* middle = find_middle(*head);
	list* left = *head;
	list* right = middle->next;
	middle->next = NULL;

	sort(&left);
	sort(&right);

	*head = merge(left, right);
}

