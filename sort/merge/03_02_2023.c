#include <stdio.h>
#include <stdlib.h>

typedef struct queue {
  int *items;
  int front;
  int back;
  unsigned int capacity;
  int size;
} queue;

queue *create_queue(unsigned int capacity) {
  queue *q = malloc(sizeof(queue));
  q->items = malloc(capacity * sizeof(int));
  q->capacity = capacity;
  q->front = 0;
  q->back = q->capacity - 1;
  q->size = 0;
  return q;
}

int is_full(queue *q) { return q->size == q->capacity; }
int is_empty(queue *q) { return q->size == 0; }

void enqueue(queue *q, int value) {
  if (is_full(q))
    return;

  q->back = (q->back + 1) % q->capacity;
  q->items[q->back] = value;
  q->size++;
}

int dequeue(queue *q) {
  if (is_empty(q))
    return -1;

  int val = q->items[q->front];
  q->front = (q->front + 1) % q->capacity;
  q->size--;
  return val;
}

int head_queue(queue *q) {
  if (is_empty(q))
    return -1;
  return q->items[q->front];
}

void print_queue(queue *q) {
  int i = q->front;
  while (i != q->back) {
    printf("%d ", q->items[i]);
    i = (i + 1) % q->capacity;
  }
  printf("%d", q->items[i]);
  printf("\n");
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");
}

void merge(int *items, int low, int middle, int high) {
  int i;
  queue *buffer1 = create_queue(100);
  queue *buffer2 = create_queue(100);

  for (i = low; i <= middle; i++) {
    enqueue(buffer1, items[i]); // [2, 4]
  }
  for (i = middle + 1; i <= high; i++) {
    enqueue(buffer2, items[i]); // [1, 3]
  }

  printf("Queue 1: ");
  print_queue(buffer1);
  printf("Queue 2: ");
  print_queue(buffer2);

  i = low; // low = 0 mid = 1 high = 3
  while (!(is_empty(buffer1) || is_empty(buffer2))) {
    if (head_queue(buffer1) <= head_queue(buffer2)) {
      items[i++] = dequeue(buffer1);
    } else {
      items[i++] = dequeue(buffer2);
    }
  }

  while (!is_empty(buffer1)) {
    items[i++] = dequeue(buffer1);
  }

  while (!is_empty(buffer2)) {
    items[i++] = dequeue(buffer2);
  }

  free(buffer1);
  free(buffer2);

  printf("Params: %d %d %d\n", low, middle, high);
  printf("Array after merge:\n");
  print(items, 15);
}

void sort(int *items, int low, int high) {
  int middle;
  if (low < high) {
    middle = (low + high) / 2;
    sort(items, low, middle);
    sort(items, middle + 1, high);
    merge(items, low, middle, high);
  }
}
