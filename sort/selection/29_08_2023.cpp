#include <vector>

using namespace std;

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void sort(vector<int> items) {
  int min;
  for(int i = 0; i < items.size(); i++) {
    min = i;
    for (int j = i + 1; j < items.size(); j++){
      if (items[j] < items[min]) {
        min = j;
      }
    }
    swap(&items[min], &items[i]);
  }
}
