#include <stdio.h>

void swap(int *i, int *j) {
  int tmp = *i;
  *i = *j;
  *j = tmp;
}

void ssort(int *items, int n) {
  for (int i = 0; i < n; i++) {
	int min = i;
	for (int j = i + 1; j < n; j++) {
	  if (items[j] < items[min]) {
		min = j;
	  }
	}
	swap(&items[i], &items[min]);
  }
}

int main() {
  printf("選択ソートを実装練習\n");
  int items[4] = {3, 1, 4, 2};

  ssort(items, 4);

  printf("ソートされた配列:\n");
  for(int i = 0; i < 4; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
  return 0;
}
