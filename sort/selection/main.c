#include "10_03_2023.c"
#include "init.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define SIZE 4

int main(int argc, char *argv[]) {
  int items[SIZE] = {4, 2, 1 ,3};

  printf("Testing\n");
  print(items, SIZE);

  assert(items[0] == 4);
  assert(items[SIZE-1] == 3);

  sort(items, SIZE);

  assert(items[0] == 1);
  assert(items[SIZE - 1] == 4);

  print(items, SIZE);

  sort(items, SIZE);
  print(items, SIZE);

  assert(items[0] == 1);
  assert(items[SIZE - 1] == 4);

  return 0;
}
