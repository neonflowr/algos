#include <stdio.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void sort(int *items, int n) {
  for (int i = 0; i < n; i++) {
	int min = i;
	for (int j = i; j  < n; j++) {
	  if (items[j] < items[min]) {
		min = j;
	  }
	}

	swap(&items[i], &items[min]);
  }
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}
