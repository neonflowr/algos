#include <stdio.h>


void swap(int* x, int* y) {
  int temp = *x;
  *x = *y;
  *y = temp;
}

void selsort(int* l, int size) {
  for(int i = 0; i < size; i++) {
	int min_index = i;
	for (int j = i + 1; j < size; j++) {
	  if (l[j] < l[min_index]) {
		min_index = j;
	  }
	}

	swap(&l[i], &l[min_index]);
  }
}
