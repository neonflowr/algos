#include <stdio.h>

int *s_sort(int *items, int size);
void swap(int *items, int from, int to);

int main() {
  printf("Sel sort testing\n");

  int nums[11] = {2, 3, 1, 5, 10, 28, 523, 35, 583, 4, 6};
  int numsSize = 11;

  int *sorted = s_sort(nums, numsSize);

  printf("sorted: ");
  for (int i = 0; i < numsSize; i++)
    printf("%d ", sorted[i]);

  printf("\n");

  return 0;
}

int *s_sort(int *items, int size) {
  for (int i = 0; i < size; i++) {
    int min = items[i];
    int min_index = i;

    for (int j = i + 1; j < size; j++) {
      if (items[j] < min) {
        min = items[j];
        min_index = j;
      }
    }

    swap(items, i, min_index);
  }

  return items;
}

void swap(int *items, int from, int to) {
  int temp = items[from];
  items[from] = items[to];
  items[to] = temp;
}
