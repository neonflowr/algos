#include <stdio.h>

void swap(int* x, int* y) {
  // Swapping pointers are tricky...
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void isort(int* l, int size) {
  for (int i = 1; i < size; i++) {
	int j = i;
	while(j > 0 && l[j-1] > l[j]) {
	  swap(&l[j-1], &l[j]);
	  j--;
	}
  }
}
