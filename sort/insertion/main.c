#include "init.h"
#include "13_02_2023.c"
#include <stdio.h>
#include <assert.h>

int main() {
  printf("挿入ソートをテスト中\n");

  int items[4] = {4, 2, 1, 3};

  assert(items[0] == 4);
  assert(items[3] == 3);

  printf("Initial items: ");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");

  sort(items, 4);

  assert(items[0] == 1);
  assert(items[3] == 4);

  printf("Sorted\n");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }

  printf("\n");

  sort(items, 4);

  assert(items[0] == 1);
  assert(items[3] == 4);

  printf("Sorted\n");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }

  printf("\n");

  return 0;
}
