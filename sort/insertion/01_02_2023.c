void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void sort(int *items, int n) {
  for (int i = 1; i < n; i++) {
	for (int j = i; j > 0; j--) {
	  if (items[j] < items[j-1]) {
		swap(&items[j], &items[j-1]);
	  }
	}
  }
}
