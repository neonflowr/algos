#include <stdio.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void sort(int *items, int n) {
  for (int i = 1; i < n; i++) {
	int j = i;
	while (j > 0 && items[j] < items[j-1]) {
	  swap(&items[j], &items[j-1]);
	  j--;
	}
  }
}
