#include <stdio.h>

void isort(int *items, int size);
void swap(int *items, int x, int y);
void print_list(int *items, int size);

int main() {
  int items[8] = {34, 15, 6, 2, 1, 15, 124, 525};
  int size = 8;
  int *p_item = &items[0];
  printf("Starting: \n");
  print_list(p_item, size);

  printf("Sort...\n");
  isort(p_item, size);

  printf("Sorted: \n");
  print_list(p_item, size);

  return 0;
}

void isort(int *items, int size) {
  for (int i = 1; i < size; i++) {
    int j = i;
    while (j > 0 && items[j - 1] > items[j]) {
      swap(items, j - 1, j);
      j--;
    }
  }
}

void swap(int *items, int x, int y) {
  int tmp = items[x];
  items[x] = items[y];
  items[y] = tmp;
}

void print_list(int *items, int size) {
  printf("List: ");
  for (int i = 0; i < size; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");
}
