#include <stdio.h>
#include <stdlib.h>

void swap(int *i, int *j) {
  int tmp = *i;
  *i = *j;
  *j = tmp;
}

void isort(int *items, int n) {
  for (int i = 1; i < n; i++) {
    int j = i;
    while (j > 0 && items[j] < items[j - 1]) {
      swap(&items[j], &items[j - 1]);
      j--;
    }
  }
}

int main() {
  int items[4] = {4, 2, 1, 3};
  isort(items, 4);

  printf("Sorted\n");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");
  return 0;
}
