#include <stdio.h>

void swap(int *x, int *y)  {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void heapify(int *items, int n, int i) {
  int largest = i;
  int left = 2 * i + 1;
  int right = left + 1;

  if (left < n && items[left] >items[largest])
	largest = left;
  if (right < n && items[right] > items[largest])
	largest = right;
  if (largest != i ) {
	swap(&items[i], &items[largest]);
	heapify(items, n, largest);
  }
}

void heapsort(int *items, int n) {
  // Build max heap
  for (int i = n / 2 - 1; i >= 0; i--) {
	heapify(items, n, i);
  }

  // Sort
  for (int i = n - 1; i >= 0; i--) {
	// Essentially the pop operation
	swap(&items[0], &items[i]);
	heapify(items, i, 0);
  }
}
