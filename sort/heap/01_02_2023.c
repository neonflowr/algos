#include <stdio.h>
#include <stdlib.h>

#define HEAP_BASE_INDEX 1

typedef struct PQ {
  int *items;
  unsigned int capacity;
  int count;
} pq;

int left_child(int index) { return 2 * index; }
int right_child(int index) { return 2 * index + 1; }
int parent(int index) {
  int val = index / 2;
  return val;
}

pq *create(unsigned int capacity) {
  pq *q = malloc(sizeof(pq));
  q->items = malloc(capacity * sizeof(int));
  q->capacity = capacity;
  q->count = HEAP_BASE_INDEX;
  return q;
}

void free_pq(pq *q) {
  free(q->items);
  q->items = NULL;
  free(q);
  q = NULL;
}

int is_full(pq *q) { return q->count == q->capacity; }
int is_empty(pq *q) { return q->count == HEAP_BASE_INDEX; }

// Min heap
int satisfy_heap_invariant(pq *q, int index) {
  if (index == HEAP_BASE_INDEX)
    return 1;
  if (q->items[parent(index)] < q->items[index])
    return 1;
  return 0;
}

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void bubble_down(pq *q, int index) {
  if (index >= q->count)
    return;

  int min = index;
  for (int i = 0; i <= 1; i++) {
    int child_index = 2 * index + i;
    if ((child_index < q->count) && q->items[child_index] < q->items[min]) {
      min = child_index;
    }
  }

  if (min == index)
    return;

  swap(&q->items[index], &q->items[min]);
  bubble_down(q, min);
}
void bubble_up(pq *q, int index) {
  if (satisfy_heap_invariant(q, index))
    return;
  int parent_index = parent(index);
  swap(&q->items[parent_index], &q->items[index]);
  bubble_up(q, parent_index);
}

void insert(pq *q, int value) {
  // Insert at n
  if (is_full(q)) {
    return;
  }

  q->items[q->count] = value;
  printf("Insert value %d\n", value);
  bubble_up(q, q->count);
  q->count++;
}

void *find_min(pq *q) { return &q->items[HEAP_BASE_INDEX]; }

int delete_min(pq *q) {
  // Replace the top elem with the rightmost elem
  // Then check its children and bubble down
  if (is_empty(q))
    return -1;

  int val = q->items[HEAP_BASE_INDEX];
  q->items[HEAP_BASE_INDEX] = q->items[--q->count];
  printf("Delete %d\n", val);
  bubble_down(q, HEAP_BASE_INDEX);
  return val;
}

void print(pq *q) {
  for (int i = 1; i < q->count; i++) {
    printf("%d ", q->items[i]);
  }
  printf("\n");
}

void heapsort(int *items, int n) {
  pq *q = create(100);
  for (int i = 0; i < n; i++) {
	insert(q, items[i]);
  }

  // Sort
  for (int i = 0; i < n; i++) {
	items[i] = delete_min(q);
  }
  free_pq(q);
}
