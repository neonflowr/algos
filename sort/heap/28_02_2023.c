#include "init.h"
#include <stdio.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void heapify(int *items, int n, int i) {
  // Essentially bubble down
  int largest = i;
  int left = 2 * i + 1;
  int right = 2 * i + 2;

  if (left < n && items[left] > items[largest]) {
    largest = left;
  }
  if (right < n && items[right] > items[largest]) {
    largest = right;
  }

  if (largest != i) {
    swap(&items[i], &items[largest]);
    heapify(items, n, largest);
  }
}

void heapsort(int *items, int n) {
  // Heapify
  for (int i = n / 2 - 1; i >= 0; i--) {
    heapify(items, n, i);
  }

  // Pop operation essentially
  for (int i = n - 1; i >= 0; i--) {
    swap(&items[i], &items[0]);
    heapify(items, i, 0);
  }
}
