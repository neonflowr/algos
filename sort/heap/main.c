#include "12_03_2023.c"
#include "init.h"
#include <stdio.h>
#include <assert.h>

int main() {
  int items[4] = {4, 2, 1, 3};

  printf("Initial items: ");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");

  assert(items[0] == 4);
  assert(items[1] == 2);
  assert(items[2] == 1);
  assert(items[3] == 3);

  heapsort(items, 4);

  printf("Sorted\n");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }

  assert(items[0] == 1);
  assert(items[1] == 2);
  assert(items[2] == 3);
  assert(items[3] == 4);


  printf("\n");

  heapsort(items, 4);

  printf("Sorted\n");
  for (int i = 0; i < 4; i++) {
    printf("%d ", items[i]);
  }

  assert(items[0] == 1);
  assert(items[1] == 2);
  assert(items[2] == 3);
  assert(items[3] == 4);

  printf("\n");

  getchar();

  return 0;
}
