#include <vector>

using namespace std;

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

// bubble down
void heapify(vector<int> items, int n, int i) {
  int largest = i;
  int left = 2 * i + 1;
  int right = 2 * i + 2;

  if (left < n  && items[left] > items[largest]) {
    largest = left;
  }
  if (right < n && items[right] > items[largest]) {
    largest = right;
  }

  if (largest != i) {
    swap(&items[i], &items[largest]);
    heapify(items, n, largest);
  }
}

void sort(vector<int> items) {
  for(int i = items.size() / 2 - 1; i >=0; i--) {
    heapify(items, items.size(), i);
  }

  for(int i = items.size() - 1; i > 0; i--) {
    swap(&items[i], &items[0]);
    heapify(items, i, 0);
  }
}
