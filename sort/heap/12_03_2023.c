#include <stdio.h>
#include "init.h"

void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void heapify(int* items, int n, int s) {
	int max = s;
	int child_i = 2 * s + 1;

	if (child_i < n && items[child_i] > items[max])
		max = child_i;
	child_i++;
	if (child_i < n && items[child_i] > items[max])
		max = child_i;

	if (max != s) {
		swap(&items[max], &items[s]);
		heapify(items, n, max);
	}
}


void heapsort(int* items, int n) {
	// heapify 
	for (int i = (n / 2) - 1; i >= 0; i--) {
		heapify(items, n, i);
	}

	printf("Heap\n");
	for (int i = 0; i < 4; i++) {
		printf("%d ", items[i]);
	}


	// pop
	for (int i = n - 1; i >= 0; i--) {
		swap(&items[0], &items[i]);
		heapify(items, i, 0);
	}
}
