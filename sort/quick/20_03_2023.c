#include <stdio.h>
#include "init.h"

void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

int median_of_three(int* items, int low, int high) {
	int middle = (low + high) / 2;
	if (middle < low) {
		swap(&items[middle], &items[low]);
	}
	if (high < low) {
		swap(&items[low], &items[high]);
	} 
	if (high < middle) {
		swap(&items[middle], &items[high]);
	}
	return middle;
}

int partition(int* items, int low, int high) {
	int pivot = median_of_three(items, low, high);
	int firsthigh = low;

	for (int i = low; i <= high; i++) {
		if (items[i] < items[pivot]) {
			if (firsthigh == pivot) {
				pivot = i;
			}
			swap(&items[i], &items[firsthigh++]);
		}
	}
	swap(&items[pivot], &items[firsthigh]);

	return firsthigh;
}


void sort(int* items, int low, int high) {
	if (low < high) {
		int p = partition(items, low, high);
		sort(items, low, p - 1);
		sort(items, p + 1, high);
	}
}

