#include <stdio.h>
#include <stdlib.h>


void swap(int* x, int* y) {
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

int partition(int* items, int low, int high) {
	int pivot = high; 
	int firsthigh = low;

	for (int i = low; i < high; i++) {
		if (items[i] < items[pivot]) {
			swap(&items[i], &items[firsthigh]);
			firsthigh++;
		}
	}
	swap(&items[pivot], &items[firsthigh]);

	return firsthigh;
}

void sort(int* items, int low, int high) {
	if (low < high) {
		int p = partition(items, low, high);
		sort(items, low, p - 1);
		sort(items, p + 1, high);
	}
}

