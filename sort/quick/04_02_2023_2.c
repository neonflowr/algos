#include <stdio.h>
#include <stdlib.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int partition(int *items, int low, int high) {
  int p = high; // pick random pivot
  int first_higher_item = low; // stores the index of the next item higher than the pivot so we can swap it with the next smaller item we find

  // Comparing to pivot, swap the smaller elements and the bigger elements
  for (int i = low; i < high; i++) {
	if (items[i] < items[p]) {
	  swap(&items[i], &items[first_higher_item]);
	  first_higher_item++;
	}
  }
  swap(&items[first_higher_item], &items[p]); // swap the last item higher than the pivot with the pivot, meaning now left to the pivot is all smaller items and right is all bigger items

  return first_higher_item;
}

void sort(int *items, int low, int high) {
  if (low < high) {
	int p = partition(items, low, high);
	sort(items, low, p - 1);
	sort(items, p + 1, high);
  }
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}
