#include "init.h"
#include "21_02_2023.c"
#include <stdio.h>
#include <assert.h>

void print(int *items, int n) {
  for(int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

int main(int argc, char *argv[]) {
  printf("クイックソートをテスト中\n");

  int items[6] = {4, 7, 2, 1, 3, 5};

  assert(items[0] == 4);
  assert(items[5] == 5);

  print(items, 6);

  sort(items, 0, 5);

  print(items, 6);

  assert(items[0] == 1);
  assert(items[1] == 2);
  assert(items[2] == 3);
  assert(items[3] == 4);
  assert(items[4] == 5);
  assert(items[5] == 7);

  sort(items, 0, 5);

  print(items, 6);

  assert(items[0] == 1);
  assert(items[1] == 2);
  assert(items[2] == 3);
  assert(items[3] == 4);
  assert(items[4] == 5);
  assert(items[5] == 7);

  return 0;
}
