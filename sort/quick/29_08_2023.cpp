#include <iostream>
#include <vector>

using namespace std;

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int partition(vector<int> items, int low, int high) {
  int p = high;
  int fh = low;

  for(int i = low; i < high; i++) {
    if (items[i] < items[p]) {
      swap(&items[i], &items[fh]);
      fh++;
    }
  }

  swap(&items[fh], &items[p]);

  return fh;
}

void sort(vector<int> items, int low, int high) {
  if (low < high) {
    int p = partition(items, low, high);
    sort(items, low, middle -1);
    sort(items, middle + 1, high);
  }
}
