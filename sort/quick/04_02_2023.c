#include <stdio.h>
#include "init.h"

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int partition(int *items, int low, int high) {
  int p;
  p = high;
  int firsthigh = low;
  for (int i = low; i < high; i++) {
	if (items[i] < items[p]) {
	  swap(&items[i], &items[firsthigh]);
	  firsthigh++;
	}
  }
  swap(&items[p], &items[firsthigh]);

  return firsthigh;
}

void sort(int *items, int low, int high) {
  if (low < high) {
	int pivot_index = partition(items, low, high);
	sort(items, low, pivot_index - 1);
	sort(items, pivot_index + 1, high);
  }
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}
