#include "19_09_2023.c"
#include "init.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void array();
void linked_list();

int main(int argc, char *argv[]) {
  printf("Testing bubble sort\n");
  array(); 
  //linked_list();
  return 0;
}

 void array() { 
   int items[8] = { 3, 2, 3, 8, 5, 6 , 4 ,1  }; 

   print(items, 8); 

   sort(items, 8); 

   print(items, 8);
} 

//void linked_list() {
//  printf("Linked list bubble sort test\n");
//
//  struct List **head;
//  *head = NULL;
//  insert(head, 3);
//  insert(head, 1);
//  insert(head, 2);
//  insert(head, 4);
//
//  printf("Initial list\n");
//  print(*head);
//
//  assert((*head)->data == 4);
//
//  sort(*head);
//
//  printf("Sorted list\n");
//  print(*head);
//
//  assert((*head)->data == 1);
//}
//
