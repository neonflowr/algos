#include <stdio.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void bsort(int *items, int size) {
  int swapped = 1;
  while (swapped == 1) {
    swapped = 0;
    for (int i = 0; i < size - 1; i++) {
      if (items[i + 1] < items[i]) {
        swap(&items[i + 1], &items[i]);
        swapped = 1;
      }
    }
  }
}

void print_list(int *items, int size) {
  printf("List: ");
  for (int i = 0; i < size; i++) {
    printf("%d ", items[i]);
  }
  printf("\n");
}

int main() {
  printf("Bubble sort test\n");
  int items[8] = {34, 15, 6, 2, 1, 15, 124, 525};
  int size = 8;
  int *p_item = &items[0];
  printf("Starting: \n");
  print_list(p_item, size);

  printf("Sort...\n");
  bsort(p_item, size);

  printf("Sorted: \n");
  print_list(p_item, size);

  return 0;
}
