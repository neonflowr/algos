#include <stdio.h>
#include "init.h"
#include <stdlib.h>

struct List {
  int data;
  struct List *next;
};

void swap(struct List *x, struct List *y) {
  int tmp = x->data;
  x->data = y->data;
  y->data = tmp;
}

void print(struct List *x) {
  while(x != NULL) {
	printf("%d ", x->data);
	x = x->next;
  }
  printf("\n");
}

void insert(struct List **items, int value) {
  struct List *node = malloc(sizeof(struct List));
  node->data = value;
  node->next = *items;
  *items = node;
}

void sort(struct List *items) {
  int swapped = 1;
  while(swapped != 0) {
	swapped = 0;

	struct List *current = items;
	struct List *prev;
	struct List *next;

	while(current->next != NULL) {
	  next = current->next;
	  if (current->data > next->data) {
		swap(current, next);
		swapped = 1;
	  }
	  current = next;
	  prev = current;
	}
  }
}

void *create() {
  struct List *node = malloc(sizeof(struct List));
  node->data = 1;
  node->next = NULL;
  return node;
}
