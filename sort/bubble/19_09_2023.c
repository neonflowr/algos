#include <stdio.h>
#include "init.h"

void print(int* items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

void swap(int* x, int* y) {
	int tmp = *x;
	*x= *y;
	*y = tmp;
}

void sort(int* items, int n) {
	printf("Actual animal\n");
	int swapped = 1;
	int turn = 0;
	int i;

	while (swapped) {
		swapped = 0;

		i = turn;

		while(i + 1 < n) {
			if (items[i] > items[i + 1]) {
				swap(&items[i], &items[i + 1]);
				swapped = 1;
			}
			i += 2;
		}

		printf("State: ");
		print(items, n);

		turn = turn ^ 1;
	}
}

