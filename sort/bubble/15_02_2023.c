#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

void swap(list *x, list *y) {
  int tmp = x->data;
  x->data = y->data;
  y->data = tmp;
}

void sort(list *l) {
  int swapped = 1;
  list *tmp;
  while(swapped == 1) {
    tmp = l;
    swapped = 0;
    while (tmp->next != NULL) {
      if (tmp->next->data < tmp->data) {
        swap(tmp->next, tmp);
        swapped = 1;
      }
      tmp = tmp->next;
	}
  }
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
