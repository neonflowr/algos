#include "init.h"
#include <stdio.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

void sort(int *items, int n) {
  int swapped = 1;
  while (swapped != 0) {
	swapped = 0;

	for (int i = 0; i < n - 1; i++) {
	  if (items[i] > items[i+1]) {
		swap(&items[i], &items[i+1]);
		swapped = 1;
	  }
	}
  }
}

void print(int *items, int n) {
  for (int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}
