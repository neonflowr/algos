// Union-find practice

#define SET_SIZE 100

class union_find {
public: 
	int p[SET_SIZE + 1];
	int size[SET_SIZE + 1];
	int n;

	union_find(int n) {
		for (int i = 1; i <= n; i++) {
			this->p[i] = i; // each set element starts out in its own subset
			this->size[i] = 1;
		}
		s->n = n;
	}

	int find(int x) {
		if (s->p[x] == x) return x;
		return this->find(s->p[x]);
	}

	void union_sets(int s1, int s2) {
		int r1, r2;
		r1 = this->find(s1);
		r2 = this->find(s2);

		if (r1 == r2) return; // already same set

		if (this->size[r1] >= this->size[r2]) {
			this->size[r1] = this->size[r1] + this->size[r2];
			s->p[r2] = r1;
		}
		else {
			this->size[r2] = this->size[r1] + this->size[r2];
			s->p[r1] = r2;

		}
	}

	bool same_component(int s1, int s2) {
		return this->find(s1) == this->find(s2);
	}
}
