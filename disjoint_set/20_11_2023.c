#define SET_SIZE 100

typedef struct _union_find {
  int p[SET_SIZE+1];
  int size[SET_SIZE+1];
  int n;
} uf;
