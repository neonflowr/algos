#include "init.h"
#include "08_02_2023.c"
#include <stdlib.h>
#include <assert.h>

int main(int argc, char *argv[]) {
  printf("ソートされた配列をテスト中\n");

  arr *items = create(6);

  insert(items, 3);
  print(items);
  insert(items, 4);
  print(items);
  insert(items, 2);
  print(items);
  insert(items, 7);
  print(items);
  insert(items, 5);
  print(items);
  insert(items, 51);
  print(items);

  assert(items->items[0] == 2);
  assert(items->items[items->capacity - 1] == 51);

  remove_item(items, 4);
  print(items);
  assert(search(items, 4, 0, items->capacity - 1) == -1);

  remove_item(items, 3);
  assert(search(items, 3, 0, items->capacity - 1) == -1);
  print(items);

  remove_item(items, 57);
  assert(search(items, 57, 0, items->capacity - 1) == -1);
  print(items);

  remove_item(items, 51);
  assert(search(items, 51, 0, items->capacity - 1) == -1);
  print(items);

  remove_item(items, 2);
  assert(search(items, 2, 0, items->capacity - 1) == -1);
  print(items);

  assert(search(items, 5, 0, items->capacity - 1) == 0);
  assert(search(items, 7, 0, items->capacity - 1) == 1);

  free_arr(&items);

  return 0;
}
