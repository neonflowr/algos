#include "init.h"
#include <stdio.h>
#include <stdlib.h>

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int partition(int *items, int low, int high) {
  int p = high;
  int firsthigh = low;

  for (int i = low; i < high; i++) {
    if (items[i] < items[p]) {
      swap(&items[i], &items[firsthigh]);
      firsthigh++;
    }
  }
  swap(&items[firsthigh], &items[p]);

  return firsthigh;
}

// 多分クイックソートな
void sort(int *items, int low, int high) {
  if (low < high) {
    int pivot = partition(items, low, high);
    sort(items, low, pivot - 1);
    sort(items, pivot + 1, high);
  }
}

typedef struct SortedArray {
  int *items;
  int size;
  unsigned int capacity;
} arr;

arr *create(unsigned int capacity) {
  arr *a = malloc(sizeof(arr));
  a->items = malloc(capacity * sizeof(int));
  a->size = 0;
  a->capacity = capacity;
  return a;
}

int is_full(arr *a) {
  return a->size == a->capacity;
}
int is_empty(arr *a) {
  return a->size == 0;
}

int find_suitable_index(arr *a, int value, int low, int high) {
  int middle = (low + high) / 2;
  if (low < high) {
    if (value > a->items[middle]) {
      return find_suitable_index(a, value, middle + 1, high);
    } else if (value < a->items[middle]) {
      return find_suitable_index(a, value, low, middle - 1);
    }
  }

  // 値はもう配列にある
  if (value == a->items[middle]) return -1;

  return middle;
}

int search(arr *a, int value, int low, int high) {
  if (is_empty(a)) return -1;

  if (low > high) return -1;

  int middle = (low + high) / 2;
  if (value > a->items[middle]) {
	return search(a, value, middle + 1, high);
  } else if (value < a->items[middle]) {
	return search(a, value, low, middle - 1);
  }

  return middle;
}

void insert(arr *a, int value) { // O(log n) + O(n) = O(n)
  if (is_full(a)) return;
  if (a->size == 0) {
	a->items[a->size++] = value;
	return;
  }

  // O(log n)
  int index = find_suitable_index(a, value, 0, a->size - 1);
  if (a->items[index] < value) {
	index++;
  }

  printf("Suitable index for %d is %d\n", value, index);

  int prev = a->items[index];
  int tmp = a->items[index + 1];
  for (int i = index + 1; i <= a->size; i++) { // O(n) worst case
	tmp = a->items[i];
	a->items[i] = prev;
	prev = tmp;
  }
  a->items[index] = value;

  a->size++;
}

int remove_item(arr *a, int value) { // O(log n) + O(n) = O(n)
  if (is_empty(a)) return -1;

  int index = search(a, value, 0, a->size - 1); // O(log n)
  if (index == -1) return -1;

  int tmp = a->items[index];

  for (int i = index; i < a->size; i++) { // O(n)
	if (i == a->size - 1) {
	  a->items[i] = 0;
	} else {
	  a->items[i] = a->items[i + 1];
	}
  }
  a->size--;

  return tmp;
}

void print(arr *a) {
  for (int i = 0; i < a->size; i++) {
	printf("%d ", a->items[i]);
  }
  printf("\n");
}

void free_arr(arr **a) {
  free((*a)->items);
  free(*a);
}
