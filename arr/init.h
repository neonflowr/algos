struct SortedArray;
struct SortedArray *create();
void sort();
void insert();
int remove_item();
void print();
void free_arr();
