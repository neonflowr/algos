#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct SortedArray {
  int *items;
  int size;
  unsigned int capacity;
} arr;

arr *create(unsigned int capacity) {
  arr *a = malloc(sizeof(arr));
  a->items = malloc(capacity * sizeof(int));
  a->size = 0;
  a->capacity = capacity;
  return a;
}

void free_arr(arr **a) {
  free((*a)->items);
  (*a)->items = NULL;
  free(*a);
  *a = NULL;
}

void swap(int *x, int *y) {
  int tmp = *x;
  *x = *y;
  *y = tmp;
}

int partition(int *items, int low, int high) {
  int pivot = high;
  int firsthigh = low;

  for (int i = low; i < high; i++) {
	if (items[i] < items[pivot]) {
	  swap(&items[i], &items[firsthigh++]);
	}
  }

  swap(&items[pivot], &items[firsthigh]);

  return firsthigh;
}

void sort(int *items, int low, int high) {
  if (low < high) {
	int p = partition(items, low, high);
	sort(items, low, p - 1);
	sort(items, p + 1, high);
  }
}


int find_suitable_index(int *items, int low, int high, int value) {
  if (low > high)
	return low;

  int middle = (low + high) / 2;

  if (items[middle] == value) return middle + 1;

  if (items[middle] && items[middle] < value) {
	return find_suitable_index(items, middle + 1, high, value);
  } else {
    return find_suitable_index(items, low, middle - 1, value);
  }
}

int search(arr *a, int value, int low, int high) {
  if (low > high)
	return -1;

  int middle = (low + high) / 2;

  printf("Middle %d %d\n", middle, a->items[middle]);

  if (a->items[middle] == value) return middle;

  if (a->items[middle] && a->items[middle] < value) {
	return search(a, value, middle + 1, high);
  } else {
    return search(a, value, low, middle - 1);
  }
}

void insert(arr *a, int value) {
  int index = find_suitable_index(a->items, 0, a->size, value);

  for (int i = a->size + 1; i >= index; i--) {
	a->items[i] = a->items[i-1];
  }

  a->items[index] = value;
  a->size++;
}

int remove_item(arr *a, int value) {
  int index = search(a, value, 0, a->size);

  if (index == -1) return -1;

  int val = a->items[index];

  for (int i = index; i < a->size; i++) {
	a->items[i] = a->items[i+1];
  }
  a->items[a->size--] = 0;

  return val;
}

void print(arr *a) {
  for (int i = 0; i < a->size; i++) {
    printf("%d ", a->items[i]);
  }
  printf("\n");
}
