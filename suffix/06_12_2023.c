#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _suffix {
  int index;
  char *suff;
} suffix_t;

int suf_comp(const suffix_t *a, const suffix_t *b) {
  return strcmp(a->suff, b->suff);
}

int *build(char *s, int n) {
  suffix_t suffixes[n];
  for(int i = 0; i < n; i++) {
    suffixes[i].suff  = s+i;
    suffixes[i].index = i;
  }

  qsort(suffixes, n, sizeof(suffix_t), suf_comp);

  int *indexes = calloc(n, sizeof(int));
  for(int i = 0; i < n; i++) {
    indexes[i] = suffixes[i].index;
  }

  return indexes;
}

int search(char *s, int n, char *p, int np, int *indexes, int low, int high) {
  if (low <= high) {
    int count = 0;

    int mid   = (low + high) / 2;
    int res   = strncmp(p, s + indexes[mid], np);

    if (res == 0) {
      return count + search(s, n, p, np, indexes, low, mid - 1) + search(s, n, p, np, indexes, mid + 1, high);
    }

    if (res < 0) {
      return search(s, n, p, np, indexes, low, mid - 1);
    } else {
      return search(s, n, p, np, indexes, mid + 1, high);
    }
  }

  return 0;
}
