#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _suffix {
  int index;
  char *suff;
} suffix;

int comp(const suffix *a, const suffix *b) {
  return strcmp(a->suff, b->suff);
}

int *build(char* s, int n) {
  suffix suffixes[n];

  for(int i = 0; i < n; i++) {
    suffixes[i].suff  = s+i;
    printf("Add suffix %s\n", suffixes[i].suff);
    suffixes[i].index = i;
  }
  qsort(suffixes, n, sizeof(suffix), comp);

  printf("Sorted\n");
  for(int i = 0; i < n; i++) {
    printf("%s ", suffixes[i].suff);
  }
  printf("\n");

  int *indexes = calloc(n, sizeof(int));
  for(int i = 0; i < n; i++) {
    indexes[i] = suffixes[i].index;
    printf("Add suffix index %d\n", indexes[i]);
  }

  return indexes;
}

int search(char* s, int n, char *p, int np, int *indexes, int low, int high) {
  if (low <= high) {
    printf("Find pattern %s with len wtf %d\n", p, np);
    int count = 0;

    int mid = (low + high) / 2;
    int res = strncmp(p, s + indexes[mid], np);
    if (res == 0) {
      count++;
      printf("Found at mid %d\n", indexes[mid]);
      return count + search(s, n, p, np, indexes, low, mid - 1) + search(s, n, p, np, indexes, mid + 1, high);
    }

    if (res < 0) {
      return search(s, n, p, np, indexes, low, mid - 1);
    } else {
      return search(s, n, p, np, indexes, mid + 1, high);
    }
  }
  return 0;
}


int main(int argc, char* argv[]) {
  if (argc < 3) {
    printf("Not enough args\n");
    return 0;
  }
  char *s = argv[1];
  int n = strlen(s);
  
  int *suff_arr = build(s, n);

  char *substr = argv[2];
    
  printf("Pattern found %d times\n", search(s, n, substr, strlen(substr), suff_arr, 0, n - 1));

  free(suff_arr);
  
  return 0;
}
