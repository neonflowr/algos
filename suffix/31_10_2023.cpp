#include <iostream>
#include <vector>
#include <string>
#include <stdio.h>

#define ALPHABET_SIZE 26

using namespace std;

class Trie {
public:
  Trie* children[ALPHABET_SIZE];
  int children_count;
  char value;
  bool is_end = false;

  Trie(char val) {
    // Remember that root node is null string
    value = val;
    for(int i = 0; i < ALPHABET_SIZE; i++) {
      children[i] = nullptr;
    }
  }

  static int get_index(char value) {
    int res =value % ALPHABET_SIZE;
    printf("Hash value: %d | %c\n", res, value);
    return res;
  }

  void insert(string s) {
    int n = s.length();

    Trie *current = this;
    char c;
    int index = 0;

    for(int i = 0; i < n; i++) {
      c = s[i];
      index = get_index(c);
      if (current->children[index] == nullptr){
	printf("Index new node %c\n", c);
	current->children[index] = new Trie(c);
      }
      current = current->children[index];
    }
    printf("Final: %c\n", current->value);
    current->is_end = true;
  }

  Trie* search(string s) {
    int n = s.length();
    Trie *current = this;
    int index;
    int i =0 ;
    while(i < n && !current->is_end) {
      index = get_index(s[i]);
      if (current->children[index] == nullptr) {
	return nullptr;
      }
      current = current->children[index];
      i++;
    }
    return current;
  }

  void print() {
    for (int i = 0; i < ALPHABET_SIZE; i++) {
      if (children[i] != nullptr) {
	children[i]->print();
	printf("\n");
      }
    }
    if (value) {
      printf("%c: ", value);
    }
    printf("\n");
  }
};

int main(int argc, char *argv[]) {
  Trie *root = new Trie(0);
  root->insert("abc");
  root->insert("aba");
  root->insert("abd");
  root->insert("THE BEE MOVIE");
  root->print();

  printf("abc found: %d\n", root->search("abc") != nullptr);
  printf("abd found: %d\n", root->search("abd") != nullptr);
  printf("abe found: %d\n", root->search("abe") != nullptr);
  printf("THE BEE MOVIE found: %d\n", root->search("THE BEE MOVIE") != nullptr);
  printf("the bee movie found: %d\n", root->search("the bee movie") != nullptr);
  printf("THE BEE found: %d\n", root->search("THE BEE") != nullptr);

  delete root;
  
  return 0;
}
