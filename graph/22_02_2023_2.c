#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct EdgeNode {
  int y;
  int weight;
  struct EdgeNode *next;
} edge;

void free_edge(edge *l) {
  if (l == NULL) return;
  edge *tmp = l->next;
  free(l);
  free_edge(tmp);
}

typedef struct Graph {
  edge *edges[MAXV + 1];
  int degree[MAXV + 1];
  int nvertices;
  int nedges;
  int directed;
} graph;

graph *create(int directed) {
  graph *g = malloc(sizeof(graph));
  g->nvertices = g->nedges = 0;
  g->directed = directed;
  for (int i = 1; i <= MAXV; i++) {
	g->edges[i] = NULL;
	g->degree[i] = 0;
  }
  return g;
}

void free_graph(graph *g) {
  for (int i = 1; i <= MAXV; i++) {
	free_edge(g->edges[i]);
  }
  free(g);
}

void insert_edge(graph *g, int x, int y, int directed) {
  edge *e = malloc(sizeof(edge));
  e->y = y;
  e->weight = 0;
  e->next = g->edges[x];
  g->edges[x] = e;
  g->degree[x]++;

  if (!directed) {
	insert_edge(g, y, x, 1);
  } else {
	g->nedges++;
  }
}

void read_graph(graph *g, int directed) {
  int edge_count;
  int x, y;

  scanf("%d %d", &(g->nvertices), &edge_count);

  for (int i = 0; i < edge_count; i++) {
	scanf("%d %d", &x, &y);
	insert_edge(g, x, y, directed);
  }
}

void print(graph *g) {
  edge *p;
  for (int i = 1; i <= g->nvertices; i++) {
    printf("%d: ", i);
	p = g->edges[i];
    while (p != NULL) {
	  printf("%d ", p->y);
	  p = p->next;
	}
	printf("\n");
  }
}
