#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct EdgeNode {
  int y;
  int weight;
  struct EdgeNode *next;
} edge;

typedef struct Graph {
  edge *edges[MAXV+1];
  int degree[MAXV+1];
  int nvertices;
  int nedges;
  int directed;
} graph;

graph *create(int directed) {
  int i;

  graph *g = malloc(sizeof(graph));

  g->nvertices = g->nedges = 0;
  g->directed = directed;

  for (i = 1; i <= MAXV; i++) {
	g->degree[i] = 0;
  }
  for (i = 1; i <= MAXV; i++) {
	g->edges[i] = NULL;
  }

  return g;
}

void insert_edge(graph *g, int x, int y, int directed) {
  edge *p = malloc(sizeof(edge));

  p->weight = 0;
  p->y = y;
  p->next = g->edges[x];

  g->edges[x] = p;

  g->degree[x]++;

  if (!directed) {
	insert_edge(g, y, x, 1);
  } else {
	g->nedges++;
  }
}

void read_graph(graph *g, int directed) {
  int m; // number of edges
  int x,y;

  scanf("%d %d", &(g->nvertices), &m);

  for (int i = 1; i <= m; i++) {
	scanf("%d %d", &x, &y);
	insert_edge(g, x, y, directed);
  }
}

void print(graph *g) {
  edge *p;

  for (int i = 1; i <= g->nvertices; i++) {
	printf("%d: ", i);
	p = g->edges[i];
	while(p != NULL) {
	  printf(" %d", p->y);
	  p = p->next;
	}
	printf("\n");
  }
}

void free_graph(graph *g) {
  edge *p, *tmp;
  for (int i = 1; i <= MAXV; i++) {
	p = g->edges[i];

	while(p != NULL) {
	  tmp = p->next;
	  free(p);
	  p = tmp;
	}

    g->edges[i] = NULL;
  }

  free(g);
}
