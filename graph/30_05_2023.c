#include <stdio.h>
#include <stdlib.h>
#include "init.h"

#define MAXV 100

typedef struct Graph {
	int edges[MAXV + 1][MAXV + 1];
	int degree[MAXV + 1];
	int nvertices;
	int nedges;
	int capacity;
	int directed;
} graph;

graph* create(int directed) {
	graph* g = malloc(sizeof(graph));
	g->edges = { 0 };
	g->degree = { 0 };
	g->nvertices = 0;
	g->nedges = 0;
	g->capacity = MAXV;
	g->directed = directed;
	return g;
}

void insert_edge(graph* g, int x, int y, int directed) {
	g->edges[x][y] = 1;
	if (!directed) {
		insert_edge(g, y, x, 1);
	}
	else {
		g->nedges++;
	}
}

int has_connection(graph* g, int x, int y) {
	return g->edges[x][y];
}

