#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct EdgeNode {
  int y;
  int weight;
  struct EdgeNode *next;
} edge;

void free_list(edge *p) {
  edge *tmp;
  while(p != NULL) {
	tmp = p->next;
	free(p);
	p = tmp;
  }
}

typedef struct Graph {
  edge *edges[MAXV+1];
  int degree[MAXV+1];
  int nvertices;
  int nedges;
  int directed;
} graph;

graph *create(int directed) {
  graph *g = malloc(sizeof(graph));
  g->nvertices = g->nedges = 0;
  g->directed = directed;
  for(int i = 1; i <= MAXV; i++) {
	g->edges[i] = NULL;
	g->degree[i] = 0;
  }
  return g;
}

void free_graph(graph *g) {
  for (int i = 1; i <= MAXV; i++) {
	free_list(g->edges[i]);
  }
  free(g);
}

void print(graph *g) {
  edge *p;
  for (int i = 1; i <= g->nvertices; i++) {
	printf("%d: ", i);
	p = g->edges[i];
	while(p != NULL) {
	  printf("%d ", p->y);
	  p = p->next;
	}
	printf("\n");
  }
}

edge *search(edge *p, int value) {
  if (p == NULL)
    return NULL;
  if (p->y == value)
    return p;
  return search(p->next, value);
}

int has_connection(graph *g, int x, int y) {
  return search(g->edges[x], y) != NULL;
}

void insert_edge(graph *g, int x, int y, int directed) {
  edge *p = malloc(sizeof(edge));
  p->y = y;
  p->weight = 0;

  p->next = g->edges[x];
  g->edges[x] = p;

  if (!directed) {
	insert_edge(g, y, x, 1);
  } else {
	g->nedges++;
  }
}
