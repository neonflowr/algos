#include <stdio.h>
#include <stdlib.h>
#include "init.h"

#define MAXV 100

typedef struct EdgeNode {
	int y;
	int weight;
	struct EdgeNode* next;
} edge;

void insertl(edge** l, int y) {
	edge* node = malloc(sizeof(edge));
	node->y = y;
	node->next = *l;
	*l = node;
}

void printl(edge* e) {
	while (e) {
		printf("%d ", e->y);
		e = e->next;
	}
	printf("\n");
}

edge* search(edge* e, int y) {
	if (e == NULL) return;
	if (e->y == y) return e;
	return search(e->next, y);
}

typedef struct Graph {
	edge *edges[MAXV+1];
	int degree[MAXV+1];
	int nvertices;
	int nedges;
	int directed;
} graph;

graph* create(int directed) {
	graph* g = malloc(sizeof(graph));
	for (int i = 1; i <= MAXV; i++) {
		g->edges[i] = NULL;
		g->degree[i] = 0;
	}

	// g->nvertices = MAXV;
	g->nedges = 0;
	g->directed = directed;
	return g;
}

void insert_edge(graph* g, int x, int y, int directed) {
	insertl(&g->edges[x], y);
	if (!directed) {
		insert_edge(g, y, x, !directed);
	}
	else {
		g->nedges++;
	}
}

int has_connection(graph* g, int x, int y) {
	return search(g->edges[x], y) != NULL;
}

void print(graph* g) {
	for (int i = 1; i <= g->nvertices; i++) {
		printf("%d: ", i);
		printl(g->edges[i]);
	}
}

