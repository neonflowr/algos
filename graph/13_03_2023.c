#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct EdgeNode {
	int y;
	int weight;
	struct EdgeNode* next;
} edge;

edge* search(edge* p, int y) {
	if (p == NULL)
		return NULL;
	if (p->y == y)
		return p;
	return search(p->next, y);
}

void free_list(edge* p) {
	edge* tmp;
	while (p != NULL) {
		tmp = p->next;
		free(p);
		p = tmp;
	}
}

typedef struct Graph {
	edge* edges[MAXV+1];
	int degree[MAXV + 1];
	int directed;
	int nvertices;
	int nedges;
} graph;

graph* create(int directed) {
	graph* g = malloc(sizeof(graph));
	for (int i = 1; i <= MAXV; i++) {
		g->edges[i] = NULL;
		g->degree[i] = 0;
	}
	g->directed = directed;
	g->nvertices = g->nedges = 0;
	return g;
}

void free_graph(graph* g) {
	for (int i = 1; i <= MAXV; i++) {
		free_list(g->edges[i]);
	}
	free(g);
}

int has_connection(graph* g, int x, int y) {
	edge* existing_edge = search(g->edges[x], y);
	if (existing_edge)
		return 1;
	return 0;
}

void insert_edge(graph* g, int x, int y, int directed) {
	edge* p = malloc(sizeof(edge));
	p->y = y;
	p->weight = 0;
	p->next = g->edges[x];
	g->edges[x] = p;
	g->degree[x]++;

	if (!directed) {
		insert_edge(g, y, x, 1);
	}
	else {
		g->nedges++;
	}
}

void print(graph* g) {
	printf("Graph:\n");
	edge* p;
	for (int i = 1; i <= g->nvertices; i++) {
		printf("%d: ", i);
		p = g->edges[i];
		while (p != NULL) {
			printf("%d ", p->y);
			p = p->next;
		}
		printf("\n");
	}
}

