struct Graph;
struct EdgeNode;
struct Graph *create();
void free_graph();
void insert_edge();
void read_graph();
int has_connection();
void print();
