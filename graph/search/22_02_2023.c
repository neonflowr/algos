#include "../init.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct List {
  int data;
  struct List *next;
} list;

void free_list(list *l) {
  list *tmp;
  while(l != NULL) {
	tmp = l->next;
	free(l);
	l = tmp;
  }
}

typedef struct Queue {
  list *front;
  list *back;
  int size;
} queue;

queue *create_queue() {
  queue *q = malloc(sizeof(queue));
  q->front = q->back = NULL;
  q->size = 0;
  return q;
}

void enqueue(queue *q, int value) {
  list *l = malloc(sizeof(list));
  l->data=  value;
  l->next = NULL;
  if (q->front == NULL) {
	q->front = q->back = l;
  } else {
	q->back->next = l;
	q->back = l;
  }
  q->size++;
}

int is_empty(queue *q) {
  return q->size == 0;
}

int dequeue(queue *q) {
  if (is_empty(q)) return -1;
  list *tmp = q->front;
  q->front = tmp->next;
  int val = tmp->data;
  free(tmp);
  tmp = NULL;
  q->size--;
  return val;
}

void free_queue(queue *q) {
  free_list(q->front);
  free(q);
}

typedef struct EdgeNode {
  int y;
  int weight;
  struct EdgeNode *next;
} edge;

void free_edge(edge *l) {
  if (l == NULL) return;
  edge *tmp = l->next;
  free(l);
  free_edge(tmp);
}

typedef struct Graph {
  edge *edges[MAXV + 1];
  int degree[MAXV + 1];
  int nvertices;
  int nedges;
  int directed;
} graph;

graph *create(int directed) {
  graph *g = malloc(sizeof(graph));
  g->nvertices = g->nedges = 0;
  g->directed = directed;
  for (int i = 1; i <= MAXV; i++) {
	g->edges[i] = NULL;
	g->degree[i] = 0;
  }
  return g;
}

void free_graph(graph *g) {
  for (int i = 1; i <= MAXV; i++) {
	free_edge(g->edges[i]);
  }
  free(g);
}

void insert_edge(graph *g, int x, int y, int directed) {
  edge *e = malloc(sizeof(edge));
  e->y = y;
  e->weight = 0;
  e->next = g->edges[x];
  g->edges[x] = e;
  g->degree[x]++;

  if (!directed) {
	insert_edge(g, y, x, 1);
  } else {
	g->nedges++;
  }
}

void read_graph(graph *g, int directed) {
  int edge_count;
  int x, y;

  scanf("%d %d", &(g->nvertices), &edge_count);

  for (int i = 0; i < edge_count; i++) {
	scanf("%d %d", &x, &y);
	insert_edge(g, x, y, directed);
  }
}

void print(graph *g) {
  edge *p;
  for (int i = 1; i <= g->nvertices; i++) {
    printf("%d: ", i);
	p = g->edges[i];
    while (p != NULL) {
	  printf("%d ", p->y);
	  p = p->next;
	}
	printf("\n");
  }
}

int bfs(graph *g, int start) {
  int discovered[MAXV+1];
  int processed[MAXV+1];
  int parent[MAXV+1];

  edge *e;
  int v, y;

  // Sum all the vertices for testing
  int sum = 0;

  queue *frontier = create_queue();

  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = 0;
	processed[i] = 0;
	parent[i] = 0;
  }

  discovered[start] = 1;
  enqueue(frontier, start);

  printf("Start\n");

  while(!is_empty(frontier)) {
	v = dequeue(frontier);

	// process vertex here
	printf("Process vertex %d\n", v);
	sum += v;

	e = g->edges[v];
	while(e != NULL) {
	  y = e->y;

	  // process edge here
	  printf("Process edge %d -> %d\n", v, y);

	  if (discovered[y] || processed[y]) {
		// ignore
		printf("Ignore\n");
	  } else {
		discovered[y] = 1;
		parent[y] = v;
		enqueue(frontier, y);
	  }

	  e = e->next;
	}

	processed[v] = 1;
  }

  free_queue(frontier);

  return sum;
}
