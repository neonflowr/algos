#include "graph.c"
#include <stdio.h>

int discovered[MAXV+1];

void dfs_routine(graph *g, int s) {
  printf("Vertex %d\n", s);
  discovered[s] = 1;
  edge *p = g->edges[s];
  while(p != NULL) {
	if (!discovered[p->y]) {
	  dfs_routine(g, p->y);
	}
	p = p->next;
  }
}

// Sweep all vertices in case of a disconnected graph
void dfs(graph *g) {
  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = 0;
  }

  for (int i = 1; i <= g->nvertices; i++) {
	if (!discovered[i]) {
	  printf("running routine\n");
	  dfs_routine(g, i);
	}
  }
}
