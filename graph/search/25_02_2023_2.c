#include "graph.c"
#include "init.h"
#include <stdio.h>

void dfs_cycle();

int has_cycle(graph *g) {
  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];
  int has_cycle = 0;

  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = 0;
	processed[i] = 0;
	parent[i] = 0;
  }

  dfs_cycle(g, 1, discovered, processed, parent, &has_cycle);

  return has_cycle;
}

void dfs_cycle(graph *g, int s, int discovered[], int processed[], int parent[], int *has_cycle) {
  discovered[s] = 1;

  // process vertex

  edge *e = g->edges[s];
  while(e != NULL) {
	if (!discovered[e->y]) {
	  // process edge
	  parent[e->y] = s;
	  dfs_cycle(g, e->y, discovered, processed, parent, has_cycle);
	} else if ( (!processed[e->y] && parent[s] != e->y )  || g->directed) { //
	  // Example for the first predicate is in cycles
	  // So essentially, if this block runs and the graph is not directed, we have a cycle since there's a back edge
	  if (!g->directed) {
		*has_cycle = 1;
		return;
	  }
	  // process edge
	}

	if (*has_cycle == 1)
	  return;

	e = e->next;
  }

  // process vertex late
  processed[s] = 1;
}
