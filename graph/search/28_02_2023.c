#include "graph.c"
#include <stdio.h>

void dfs_subroutine(graph *g, int v, int discovered[], int processed[], int parent[], int *has_cycle) {
  int y;

  discovered[v] = 1;
  edge *p = g->edges[v];

  // process vertex early
  printf("Process vertex early: %d\n", v);

  while(p != NULL) {
	y = p->y;
	if (!discovered[y]) {
	  parent[y] = v;
	  // process edge
	  dfs_subroutine(g, y, discovered, processed, parent, has_cycle);
	} else if ( g->directed || (!processed[y] && parent[v] != y) ) { // latter predicate is in case of a back edge
	  // process edge
	  if (!g->directed) *has_cycle = 1;
	}

	p = p->next;
  }

  processed[v] = 1;

  // process vertex late
  printf("Process vertex late: %d\n", v);
}

int dfs(graph *g) {
  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];

  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = 0;
	processed[i] = 0;
	parent[i] = 0;
  }

  int has_cycle = 0;

  for(int i = 1; i <= g->nvertices; i++) {
	if (!discovered[i]) {
	  dfs_subroutine(g, i, discovered, processed, parent, &has_cycle);
	}
  }

  return has_cycle;
}

int has_cycle(graph *g) {
  return dfs(g);
}
