#include "graph.c"
#include <stdio.h>
#include <stdlib.h>

int discovered[MAXV + 1];
int processed[MAXV + 1];
int entry[MAXV + 1];
int exit_time[MAXV + 1];
int parent[MAXV + 1];
int time = 0;

void dfs_routine(graph *g, int s) {
  time++;

  discovered[s] = 1;
  entry[s] = time;

  int y;

  // process vertex early
  printf("process vertex %d\n", s);

  edge *p = g->edges[s];
  while(p != NULL) {
	y = p->y;
	if (!discovered[y]) {
	  parent[y] = s;
	  // process edge
	  dfs_routine(g, y);
	} else if (  (!processed[y] && parent[s] != y)   || g->directed              ) {
	  // process edge
	}

	p = p->next;
  }

  // process vertex late
  time++;
  exit_time[s] = time;
  processed[s]= 1;
}

void dfs(graph *g, int s) {
  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = 0;
	processed[i] = 0;
	entry[i] = 0;
	exit_time[i] = 0;
	parent[i] = 0;
  }

  dfs_routine(g, s);
}
