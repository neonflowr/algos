#include "10_03_2023_2.c"
#include "init.h"
#include <assert.h>
#include <stdio.h>

void read_graph_file(char *name, graph *g, int directed) {
  FILE *fp;
  if ((fp = fopen(name, "r")) == NULL) {
    fprintf(stderr, "グラフのファイルが読み込んでません\n");
    exit(1);
  } else {
    int edge_count;
    int x, y;

    char *line = malloc(5);
    char *
        tmp; // グラフのファイルを読み込んだ時はlineポインタがもうNULLになったから

    fgets(line, 5, fp);
    if (line == NULL) {
      fprintf(stderr, "グラフのファイルが読み込んでません\n");
      exit(1);
    }

    sscanf(line, "%d %d", &(g->nvertices), &edge_count);

    while ((line = fgets(line, 5, fp)) != NULL) {
      sscanf(line, "%d %d", &x, &y);
      insert_edge(g, x, y, directed);
      tmp = line;
    }

    fclose(fp);
    free(tmp);
  }
}

//void bfs_test(graph* g) {
//	int bfs_vsum = bfs(g, 1);
//	int vsum = (g->nvertices * (g->nvertices + 1)) / 2;
//
//	printf("Sum： %d %d\n", bfs_vsum, vsum);
//
//	assert(bfs_vsum == vsum);
//}

void dfs_test(graph *g) {
  int graph_cycle = has_cycle(g);
  printf("Has cycle: %d\n", graph_cycle);
  assert(graph_cycle == 1);

  graph *non_cycle = create(0);

  read_graph_file("graph/noncycle_graph_data", non_cycle, non_cycle->directed);

  graph_cycle = has_cycle(non_cycle);
  printf("Has cycle: %d\n", graph_cycle);
  assert(graph_cycle == 0);
  free_graph(non_cycle);
}

int main() {
	printf("Graph test\n");

	graph* g = create(0);

	read_graph_file("graph/graph_data", g, g->directed);

	printf("Graph:\n");
	print(g);

	printf("Search start:\n");

	dfs_test(g);
	//bfs_test(g);

	free_graph(g);

    getchar();

	return 0;
}

