#include "graph.c"
#include <stdio.h>

void dfs_subroutine(graph* g, int s, int* discovered, int* processed, int* parent) {
	discovered[s] = 1;

	edge* p = g->edges[s];
	int y;

	while (p != NULL) {
		y = p->y;
		if (!discovered[y]) {
			parent[y] = s;
			dfs_subroutine(g, y, discovered, processed, parent);
		}
		if (g->directed || (!processed[y] && !parent[v] != y)) {
			// process back edge
			if (!g->directed) {
				// cycle
			}
		}

		p = p->next;
	}

	processed[s] = 1;
}

int dfs(graph* g) {
	int processed[MAXV + 1];
	int discovered[MAXV + 1];
	int parent[MAXV + 1];
	for (int i = 1; i <= MAXV; i++) {
		processed[i] = 0;
		discovered[i] = 0;
		parent[i] = 0;
	}


	for (int i = 1; i <= g->nvertices; i++) {
		if (!discovered[i]) {
			dfs_subroutine(g, i, discovered, processed, parent);
		}
	}
	return 0;
}

