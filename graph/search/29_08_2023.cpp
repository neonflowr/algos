#include "graph.cpp"
#include <vector>

using namespace std;

void dfs_subroutine(graph* g, int s, int *discovered, int *processed, int *parent) {
  discovered[s]= true;
  // preprocess

  for(int i = 1; i < g->nvertices; i++) {
    if (g->has_connection(s, i)) {
      if (!discovered[i]) {
        parent[i] = s;
        dfs_subroutine(g, i, discovered, processed, parent);
      }
      if (g->directed || ( !processed[i] && parent[s] != i) )  {
        // process

      }

    }


  }




  processed[s] = true;
}


void dfs(graph *g) {
  bool discovered[SIZE+1];
  bool processed[SIZE+1];
  int parent[SIZE+1];
  for(int i = 1; i <= SIZE; i++) {
    discovered[i] = false;
    processed[i] = false;
    parent[i] = -1;
  }

  for(int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      dfs_subroutine(g, i, discovered, processed, parent);
    }
  }
}
