#include "graph.cpp"

void dfs_subroutine(graph *g, int s, bool discovered[], bool processed[], int parent[]) {
  discovered[s] = true;

  for (int i = 1; i <= g->nvertices; i++) {
    if (g->has_connection(s, i)) {
      if (!discovered[i]) {
        parent[i] = s;
        dfs_subroutine(g, i, discovered, processed, parent);
      }

      if (g->directed || !processed[i] && parent[s] != i) {
        // cycle

      }
    }
  }

  processed[s] = true;
}

void dfs(graph *g) {
  bool discovered[g->nvertices+1];
  bool processed[g->nvertices+1];
  int parent[g->nvertices+1];
  for(int i = 1; i <= g->nvertices; i++) {
    discovered[i] = false;
    processed[i] = false;
    parent[i] = -1;
  }

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      dfs_subroutine(g, i, discovered, processed, parent);
    }
  }
}
