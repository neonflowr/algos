#include "init.h"
#include "graph.c"
#include <stdbool.h>
#include <stdio.h>

typedef struct List {
  int data;
  struct List *next;
} list;

typedef struct Queue {
  list *front;
  list *back;
  int size;
} queue;

queue *create_queue() {
  queue *q = malloc(sizeof(queue));
  q->front = q->back = NULL;
  q->size = 0;
  return q;
}

bool is_empty(queue *q) {
  return q->size == 0;
}

void enqueue(queue *q, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = NULL;

  if (q->front == NULL) {
	q->front = q->back = l;
  } else {
	q->back->next = l;
	q->back = l;
  }
  q->size++;
}

int dequeue(queue *q) {
  if (is_empty(q)) return -1;
  list *tmp = q->front;
  q->front = q->front->next;
  int val = tmp->data;
  free(tmp);
  q->size--;
  return val;
}



int bfs(graph *g, int s) {
  bool discovered[MAXV + 1];
  bool processed[MAXV+1];
  int parent[MAXV+1];
  for (int i = 1; i <= MAXV; i++) {
	discovered[i] = false;
	processed[i] = false;
	parent[i] = 0;
  }
  queue *frontier = create_queue();

  discovered[s] = true;
  enqueue(frontier, s);

  int v, y;
  edge *p;

  int sum = 0;

  while (!is_empty(frontier)) {
	v = dequeue(frontier);

	processed[v] = true;

	// process vertex
	sum += v;

	p = g->edges[v];
	while(p != NULL) {
	  y = p->y;
	  if (!discovered[y]) {
		parent[y] = v;
		discovered[y] = true;
		enqueue(frontier, y);
	  }
	  if (!processed[y] || g->directed) {
		// process edge
	  }
	  p = p->next;
	}
  }

  free(frontier);

  return sum;
}
