#include <iostream>

using namespace std;

class list {
public:
    int data = 0;
    list* next = nullptr;

    static void insert(list** head, int val) {
        list* node = new list();
        node->data = val;
        node->next = *head;
        *head = node;
    }

    list* search(int val) {
        list* l = this;
        while (l && l->data != val) {
            l = l->next;
        }
        return l;
    }

    void print() {
        list* l = this;
        while (l) {
            cout << l->data << " ";
            l = l->next;
        }
        cout << "\n";
    }
};

class __queue {
public:
    list* front = nullptr;
    list* back = nullptr;

    void enqueue(int val) {
        list* node = new list();
        node->data = val;
        node->next = nullptr;

        if (front == nullptr) {
            front = back = node;
        }
        else {
            back->next = node;
            back = node;
        }
    }

    int is_empty() {
        return front == nullptr;
    }

    int dequeue() {
        if (front == nullptr) return -1;
        list* l = front;
        front = front->next;
        int val = l->data;
        delete l;
        return val;
    }
};

class __stack {
public:
    list* top;

    void push(int val) {
        list* l = new l;
        l->data = val;
        l->next = nullptr;
        if (top == nullptr) {
            top = l;
        }
        else {
            l->next = top;
            top = l;
        }
    }

    int is_empty() {
        return top != nullptr;
    }

    int pop() {
        if (top == nullptr) return -1;
        list* l = top;
        top = top->next;
        int val = l->data;
        delete l;
        return val;
    }
};


#define MAXV 100

class graph {
public:
    list* edges[MAXV + 1];
    int nvertices;
    int nedges;
    int degree[MAXV + 1];
    int directed;

    graph(int n, int directed) {
        for (int i = 1; i <= MAXV; i++) {
            edges[i] = NULL;
            degree[i] = 0;
        }
        nvertices = n;
        nedges = 0;
        this->directed = directed;
    }

    int has_connection(int x, int y) {
        return edges[x]->search(y) != nullptr;
    }

    void insert_edge(int x, int y, int directed) {
        if (this->has_connection(x, y)) {
            return;
        }

        list::insert(&edges[x], y);
        degree[x]++;

        if (!directed) {
            insert_edge(y, x, 0);
        }
        else {
            nedges++;
        }
    }

    void print() {
        list* l;
        for (int i = 1; i <= nvertices; i++) {
            l = edges[i];
            cout << i << ": ";
            l->print();
            cout << "\n";
        }
    }

    int dfs_subroutine(int s, int* discovered, int* processed, int* parent) {
        discovered[s] = 1;

        int y;

        // process vertex
        cout << s << " ";
        
        list* e = edges[s];

        while (e) {
            y = e->data;

            if (!discovered[y]) {
                parent[y] = s;
                dfs_subroutine(y, discovered, processed, parent);
            }
            if (directed || !processed[y] && parent[s] != y) {
                // process edge
            }

            e = e->next;
        }

        processed[s] = 1;

        return 1;
    }

    int dfs_stack(int s) {
        __stack* st = new __stack;
        st->push(s);
        int discovered[MAXV + 1];
        int processed[MAXV + 1];
        int ndfs = 0;
        processed[ndfs++] = s;
        discovered[s] = 1;
        int v = -1, u = s;
        while (!st->is_empty()) {
            if (v == nvertices) {
                u = st->pop();
            }

            for (v = 0; v < nvertices; v++) {
                if (has_connection(u, v) && !discovered[v]) {
                    st->push(u);
                    st->push(v);
                    processed[ndfs++] = v;
                    discovered[v] = 1;
                    u = v;
                    break;
                }
            }
        }
    }

    int dfs() {
        int discovered[MAXV + 1];
        int processed[MAXV + 1];
        int parent[MAXV + 1];
        for (int i = 1; i <= nvertices; i++) {
            discovered[i] = 0;
            processed[i] = 0;
            parent[i] = -1;
        }

        for (int i = 1; i <= nvertices; i++) {
            if (!discovered[i]) {
                dfs_subroutine(i, discovered, processed, parent);
                cout << "\n";
            }
        }
        return 1;
    }

    int bfs(int s, int x) {
        int discovered[MAXV + 1];
        int processed[MAXV + 1];
        int parent[MAXV + 1];
        for (int i = 1; i <= nvertices; i++) {
            discovered[i] = 0;
            processed[i] = 0;
            parent[i] = -1;
        }

        __queue* frontier = new __queue();

        discovered[s] = 1;
        frontier->enqueue(s);

        int v, y;
        list* e;

        int found = 0;

        while (!frontier->is_empty()) {
            v = frontier->dequeue();
            
            processed[v] = 1;

            // process vertex 
            if (v == x) found = 1;

            cout << v << " ";
            e = this->edges[v];
            while (e) {
                y = e->data;
                if (!discovered[y]) {
                    discovered[y] = 1;
                    parent[y] = v;
                    frontier->enqueue(y);
                }

                if (directed || !processed[y]) {
                    // process
                }
                e = e->next;
            }
        }
        cout << "\n";

        return found;
    }
};

int main(int argc, char** argv) {
    cout << "Nhap do thi\n";

    int nvertices;
    int x, y;
    string in;

    cout << "Nhap so dinh trong do thi: ";
    cin >> nvertices;
    cout << "\n";
    cout << "So dinh: " << nvertices << "\n";

    graph* g = new graph(nvertices, 0);

    cout << "Nhap cac canh trong do thi:\n";
    while (scanf_s("%d %d", &x, &y) != 0) {
        if (x > nvertices || y > nvertices) break;
        g->insert_edge(x, y, 0);
        cout << "Them canh " << x << " " << y << "\n";
    }

    cout << "Do thi:\n";
    g->print();

    cout << "BFS:\n";
    g->bfs(1, 0);

    cout << "DFS:\n";
    g->dfs();

    int find = 0;
    cout << "Nhap gia tri dinh can tim: ";
    cin >> find;
    cout << "\n";

    cout << "Ket qua tim: " << g->bfs(1, find) << "\n";

    return 0;
}
