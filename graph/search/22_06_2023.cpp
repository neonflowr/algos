#include "graph.cpp"

class list {
public:
	int data;
	list* next;

	list(int data, list* next) {
		this->data = data;
		this->next = next;
	}

	void free() {
		list* p = this;
		list* tmp;
		while (p) {
			tmp = p->next;
			delete p;
			p = tmp;
		}
	}
};

class queue {
public:
	list* front;
	list* back;

	queue() {
		this->front = NULL;
		this->back = NULL;
	}

	bool enqueue(int x) {
		list* node = new list(x, NULL);
		if (this->front == NULL && this->back == NULL) {
			this->front = this->back = node;
			return true;
		}
		this->back->next = node;
		this->back = node;
		return true;
	}

	int dequeue() {
		if (this->front == NULL) return 0;
		list* p = this->front;
		this->front = this->front->next;
		int val = p->data;
		delete p;
		return val;
	}

	bool is_empty() {
		return this->front == NULL;
	}

	void free() {
		this->front->free();
		this->back->free();
	}
};

void bfs(graph *g, int start) {
	bool discovered[g->capacity] = { false };
	bool processed[g->capacity] = { false };
	int parent[g->capacity] = { -1 };
	queue* frontier = new queue();

	int v;
	int edge;
	int y;


	discovered[start] = true;
	frontier->enqueue(start);


	while (!frontier->is_empty()) {
		v = frontier->dequeue();

		processed[v] = true;

		// process vertex

		for (int i = 1; i <= g->nvertices; i++) {
			if (g->has_connection(v, i)) {
				if (!discovered[i]) {
					discovered[i] = true;
					parent[i] = v;
					frontier->enqueue(i);
				}

				if (!processed[i] || g->directed) {
					// cycle
					// process edge
				}
			}
		}
	}

	frontier->free();
	delete frontier;
	frontier = NULL;
}

void same_component(graph *g, int x, int y) {
	bool discovered[g->capacity] = { false };
	bool processed[g->capacity] = { false };
	int parent[g->capacity] = { -1 };
	queue* frontier = new queue();

	int v;
	int edge;
	int y;


	int start = x;
	discovered[start] = true;
	frontier->enqueue(start);

	bool found_y = false;

	while (!frontier->is_empty()) {
		v = frontier->dequeue();

		processed[v] = true;

		// process vertex

		for (int i = 1; i <= g->nvertices; i++) {
			if (g->has_connection(v, i)) {
				if (i == y) {
					found_y = true;
					break;
				}

				if (!discovered[i]) {
					discovered[i] = true;
					parent[i] = v;
					frontier->enqueue(i);
				}

				if (!processed[i] || g->directed) {
					// cycle
					// process edge
				}
			}
		}
	}

	frontier->free();
	delete frontier;
	frontier = NULL;

	return found_y;
}

