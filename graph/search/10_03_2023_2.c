#include "graph.c"
#include "init.h"
#include <stdio.h>

void dfs_subroutine(graph* g, int v, int discovered[], int processed[], int parent[], int *has_cycle) {
	int y;
	edge* p;

	discovered[v] = 1;

	p = g->edges[v];

	while (p != NULL) {
		y = p->y;

		if (!discovered[y]) {
			parent[y] = v;
			dfs_subroutine(g, y, discovered, processed, parent, has_cycle);
		}
		else if (g->directed) {
			;
		}
		else if (!processed[y] && parent[v] != y) {
			*has_cycle = 1;
		}

		p = p->next;
	}

	processed[v] = 1;
}


int dfs(graph *g) {
	int discovered[MAXV + 1];
	int processed[MAXV + 1];
	int parent[MAXV + 1];
	for (int i = 1; i <= MAXV; i++) {
		discovered[i] = 0;
		processed[i] = 0;
		parent[i] = 0;
	}

	int has_cycle = 0;

	for (int i = 1; i <= g->nvertices; i++) {
		if (!discovered[i]) {
			dfs_subroutine(g, i, discovered, processed, parent, &has_cycle);
		}
	}

	return has_cycle;
}

int has_cycle(graph* g) {
	return dfs(g);
}

