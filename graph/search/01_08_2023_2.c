#include "graph.c"

typedef struct __list {
    int data;
    struct __list *next;
} list;

typedef struct __queue {
    list *front;
    list *back;
} queue;

queue *create() {
    queue *q = malloc(sizeof(queue));
    q->front = q->back = NULL;
    return q;
}

int is_empty(queue *q) {
    return q->front == NULL;
}

void enqueue(queue *q, int val) {
    list *node = malloc(sizeof(list));
    node->data = val;
    node->next = NULL;
    if (q->front == NULL) { 
        q->front = q->back = node;
    } else {
        q->back->next = node;
        q->back = node;
    }
}

int dequeue(queue *q) {
    if (!q->front) return -1;
    list *tmp = q->front;
    q->front = q->front->next;
    int val = tmp->data;
    free(tmp);
    return val;
}

void bfs(graph *g, int s) {
    int discovered[100];
    int processed[100];
    int parent[100];
    for( int i = 1; i <= MAXV; i++) {
        discovered[i] = 0;
        processed[i] = 0;
        parent[i] = -1;
    }

    queue *frontier = create();

    discovered[s] = 1;
    enqueue(frontier, s);

    int v, y;
    edge *p;

    while(!is_empty(frontier)) {
        v = dequeue(frontier);
        p = g->edges[v];

        processed[v] = 1;

        while(p) {
            y = p->y;
            if (!discovered[y]) {
                discovered[y] = 1;
                parent[y] = v;
                enqueue(frontier, y);
            }

            if (!processed[y]) {
                // process edge
            }
            p = p->next;
        }
    }
}

