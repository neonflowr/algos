#include <stdlib.h>
#include "graph.c"

void dfs_subroutine(graph *g, int s, int *discovered, int *processed, int *parent) {
    discovered[s] = 1;

    edge *p = g->edges[s];

    int y; 

    while(p) {
        y = p->y;
        if (!discovered[y]) {
            parent[y] = s;
            dfs_subroutine(g, y, discovered, processed, parent);
        }
        if (!processed[y] && parent[s] != y) {
            // cycle
        }
        p = p->next;
    }

    processed[s] = 1;
}

void dfs(graph *g) {
    int discovered[MAXV+1];
    int processed[MAXV+1];
    int parent[MAXV+1];
    for (int i = 1; i <= MAXV; i++) {
        discovered[i] = 0;
        processed[i] = 0;
        parent[i] = -1;
    }

    for (int i = 1; i <= MAXV; i++) {
        if (!discovered[i]) {
            dfs_subroutine(g, i, discovered, processed, parent);
        }
    }
}
