#define SIZE 100

class graph {
public:
	int degree[SIZE+1][SIZE+1] = { 0 };
	int edges[SIZE+1][SIZE+1] = { 0 };
	int nedges;
	int nvertices;
	bool directed;
	int capacity = SIZE;

	graph(bool directed) {
		nedges = 0;
		nvertices = 0;
		this->directed = directed;
	}

	bool has_connection(int x, int y) {
		return this->edges[x][y];
	}

	bool insert_edge(int x, int y, bool directed) {
		this->edges[x][y] = 1;
		if (!directed) {
			return insert_edge(y, x, true);
		}
		else {
			this->nedges++;
		}
		return true;
	}
};
