#define SIZE 100

class graph {
public:
	int degree[SIZE][SIZE] = { 0 };
	int edges[SIZE][SIZE] = { 0 };
	int nedges;
	int nvertices;
	bool directed;

	graph(bool directed) {
		nedges = 0;
		nvertices = SIZE;
		this->directed = true;
	}

	bool has_connection(int x, int y) {
		return edges[x][y];
	}

	bool insert_edge(int x, int y, bool directed) {
		edges[x][y] = 1;
		if (!directed) {
			return insert_edge(y, x, true);
		}
		else {
			nedges++;
		}
		return true;
	}
};

