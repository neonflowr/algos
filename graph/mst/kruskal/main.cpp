#include "29_09_2023.cpp"
#include <iostream>
#include <assert.h>

int main() {
	std::cout << "Testing Kruskal's algos\n";

	 //graph* g = new graph(4, false);
	 //g->insert_edge(1, 2, 1, false);
	 //g->insert_edge(2, 3, 2, false);
	 //g->insert_edge(3, 4, 5, false);
	 //g->insert_edge(1, 4, 4, false);
	 //g->insert_edge(1, 3, 3, false);

	graph* g = new graph(5, false);
	g->insert_edge(1, 2, 1, false);
	g->insert_edge(1, 3, 7, false);
	g->insert_edge(2, 3, 5, false);
	g->insert_edge(2, 4, 4, false);
	g->insert_edge(2, 5, 3, false);
	g->insert_edge(3, 5, 6, false);
	g->insert_edge(5, 4, 2, false);

	g->print();

	int weight;

	weight = kruskal_mst(g);

	std::cout << "Total weight: " << weight << "\n";

	assert(weight == 11);

	return 0;
}

