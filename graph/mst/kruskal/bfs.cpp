#include "../graph.cpp"
#include <stdio.h>
#include <iostream>

class list {
public:
  int data;
  list *next;

  list(int data, list *next) {
    this->data = data;
    this->next = next;
  }

  void print() {
    list *p = this;
    cout << "List: ";
    while(p) {
      std::cout << p->data << " ";
      p = p->next;
    }
  }

  void free() {
    list *p = this;
    list *tmp;
    while (p) {
      tmp = p->next;
      delete p;
      p = tmp;
    }
  }
};

class queue {
public:
  list *front = nullptr;
  list *back = nullptr;

  queue() {
    this->front = nullptr;
    this->back = nullptr;
  }

  bool enqueue(int x) {
    list *node = new list(x, nullptr);
    if (this->front == nullptr) {
      this->front = node;
      this->back = node;

      return true;
    }
    this->back->next = node;
    this->back = node;

    return true;
  }

  int dequeue() {
    if (this->front == nullptr)
      return 0;
    list *p = this->front;
    this->front = this->front->next;
    int val = p->data;
    delete p;
    return val;
  }

  bool is_empty() { return this->front == nullptr; }

  void free() {
    this->front->free();
    this->back->free();
  }

  void print() {
    std::cout << "Queue: ";
    list *p = this->front;
    printf("P %p\n", p);
    while(p) {
      std::cout << p->data << " ";
      p = p->next;
    }
    std::cout << "\n";
  }
};

bool same_component(graph *g, int x, int y) {
  std::cout << "Find component for " << x << " " << y << "\n";

  bool discovered[g->nvertices + 1];
  bool processed[g->nvertices + 1];
  int parent[g->nvertices + 1];

  for (int i = 1; i <= g->nvertices; i++) {
    discovered[i] = false;
    processed[i] = false;
    parent[i] = -1;
  }

  int v;
  int edge;

  queue *frontier = new queue();

  int start = x;
  discovered[start] = true;
  frontier->enqueue(start);

  bool found_y = false;

  while (!frontier->is_empty()) {
    v = frontier->dequeue();

    processed[v] = true;

    // process vertex
    std::cout << "Check vertex " << v << "\n";

    for (int i = 1; i <= g->nvertices; i++) {
      if (g->has_connection(v, i)) {
        std::cout << "Checking connection " << v << " " << i << "\n";

        if (i == y) {
          std::cout << "Found y in same component\n";
          found_y = true;

          delete frontier;
          frontier = nullptr;

          return found_y;
        }

        if (!discovered[i]) {
          discovered[i] = true;
          parent[i] = v;
          frontier->enqueue(i);
        }

        if (!processed[i] || g->directed) {
          // cycle
          // process edge
        }
      }
    }
  }

  delete frontier;
  frontier = nullptr;

  return found_y;
}
