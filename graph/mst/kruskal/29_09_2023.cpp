#include "../graph.cpp"
#include <stdio.h>

#define SET_SIZE 100

class uf {
private:
	int p[SET_SIZE + 1];
	int size[SET_SIZE + 1];
	int n;
public:
	uf(int n) {
		for (int i = 1; i <= n; i++) {
			this->p[i] = i;
			this->size[i] = 1;
		}
		this->n = n;
	}

	int find(int x) {
		if (this->p[x] == x) return x;
		return this->find(this->p[x]);
	}

	bool same_component(int s1, int s2) {
		return this->find(s1) == this->find(s2);
	}

	void union_set(int s1, int s2) {
		int r1, r2;
		r1 = this->find(s1);
		r2 = this->find(s2);
		if (r1 == r2) return;

		if (this->size[r1] >= this->size[r2]) {
			this->size[r1] = this->size[r1] + this->size[r2];
			this->p[r2] = r1;
		}
		else {
			this->size[r2] = this->size[r1] + this->size[r2];
			this->p[r1] = r1;
		}
	}
};


void print(int *items, int n) {
	for (int i = 0; i < n; i++) {
		cout << items[i] << " ";
	}
	cout << "\n";
}

class pq {
private:
	int items_x[SET_SIZE + 1];
	int items_y[SET_SIZE + 1];
	int weight[SET_SIZE + 1];
	int capacity;
	int size;

public:
	pq(int n) {
		for (int i = 0; i <= n; i++) {
			this->items_x[i] = 0;
			this->items_y[i] = 0;
			this->weight[i] = 0;
		}
		this->capacity = n;
		this->size = 0;
	}

	int parent(int i) {
		return i / 2;
	}

	bool is_valid(int i) {
		if (i == 0) return true;
		return this->weight[parent(i)] < this->weight[i];
	}

	bool is_full() {
		return this->size == this->capacity;
	}

	void swap(int* x, int* y) {
		int tmp = *x;
		*x = *y;
		*y = tmp;
	}

	bool is_empty() {
		return this->size == 0;
	}

	void bubble_down(int i) {
		int min = i;
		int child = i * 2 + 1;
		for (int j = 0; j <= 1; j++) {
			child += j;
			if (child < this->size && this->weight[child] < this->weight[min]) {
				min = child;
			}
		}
		if (min == i) return;
		swap(&this->items_x[i], &this->items_x[min]);
		swap(&this->items_y[i], &this->items_y[min]);
		swap(&this->weight[i], &this->weight[min]);
		bubble_down(min);
	}

	bool pop(int* x, int* y, int* weight) {
		if (this->is_empty()) return false;
		int xval = this->items_x[0];
		int yval = this->items_y[0];
		int wval = this->weight[0];
		this->size--;
		this->swap(&this->items_x[0], &this->items_x[this->size]);
		this->swap(&this->items_y[0], &this->items_y[this->size]);
		this->swap(&this->weight[0], &this->weight[this->size]);
		this->bubble_down(0);

		*x = xval;
		*y = yval;
		*weight = wval;

		printf("after pop %d %d %d\n", xval, yval, wval);
		print(this->weight, this->size);

		return true;
	}

	void bubble_up(int i) {
		if (this->is_valid(i)) return;
		int p = this->parent(i);
		this->swap(&this->items_x[i], &this->items_x[p]);
		this->swap(&this->items_y[i], &this->items_y[p]);
		this->swap(&this->weight[i], &this->weight[p]);
		bubble_up(p);
	}

	void insert(int x, int y, int weight) {
		if (this->is_full()) return;
		this->items_x[this->size] = x;
		this->items_y[this->size] = y;
		this->weight[this->size] = weight;
		this->bubble_up(this->size);
		this->size++;
		printf("after insret %d %d %d\n", x, y, weight);
		print(this->weight, this->size);
	}
};

int kruskal_mst(graph* g) {
	pq* p = new pq(g->nvertices * g->nvertices);
	for (int i = 1; i <= g->nvertices; i++) {
		for (int j = i + 1; j <= g->nvertices; j++) {
			if (g->has_connection(i, j)) {
				printf("Graph insert edge %d %d with weight %d\n", i, j, g->edges[i][j]);
				p->insert(i, j, g->edges[i][j]);
			}
		}
	}

	int x, y, w;

	uf* union_find = new uf(g->nvertices);
	int total = 0;

	while (!p->is_empty()) {
		p->pop(&x, &y, &w);
		if (!union_find->same_component(x, y)) {
			printf("Union for %d %d with weight %d\n", x, y, w);
			union_find->union_set(x, y);
			total += w;
		}
	}

	delete p;
	delete union_find;

	return total;
}

