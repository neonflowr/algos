#include <iostream>

#define SIZE 100

using namespace std;

class pq_edge {
public:
  int x = 0;
  int y = 0;
  int weight = 0;

  pq_edge(int x, int y, int weight) {
    this->x = x;
    this->y = y;
    this->weight = weight;
  }
};

class pq {
public:
  pq_edge *items[SIZE] = {nullptr};
  int size = 0;
  int capacity = SIZE;

  bool is_full() { return this->size == this->capacity; }

  bool is_empty() { return this->size == 0; }

  void insert(int x, int y, int weight) {
    if (this->is_full())
      return;
    this->items[this->size] = new pq_edge(x, y, weight);
    bubble_up(this->size++);
  }

  pq_edge *pop() {
    if (this->is_empty())
      return 0;
    pq_edge *val = this->items[0];
    this->swap(&this->items[0], &this->items[--this->size]);
    this->bubble_down(0);
    cout << "Popping " << val->weight << "\n";
    return val;
  }

  pq_edge *min() {
    if (is_empty())
      return 0;
    return this->items[0];
  }

  void print() {
    cout << "PQ: ";
    for (int i = 0; i < size; i++) {
      cout << this->items[i]->weight << " ";
    }
    cout << "\n";
  }

private:
  void swap(pq_edge **x, pq_edge **y) {
    pq_edge* tmp = *x;
    *x = *y;
    *y = tmp;
  }

  int parent(int x) { return x / 2; }

  bool is_valid(int i) {
    if (i == 0)
      return true;
    return this->items[this->parent(i)]->weight < this->items[i]->weight;
  }

  void bubble_up(int i) {
    if (this->is_valid(i))
      return;
    int p = this->parent(i);
    swap(&this->items[i], &this->items[p]);
    this->bubble_up(p);
  }

  void bubble_down(int index) {
    int child = index * 2 + 1;
    int min = index;
    for (int i = 0; i < 2; i++) {
      child = child + i;
      if (child < this->size &&
          this->items[child]->weight < this->items[min]->weight) {
        min = child;
      }
    }

    if (min == index)
      return;

    swap(&this->items[min], &this->items[index]);
    bubble_down(min);
  }
};
