#include "pq.cpp"
#include "bfs.cpp"

void kruskal(graph* g, int s) {
    // Sort all the edges in a priority queue
    pq *p = new pq();
    for(int i = 1; i <= g->nvertices; i++) {
        for (int j = 1; j <= g->nvertices; j++) {
            if (g->has_connection(i, j)) {
                p->insert(i, j, g->edges[i][j]);
            }
        }
    }

    graph *t = new graph();
    t->nvertices = g->nvertices;

    int count = 1;
    int weight = 0;
    pq_edge *e;

    while(count < g->nvertices) {
        e = p->pop();
        if (!same_component(t, e->x, e->y)) {
            t->insert_edge(e->x, e->y, e->weight, false);
            weight += e->weight;
        }

        count++;
    }
}