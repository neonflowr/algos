#include "bfs.cpp"
#include "pq.cpp"
#include <iostream>

int kruskal_mst(graph *g) {
  // sort all edges with a pq
  pq *p = new pq();
  for (int i = 1; i <= g->nvertices; i++) {
    for (int j = i + 1; j <= g->nvertices; j++) {
      if (g->has_connection(i, j)) {
        p->insert(i, j, g->edges[i][j]);
      }
    }
  }

  p->print();

  int count = 0;
  pq_edge *next;

  graph *t = new graph(g->nvertices, false);
  t->nvertices = g->nvertices;

  t->print();

  int weight = 0;

  while (count < g->nvertices) {
    next = p->pop();

    std::cout << "Next: " << next->weight << " " << next->x << " " << next->y << "\n";

    std::cout << "MST:\n";
    t->print();

    if (!same_component(t, next->x, next->y)) {
      // connect edge
      // add to tree
      // merge component
      cout << "Insert edge " << next->x << " " << next->y << "\n";
      t->insert_edge(next->x, next->y, next->weight, false);
      weight += next->weight;
    }

    count++;
  }

  std::cout << "Final MST:\n";
  t->print();

  delete p;
  delete t;

  return weight;
}
