#include <iostream> 
#include <limits>
#include <stdio.h>

#define MAXV 100

using namespace std;

class graph {
public:
    int edges[MAXV + 1][MAXV + 1];
    int nvertices;
    int nedges;
    bool directed;
    graph(int n, bool d) {
        for (int i = 1; i <= MAXV; i++) {
            for (int j = 1; j <= MAXV; j++) {
                edges[i][j] = 0;
            }
        }
        nedges = 0;

        nvertices = n;
        directed = d;
    }

    int has_connection(int x, int y) {
        return edges[x][y];
    }

    void insert_edge(int x, int y, bool directed, int weight) {
        if (has_connection(x, y)) return;

        edges[x][y] = weight;

        if (!directed) {
            insert_edge(y, x, false, weight);
        }
        else {
            nedges++;
        }
    }

    int E1[MAXV + 1];
    int E2[MAXV + 1];
    int wE[MAXV + 1];
    int nE = 0;

    int T1[MAXV + 1];
    int T2[MAXV + 1];
    int wT[MAXV + 1];
    int nT = 0;

    void initE() {
        for (int i = 1; i <= MAXV; i++) {
            for (int j = 1; j <= MAXV; j++) {
                if (has_connection(i, j)) {
                    E1[nE] = i;
                    E2[nE] = j;
                    wE[nE] = edges[i][j];
                    nE++;
                }
            }
        }
    }

    void swap(int* x, int* y) {
        int tmp = *x;
        *x = *y;
        *y = tmp;
    }

    void sortE() {
        for (int i = 1; i < nE - 1; i++) {
            for (int j = i; j > 0; j--) {
                if (wE[j] < wE[j - 1]) {
                    swap(&wE[j], &wE[j - 1]);
                    swap(&E1[j], &E1[j - 1]);
                    swap(&E2[j], &E2[j - 1]);
                }
            }
        }
    }

    bool exists(int ei, int t1[], int nt) {
        for (int i = 0; i < nt; i++) {
            if (t1[i] == ei) return true;
        }
        return false;
    }

    int kruskal() {
        initE();
        sortE();

        int total = 0;

        for (int i = 0; i < nE; i++) {
            if (exists(E1[i], T1, nT) && exists(E2[i], T2, nT)) {
                continue;
            }

            if (exists(E1[i], T2, nT) && exists(E2[i], T1, nT)) {
                continue;
            }

            T1[nT] = E1[i];
            T2[nT] = E2[i];
            wT[nT] = wE[i];
            total += wE[i];

            printf("Canh %d %d %d\n", T1[nT], T2[nT], wT[nT]);

            nT++;
            if (nT == nvertices - 1) break;
        }

        return total;
    }

    int prims(int start) {
        int distance[MAXV + 1];
        int parent[MAXV + 1];
        bool intree[MAXV + 1];
        for (int i = 1; i <= MAXV; i++) {
            distance[i] = INT_MAX;
            parent[i] = -1;
            intree[i] = false;
        }

        int dist;

        int v, total;
        v = start;
        distance[v] = 0;
        total = 0;

        while (!intree[v]) {
            cout << v << " in tree\n";
            intree[v] = true;

            if (v != start) {
                total += dist;
            }

            for (int i = 1; i <= MAXV; i++) {
                if (!intree[i] && has_connection(v, i) && distance[i] > edges[v][i]) {
                    distance[i] = edges[v][i];
                    parent[i] = v;
                }
            }

            dist = INT_MAX;

            for (int i = 1; i <= MAXV; i++) {
                if (!intree[i] && dist > distance[i]) {
                    dist = distance[i];
                    v = i;
                }
            }
        }

        return total;
    }
};

int main() {
    graph* g = new graph(9, false);
    g->insert_edge(1, 2, false, 4);
    g->insert_edge(1, 8, false, 8);

    g->insert_edge(2, 8, false, 11);
    g->insert_edge(2, 3, false, 8);
    g->insert_edge(3, 4, false, 7);
    g->insert_edge(3, 6, false, 4);
    //g->insert_edge(3, 7, false, 3);
    g->insert_edge(3, 9, false, 2);

    g->insert_edge(4, 5, false, 9);
    g->insert_edge(4, 6, false, 14);
    g->insert_edge(5, 6, false, 10);
    g->insert_edge(6, 7, false, 2);
    g->insert_edge(7, 8, false, 1);
    g->insert_edge(8, 9, false, 7);
    g->insert_edge(7, 9, false, 6);

    int mst = g->prims(1);
    cout << "MST Prim: " << mst << "\n";


    mst = g->kruskal();
    cout << "MST Kruskal: " << mst << "\n";

    delete g;

    return 0;
}
