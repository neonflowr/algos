#include "23_06_2023.cpp"
#include <iostream>

int main() {
	std::cout << "Testing Dijkstra's algorithm\n";

	graph* g = new graph(5, false);

	g->insert_edge(1, 2, 1, false);
	g->insert_edge(1, 3, 7, false);
	g->insert_edge(2, 3, 5, false);
	g->insert_edge(2, 4, 14, false);
	g->insert_edge(2, 5, 3, false);
	g->insert_edge(3, 5, 6, false);
	g->insert_edge(5, 4, 2, false);

	g->print();

	int weight;

	weight = dijkstra(g, 1, 4);

	std::cout << "Total weight: " << weight << "\n";

	return 0;
}
