#include "../graph.cpp"
#include <limits.h>
#include <stdio.h>

int dijkstra(graph *g, int start, int end) {
  bool intree[g->nvertices+1];
  int distance[g->nvertices+1];
  int parent[g->nvertices+1];

  for (int i = 1; i <= g->nvertices; i++) {
    intree[i] = false;
    distance[i] = INT_MAX;
    parent[i] = -1;
  }

  int v;
  int dist;
  int weight = 0;

  v = start;

  distance[v] = 0;

  while(!intree[v]) {
    intree[v] = true;

    if (v != start) {
      printf("Insert edge %d %d\n", v, parent[v]);
      weight += dist;
    }

    for (int i = 1; i <= g->nvertices; i++) {
      if (g->has_connection(v, i)) {
        if (distance[i] > (distance[v] + g->edges[v][i])) {
          distance[i] = distance[v] + g->edges[v][i];
          parent[i] = v;
        }
      }
    }

    dist = INT_MAX;

    for (int i = 1; i <= g->nvertices; i++) {
      if (!intree[i] && dist > distance[i]) {
        dist = distance[i];
        v = i;
      }
    }
  }

  int i = end;
  printf("Shortest path from %d to %d is:\n", start, end);
  while(parent[i] != -1) {
    printf("%d <-", i);
    i = parent[i];
  }
  printf("%d\n", start);

  return weight;
}
