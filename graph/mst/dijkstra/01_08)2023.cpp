#include "../graph.c"
#include <limits.h>

void dijkstra(graph *g, int s) {
    int distance[g->nvertices+1];
    bool intree[g->nvertices+1];
    int parent[g->nvertices+1];
    for(int i = 1; i <= g->nvertices; i++) {
        distance[i] = INT_MAX;
        intree[i] = false;
        parent[i] = -1;
    }
    int v, w, weight, dist;
    weight = 0;

    v = s;
    distance[v] = 0;

    while(!intree[v]) {
        intree[v] = true;
        if (v != s) {
            weight += dist;
        }

        for(int i = 1; i <= g->nvertices; i++) {
            if (g->has_connection(v, i)) {
                w = i;
                if (distance[w] > distance[v] + g->edges[v][w]) { // no need for intree
                    distance[w] = distance[v] + g->edges[v][w];
                    parent[w] = v;
                }
            }
        }

        dist = INT_MAX;

        for(int i = 1; i <= g->nvertices; i++) {
            if (!intree[i] && dist > distance[i]) {
                dist = distance[i];
                v =i;
            }
        }
    }
}