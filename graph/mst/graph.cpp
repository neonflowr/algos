#include <iostream>
#define SIZE 100

using namespace std;

class graph {
public:
	int degree[SIZE][SIZE] = { 0 };
	int edges[SIZE][SIZE] = { 0 };
	int nedges = 0;
	int nvertices = 0;
	bool directed = false;

	graph(int size, bool directed) {
		this->nvertices = size;
		this->directed = directed;
	}

	bool has_connection(int x, int y) {
		return edges[x][y];
	}

	bool insert_edge(int x, int y, int weight, bool directed) {
		edges[x][y] = weight;
		if (!directed) {
			return insert_edge(y, x, weight, true);
		}
		else {
			nedges++;
		}
		return true;
	}

	void print() {
		cout << "Graph:\n";
		for (int i = 1; i <= this->nvertices; i++) {
			cout << i << ": ";
			for (int j = 1; j <= this->nvertices; j++) {
				if (this->has_connection(i, j)) {
					cout << j << "(" << this->edges[i][j] << ")" << " ";
				}
			}
			cout << "\n";
		}
	}
};
