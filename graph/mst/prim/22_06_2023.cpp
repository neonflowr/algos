#include "../graph.cpp"
#include <limits.h>
#include <iostream>

int prims_mst(graph* g, int start) {
	bool intree[g->nvertices + 1];
	int distance[g->nvertices+1];
	int parent[g->nvertices + 1];

	for (int i = 1; i <= g->nvertices; i++) {
		intree[i] = false;
		distance[i] = INT_MAX;
		parent[i] = -1;
	}

	int w, v, y, i, dist, weight;
	weight = 0;

	v = start;
	distance[v] = 0;

	while (!intree[v]) {
		if (v != start) {
			// insert edge between v and parent[v] here to create the spanning tree
			std::cout << "Minimum edge: " << v << " " << parent[v] << "\n";
			weight = weight + dist;
		}

		intree[v] = true;

		for (int j = 1; j <= g->nvertices; j++) {
			if (g->has_connection(v, j)) {
				w = j;
				if (!intree[w] && distance[w] > g->edges[v][j]) {
					distance[w] = g->edges[v][j];
					parent[w] = v;
				}
			}
		}

		dist = INT_MAX;

		for (i = 1; i <= g->nvertices; i++) {
			if (!intree[i] && dist > distance[i]) {
				dist = distance[i];
				v = i;
			}
		}
	}

	return weight;
}

