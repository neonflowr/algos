#include "../graph.cpp"
#include<limits>

#define SIZE 100

class edge {
	int y;
	int weight;
	edge* next;
};

class graph {
	edge* edges[SIZE];
	int degree[SIZE];
	int nvertices;
	int nedges;
	int directed;
	int size = SIZE;
};

int prims_mst(graph* g, int start) {
	bool intree[g->size] = { false };
	int distance[g->size] = { INT_MAX };
	int parent[g->size] = { -1 };

	edge* p;

	int w, v, y, i, dist, weight;
	weight = 0;

	v = start;
	distance[v] = 0;

	while (!intree[v]) {
		if (v != start) {
			// insert edge between v and parent[v] here to create the spanning tree
			weight = weight + dist;
		}

		intree[v] = true;

		p = g->edges[v];

		while (p != NULL) {
			w = p->y;

			if (!intree[w] && distance[w] > p->weight) {
				distance[w] = p->weight;
				parent[w] = v;
			}

			p = p->next;
		}

		dist = INT_MAX;

		for (i = 0; i < g->nvertices; i++) {
			if (!intree[i] && dist > distance[i]) {
				dist = distance[i];
				v = i;
			}
		}
	}

	return weight;
}

