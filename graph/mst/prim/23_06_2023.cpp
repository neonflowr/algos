#include "../graph.cpp"
#include <limits.h>

int prims_mst(graph *g, int start) {
  bool intree[g->nvertices+1];
  int parent[g->nvertices+1];
  int distance[g->nvertices+1];
  for (int i = 1; i <= g->nvertices; i++) {
    intree[i] = false;
    parent[i] = -1;
    distance[i] = INT_MAX;
  }

  int i, w, v, dist;
  int weight = 0;

  v = start;
  distance[v] = 0;
  while(!intree[v]) {
    intree[v] = true;
    if(v != start) {
      weight += dist;
    }

    for (i = 1; i <= g->nvertices; i++) {
      if (g->has_connection(v, i)) {
        if (!intree[i] && distance[i] > g->edges[v][i]) {
          distance[i] = g->edges[v][i];
          parent[i] = v;
        }
      }
    }

    dist = INT_MAX;
    for (i = 1; i <= g->nvertices; i++) {
      if (!intree[i] && dist > distance[i]) {
        dist = distance[i];
        v = i;
      }
    }
  }

  return weight;
}
