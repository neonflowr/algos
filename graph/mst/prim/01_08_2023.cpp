#include "../graph.c"
#include <limits.h>

int prims(graph *g, int s) {
    bool intree[g->nvertices+1];
    int distance[g->nvertices+1];
    int parent[g->nvertices+1];
    for(int i = 1; i <= g->nvertices; i++) {
        intree[i] = false;
        distance[i] = INT_MAX;
        parent[i] = -1;
    }

    int v, w, weight, dist;
    weight = 0;

    v = s;
    distance[v] = 0;

    while(!intree[v]) {
        if (v != s) {
            // build tree;
            weight = weight + dist;
        }

        intree[v] = true;

        // Track closest vertices to the tree
        for(int i = 1; i <= g->nvertices; i++) {
            if (g->has_connection(v, i)) {
                w = i;
                if (!intree[w] && distance[w] > g->edges[v][w]) {
                    distance[w] = g->edges[v][w];
                    parent[w] = v;
                }
            }
        }

        dist = INT_MAX;

        // Find closest vertex to tree
        for(int i = 1; i <= g->nvertices; i++) {
            if (!intree[i] && dist > distance[i]) {
                dist = distance[i];
                v = i;
            }
        }
    }





}