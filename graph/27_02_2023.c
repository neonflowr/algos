#include <stdio.h>
#include <stdlib.h>
#include "init.h"

#define MAXV 100

typedef struct Graph {
	int edges[MAXV + 1][MAXV + 1];
	int degree[MAXV + 1];
	int nvertices;
	int nedges;
	int directed;
	int capacity;
} graph;

graph* create(int directed) {
	graph* g = malloc(sizeof(graph));
	g->capacity = MAXV;
	for (int i = 1; i <= g->capacity; i++) {
		g->degree[i] = 0;
		for (int j = 1; j <= g->capacity; j++) {
			g->edges[i][j] = 0;
		}
	}

	g->nvertices = g->nedges = 0;
	g->directed = directed;
	return g;
}

void free_graph(graph* g) {
	free(g);
}

void print(graph* g) {
	printf("   ");
	for (int i = 1; i <= g->nvertices; i++) {
		printf("%d ", i);
	}
	printf("\n");
	for (int j = 1; j <= g->nvertices; j++) {
		printf("%d: ", j);
		for (int i = 1; i <= g->nvertices; i++) {
			printf("%d ", g->edges[i][j]);
		}
		printf("\n");
	}
}

int has_connection(graph* g, int x, int y) {
	return g->edges[x][y];
}

void insert_edge(graph* g, int x, int y, int directed) {
	g->edges[x][y] = 1;
	if (!directed) {
		insert_edge(g, y, x, 1);
	}
	else {
		g->nedges++;
	}
}
