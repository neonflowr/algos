#include <stdlib.h>

#define MAXV 100

typedef struct __list {
    int data;
    int weight;
    struct __list *next;
} list;

list *search(list *l, int val) {
    if (!l) return NULL;
    if (l->data == val) return l;
    return search(l->next, val);
}

void insert(list **head, int val) {
    list *node = malloc(sizeof(list));
    node->data  =val;
    node->next = *head;
    *head = node;
}

typedef struct __graph {
    list *edges[MAXV+1];
    int degree[MAXV+1];
    int nvertices;
    int nedges;
    int directed;
} graph;

int has_connection(graph *g, int x, int y) {
    return search(g->edges[x], y) != NULL;
}

void insert_edge(graph *g, int x, int y, int directed) {
    if (has_connection(g, x, y)) return;

    insert(&g->edges[x], y);
    g->degree[x]++;

    if (!directed) {
        insert_edge(g, y, x, 1);
    } else{
        g->nedges++;
    }
}
