#include "28_02_2023.c"
#include <assert.h>
#include <stdio.h>

void read_graph_file(char *name, graph *g, int directed) {
  FILE *fp;
  if ((fp = fopen(name, "r")) == NULL) {
    fprintf(stderr, "グラフのファイルが読み込んでません\n");
    exit(1);
  } else {
    int edge_count;
    int x, y;

    char *line = malloc(10);
    char *
        tmp; // グラフのファイルを読み込んだ時はlineポインタがもうNULLになったから

    fgets(line, 10, fp);
    if (line == NULL) {
      fprintf(stderr, "グラフのファイルが読み込んでません\n");
      exit(1);
    }

    sscanf(line, "%d %d", &(g->nvertices), &edge_count);

    while ((line = fgets(line, 10, fp)) != NULL) {
      sscanf(line, "%d %d", &x, &y);
      insert_edge(g, x, y, directed);
      tmp = line;
    }

    fclose(fp);
    free(tmp);
  }
}

void print_arr(int *items, int n) {
  for(int i = 0; i < n; i++) {
	printf("%d ", items[i]);
  }
  printf("\n");
}

int main() {
  printf("グラフの探索をテスト中\n");

  graph *g = create(0);

  // Read graph
  read_graph_file("グラフ/connected_component/graph_data", g, g->directed);

  printf("グラフ:\n");
  print(g);

  printf("Connected components:\n");
  int count = connected_components(g);

  assert(count == 2);

  free_graph(g);

  return 0;
}
