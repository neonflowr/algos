#include "graph.c"
#include "init.h"

typedef struct List {
  int data;
  struct List *next;
} list;

void free_list(list *l) {
  list *tmp;
  while (l != NULL) {
    tmp = l->next;
    free(l);
    l = tmp;
  }
}

typedef struct Queue {
  list *front;
  list *back;
  int size;
} queue;

int is_empty(queue *q) { return q->size == 0; }

queue *create_queue() {
  queue *q = malloc(sizeof(queue));
  q->front = q->back = NULL;
  q->size = 0;
  return q;
}

void enqueue(queue *q, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = NULL;
  if (q->front == NULL) {
    q->front = q->back = l;
  } else {
    q->back->next = l;
    q->back = l;
  }
  q->size++;
}

int dequeue(queue *q) {
  if (is_empty(q))
    return -1;
  list *tmp = q->front;
  q->front = q->front->next;
  int val = tmp->data;
  free(tmp);
  q->size--;
  return val;
}

void bfs(graph *g, int s, int discovered[], int processed[], int parent[],
         void (*process_v)(graph *, int)) {
  queue *frontier = create_queue();

  discovered[s] = 1;
  enqueue(frontier, s);

  edge *p;
  int v, y;

  processed[s] = 1;

  while (!is_empty(frontier)) {
    v = dequeue(frontier);

    (*process_v)(g, v);

    p = g->edges[v];

    while (p != NULL) {
      y = p->y;

      if (!discovered[y]) {
        discovered[y] = 1;
        parent[y] = v;
        enqueue(frontier, y);
        // process edge
      } else if (g->directed || !processed[y]) {
        // process edge
      }

      p = p->next;
    }
  }

  free(frontier);
}

void process_vertex(graph *g, int s) { printf("%d ", s); }

int connected_components(graph *g) {
  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];

  for (int i = 1; i <= MAXV; i++) {
    discovered[i] = 0;
    processed[i] = 0;
    parent[i] = 0;
  }

  int ci = 1;

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      printf("Connected component %d: ", ci);
      bfs(g, i, discovered, processed, parent, process_vertex);

      ci++;
      printf("\n");
    }
  }

  return ci - 1;
}
