#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct List {
  int data;
  struct List *next;
} list;

typedef struct {
  list *front;
  list *back;
  int size;
} queue;

queue *create_queue() {
  queue *q = malloc(sizeof(queue));
  q->front = q->back = NULL;
  q->size = 0;
  return q;
}

int is_empty(queue *q) { return q->size == 0; }

void enqueue(queue *q, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = NULL;
  if (q->front == NULL) {
    q->front = q->back = l;
  } else {
    q->back->next = l;
    q->back = l;
  }
  q->size++;
}

int dequeue(queue *q) {
  if (is_empty(q))
    return -1;
  list *tmp = q->front;
  q->front = tmp->next;
  int val = tmp->data;
  free(tmp);
  q->size--;
  return val;
}

typedef struct EdgeNode {
  int y;
  int weight;
  struct EdgeNode *next;
} edge;

typedef struct {
  edge *edges[MAXV + 1];
  int degree[MAXV + 1];
  int nvertices;
  int nedges;
  int directed;
} graph;

graph *create(int directed) {
  graph *g = malloc(sizeof(graph));
  g->nvertices = g->nedges = 0;
  g->directed = directed;

  for (int i = 1; i <= MAXV; i++) {
    g->edges[i] = NULL;
    g->degree[i] = 0;
  }

  return g;
}

void free_edge(edge *e) {
  edge *tmp;
  while(e != NULL) {
	tmp = e->next;
	free(e);
	e = tmp;
  }
}

void free_graph(graph *g) {
  for (int i = 1; i <= g->nvertices; i++) {
	free_edge(g->edges[i]);
  }
  free(g);
}

void insert_edge(graph *g, int x, int y, int directed) {
  edge *p = malloc(sizeof(edge));
  p->y = y;
  p->weight = 0;
  p->next = g->edges[x];
  g->edges[x] = p;

  if (!directed) {
    insert_edge(g, y, x, 1);
  } else {
    g->nedges++;
  }
}

void read_graph(graph *g) {
  int m;
  int x, y;

  scanf("%d %d", &(g->nvertices), &m);

  for (int i = 1; i <= m; i++) {
    scanf("%d %d", &x, &y);
    insert_edge(g, x, y, g->directed);
  }
}

void print(graph *g) {
  edge *p;
  for (int i = 1; i <= g->nvertices; i++) {
	printf("%d: ", i);
	p = g->edges[i];
	while(p != NULL) {
	  printf("%d ", p->y);
	  p = p->next;
	}
	printf("\n");
  }
}

void bfs(graph *g, int s, int *processed, int *discovered) {
  /* int processed[MAXV + 1]; */
  /* int discovered[MAXV + 1]; */
  int parent[MAXV + 1];

  /* for (int i = 1; i <= MAXV; i++) { */
  /*   processed[i] = 0; */
  /*   discovered[i] = 0; */
  /*   parent[i] = 0; */
  /* } */

  edge *p;
  int v, y;

  queue *frontier = create_queue();
  int q = 0;

  discovered[s] = 1;
  enqueue(frontier, s);

  while (!is_empty(frontier)) {
    v = dequeue(frontier);
    p = g->edges[v];

    // process vertex
	printf(" %d", v);

    while (p != NULL) {
      y = p->y;
      if (!processed[y] || g->directed) {
        // process edgpe
      }
      if (!discovered[y]) {
        discovered[y] = 1;
        parent[y] = v;
        enqueue(frontier, y);
      }
      p = p->next;
    }

    processed[v] = 1;
  }

  free(frontier);
}

void connected_components(graph *g) {
  int processed[MAXV + 1];
  int discovered[MAXV + 1];

  for (int i = 1; i <= MAXV; i++) {
    processed[i] = 0;
    discovered[i] = 0;
  }

  int c = 0;
  for (int i =1; i <= g->nvertices; i++) {
	if (!discovered[i]) {
	  c++;
	  printf("Component %d: ", c);
	  bfs(g, i, processed, discovered);

	  printf("\n");
	}
  }
}
