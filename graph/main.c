#include "init.h"
#include "13_03_2023.c"
#include <stdio.h>
#include <assert.h>

void read_graph(char *name, graph *g, int directed) {
  FILE *fp;
  if ((fp = fopen(name, "r")) == NULL) {
    fprintf(stderr, "グラフのファイルが読み込んでません\n");
    exit(1);
  } else {
    int edge_count;
    int x, y;

    char *line = malloc(5);
    char *tmp; // グラフのファイルを読み込んだ時はlineポインタがもうNULLになったから

    fgets(line, 5, fp);
    if (line == NULL) {
      fprintf(stderr, "グラフのファイルが読み込んでません\n");
      exit(1);
    }

    sscanf(line, "%d %d", &(g->nvertices), &edge_count);

    while ((line = fgets(line, 5, fp)) != NULL) {
      sscanf(line, "%d %d", &x, &y);
      insert_edge(g, x, y, directed);
      tmp = line;
    }

    fclose(fp);
    free(tmp);
  }
}

int main() {
  printf("グラフをテスト中\n");

  graph *g = create(0);

  read_graph("graph/graph_data", g, g->directed);

  printf("グラフ:\n");
  print(g);

  assert(has_connection(g, 1, 3));
  assert(has_connection(g, 1, 2));
  assert(has_connection(g, 3, 4));
  assert(has_connection(g, 3, 2));
  assert(!has_connection(g, 1, 4));
  assert(!has_connection(g, 2, 4));

  free_graph(g);

  getchar();

  return 0;
}

