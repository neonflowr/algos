#include <stdio.h>
#include "graph.c"
#include "init.h"

typedef struct Stack {
	int items[MAXV];
	int top;
	int size;
	int capacity;
} stack;

stack* creates() {
	stack* s = malloc(sizeof(stack));
	s->top = -1;
	s->size = 0;
	s->capacity = MAXV;
	return s;
}

void push(stack* s, int value) {
	if (s->top == s->capacity)
		return;
	s->items[++s->top] = value;
	s->size++;
}

int pop(stack* s) {
	if (s->top == -1) 
		return;
	s->size--;
	return s->items[s->top--];
}

void dfs_subroutine(graph* g, int v, int* processed, int* discovered, int* parent, stack *s) {
	discovered[v] = 1;

	int y;

	edge* p = g->edges[v];

	while (p) {
		y = p->y;
		if (!discovered[y]) {
			parent[y] = v;
			dfs_subroutine(g, y, processed, discovered, parent, s);
		} 
		if (g->directed) {
			// process
		}
		else if (!processed[y] && parent[v] != y) {
			// back edge
			// cancel the topsort if we actually cared about program correctness
		}

		p = p->next;
	}

	processed[v] = 1;
	push(s, v);
}

int* topsort(graph *g) {
	if (!g->directed) return NULL;

	int processed[MAXV + 1] = { 0 };
	int discovered[MAXV + 1] = { 0 };
	int parent[MAXV + 1] = { 0 };

	stack* s = creates();

	for (int i = 1; i <= g->nvertices; i++) {
		while (!discovered[i]) {
			dfs_subroutine(g, i, processed, discovered, parent, s);
		}
	}

	// pop the stack to get the sort
	int* topsort = calloc(s->size, sizeof(int));
	int i = 0;
	while (s->size > 0) {
		topsort[i++] = pop(s);
	}
	free(s);

	return topsort;
}

