#include "init.h"
#include "graph.c"

typedef struct Stack {
	int* items;
	int top;
	int size;
	int capacity;
} stack;

stack* create_stack(int capacity) {
	stack* s = malloc(sizeof(stack));
	s->items = calloc(capacity, sizeof(int));
	s->top = -1;
	s->size = 0;
	s->capacity = capacity;
	return s;
}

int is_full(stack* s) {
	return s->size == s->capacity;
}

int is_empty(stack* s) {
	return s->size == 0;
}

void push(stack* s, int value) {
	if (is_full(s)) return;
	s->items[++s->top] = value;
	s->size++;
}

int pop(stack* s) {
	if (is_empty(s)) return -1;
	s->size--;
	return s->items[s->top--];
}

void dfs_subroutine(graph* g, int s, int discovered[], int processed[], int parent[], stack* ts_stack) {
	discovered[s] = 1;
	int y;
	edge* p = g->edges[s];

	// process vertex early

	while (p != NULL) {
		y = p->y;
		if (!discovered[y]) {
			parent[y] = s;
			// process edge
			dfs_subroutine(g, y, discovered, processed, parent, ts_stack);
		}
		else if (g->directed) {
			// process edge
		}
		else if (!processed[y] && parent[s] != y) {
			// back edge 
			printf("found back edge. invalid graph.\n");
		}

		p = p->next;
	}

	// process late
	processed[s] = 1;
	push(ts_stack, s);
}

int *topsort(graph *g) {
	if (!g->directed) return NULL;
	stack* s = create_stack(MAXV);

	int discovered[MAXV + 1];
	int processed[MAXV + 1];
	int parent[MAXV + 1];

	for (int i = 1; i <= MAXV; i++) {
		discovered[i] = 0;
		processed[i] = 0;
		parent[i] = 0;
	}

	for (int i = 1; i <= g->nvertices; i++) {
		if (!discovered[i]) {
			dfs_subroutine(g, i, discovered, processed, parent, s);
		}
	}

	// pop stack
	int* ts = calloc(g->nvertices, sizeof(int));
	for (int i = 0; i < g->nvertices; i++) {
		ts[i] = pop(s);
	}

	free(s->items);
	free(s);

	return ts;
}

