#include "graph.c"
#include <stdio.h>

#define TREE 1
#define BACK 2
#define FORWARD 3
#define CROSS 4

char *EDGE_TYPES_STR[5] = {"", "TREE", "BACK", "FORWARD", "CROSS"};

typedef struct Stack {
  int *items;
  int capacity;
  int top;
} stack;

stack *create_stack(int capacity) {
  stack *s = malloc(sizeof(stack));
  s->capacity = capacity;
  s->top = -1;
  s->items = calloc(capacity, sizeof(int));
  return s;
}

int is_full(stack *s) {
  return s->top == s->capacity - 1;
}
int is_empty(stack *s) {
  return s->top == -1;
}

void push(stack *s, int value) {
  if (is_full(s)) return;
  s->items[++s->top] = value;
}
int pop(stack *s) {
  if (is_empty(s)) return -1;
  return s->items[s->top--];
}

void print_stack(stack *s) {
  printf("Stack: ");
  for (int i = s->top; i >= 0; i--) {
	printf("%d ", s->items[i]);
  }
  printf("\n");
}

void free_stack(stack *s) {
  free(s->items);
  free(s);
}

int edge_classification(int x, int y, int discovered[], int processed[],
                        int entry_time[], int exit_time[], int *has_cycle) {
  int result = 0;
  if (!discovered[y]) {
    result = TREE;
  }

  if (discovered[y] && !processed[y]) {
    result = BACK; // Cycle
	*has_cycle = 1;
  }

  if (processed[y] && entry_time[y] > entry_time[x]) {
    result = FORWARD;
  }

  if (processed[y] && entry_time[y] < entry_time[x]) {
    result = CROSS;
  }
  printf("Edge %d %d is of type %s\n", x, y, EDGE_TYPES_STR[result]);

  return result;
}

void process_edge(int x, int y, int discovered[]) {
  printf("Processing edge: %d %d\n", x, y);
}

void process_vertex_early(graph *g, int v, int discovered[]) {
  printf("Processing vertex early: %d\n", v);
}

void process_vertex_late(graph *g, int v, int discovered[]) {
  printf("Processing vertex late: %d\n", v);
}

void dfs_subroutine(graph *g, int s, int discovered[], int processed[],
                    int parent[], int *time, int entry_time[],
                    int exit_time[], int *has_cycle) {
  int y;

  discovered[s] = 1;

  process_vertex_early(g, s, discovered);
  edge *e = g->edges[s];

  (*time)++;
  entry_time[s] = *time;

  while (e != NULL) {
    y = e->y;
    if (!discovered[y]) {
	  parent[y] = s;
      process_edge(s, y, discovered);
      edge_classification(s, y, discovered, processed, entry_time, exit_time, has_cycle);
      dfs_subroutine(g, y, discovered, processed, parent, time, entry_time,
                     exit_time, has_cycle);
    } else if ((!processed[y] && (parent[s] != y)) || g->directed) {
      process_edge(s, y, discovered);
      edge_classification(s, y, discovered, processed, entry_time, exit_time, has_cycle);
    }
    e = e->next;
  }

  process_vertex_late(g, s, discovered);
  (*time)++;
  exit_time[s] = *time;
  processed[s] = 1;
}

int dfs(graph *g) {
  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];
  int entry_time[MAXV + 1];
  int exit_time[MAXV + 1];
  for (int i = 1; i <= MAXV; i++) {
    discovered[i] = 0;
    processed[i] = 0;
    parent[i] = 0;
    entry_time[i] = 0;
    exit_time[i] = 0;
  }

  int time = 0;

  int has_cycle= 0;

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      dfs_subroutine(g, i, discovered, processed, parent, &time, entry_time,
                     exit_time, &has_cycle);
    }
  }

  return has_cycle;
}

int has_cycle(graph *g) {
  return dfs(g);
}




int edge_classification_topsort(int x, int y, int discovered[], int processed[],
                        int entry_time[], int exit_time[], int *has_cycle) {
  int result = 0;
  if (!discovered[y]) {
    result = TREE;
  }

  if (discovered[y] && !processed[y]) {
    result = BACK; // Cycle
    *has_cycle = 1;
  }

  if (processed[y] && entry_time[y] > entry_time[x]) {
    result = FORWARD;
  }

  if (processed[y] && entry_time[y] < entry_time[x]) {
    result = CROSS;
  }
  printf("Edge %d %d is of type %s\n", x, y, EDGE_TYPES_STR[result]);

  return result;
}

void process_vertex_late_topsort(graph *g, int v, int discovered[], stack *s) {
  printf("Processing vertex late: %d\n", v);
  push(s, v);
}

void dfs_subroutine_topsort(graph *g, int s, int discovered[], int processed[],
                    int parent[], int *time, int entry_time[], int exit_time[],
                    stack *st) {
  int y;

  discovered[s] = 1;

  /* process_vertex_early(g, s, discovered); */
  edge *e = g->edges[s];

  (*time)++;
  entry_time[s] = *time;

  while (e != NULL) {
    y = e->y;
    if (!discovered[y]) {
      parent[y] = s;
      /* process_edge(s, y, discovered); */
      dfs_subroutine_topsort(g, y, discovered, processed, parent, time, entry_time,
							 exit_time, st);
    } else if ((!processed[y] && (parent[s] != y)) || g->directed) {
      /* process_edge(s, y, discovered); */
    }
    e = e->next;
  }

  process_vertex_late_topsort(g, s, discovered, st);
  (*time)++;
  exit_time[s] = *time;
  processed[s] = 1;
}

void dfs_topsort(graph *g) {
  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];
  int entry_time[MAXV + 1];
  int exit_time[MAXV + 1];
  for (int i = 1; i <= MAXV; i++) {
    discovered[i] = 0;
    processed[i] = 0;
    parent[i] = 0;
    entry_time[i] = 0;
    exit_time[i] = 0;
  }

  int time = 0;

  int has_cycle = 0;

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      dfs_subroutine(g, i, discovered, processed, parent, &time, entry_time,
                     exit_time, &has_cycle);
    }
  }
}

int *topsort(graph *g) {
  if (!g->directed || has_cycle(g)) return NULL; // Only works on DAGs

  stack *s = create_stack(MAXV);

  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];
  int entry_time[MAXV + 1];
  int exit_time[MAXV + 1];
  for (int i = 1; i <= MAXV; i++) {
    discovered[i] = 0;
    processed[i] = 0;
    parent[i] = 0;
    entry_time[i] = 0;
    exit_time[i] = 0;
  }

  int time = 0;

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      dfs_subroutine_topsort(g, i, discovered, processed, parent, &time, entry_time,
                     exit_time, s);
    }
  }

  int *top_list = calloc(g->nvertices, sizeof(int));
  int i = 0;
  while(!is_empty(s)) {
	top_list[i++] = pop(s);
  }

  free_stack(s);

  return top_list;
}
