#include "graph.c"
#include <stdio.h>
#include <stdlib.h>

typedef struct Stack {
  int *items;
  int top;
  unsigned int capacity;
} stack;

stack *create_stack(unsigned int capacity) {
  stack *s = malloc(sizeof(stack));
  s->items = calloc(capacity, sizeof(int));
  s->top = -1;
  s->capacity = capacity;
  return s;
}

int is_full(stack *s) { return s->top == s->capacity - 1; }
int is_empty(stack *s) { return s->top == -1; }

void print_stack(stack *s) {
  for (int i = 0; i <= s->top; i++) {
    printf("%d ", s->items[i]);
  }
  printf("\n");
}

void push(stack *s, int value) {
  if (is_full(s))
    return;
  s->items[++(s->top)] = value;
}

int pop(stack *s) {
  if (is_empty(s))
    return -1;
  return s->items[(s->top)--];
}

void dfs_subroutine(graph *g, int v, int discovered[], int processed[],
                    int parent[], stack *s, int *finished) {
  discovered[v] = 1;

  int y;
  edge *p = g->edges[v];

  while (p != NULL) {
    y = p->y;

    if (!discovered[y]) {
      parent[y] = v;
      // process edge
      dfs_subroutine(g, y, discovered, processed, parent, s, finished);
    } else if (g->directed || (!processed[y] && parent[v] != y)) {
      // process edge
      if (!processed[y] && parent[v] != y) { // has cycle
        *finished = 1;
      }
    }

    if (*finished == 1) {
      return;
    }

    p = p->next;
  }

  processed[v] = 1;
  push(s, v);
}

int *topsort(graph *g) {
  if (!g->directed)
    return NULL;

  int discovered[MAXV + 1];
  int processed[MAXV + 1];
  int parent[MAXV + 1];

  for (int i = 1; i <= MAXV; i++) {
    discovered[i] = 0;
    processed[i] = 0;
    parent[i] = 0;
  }

  stack *s = create_stack(MAXV);

  for (int i = 1; i <= g->nvertices; i++) {
    if (!discovered[i]) {
      int finished = 0;
      dfs_subroutine(g, i, discovered, processed, parent, s, &finished);
    }
  }

  int *top = calloc(g->nvertices, sizeof(int));
  int i = 0;
  while (!is_empty(s)) {
    top[i++] = pop(s);
  }

  free(s->items);
  free(s);

  return top;
}
