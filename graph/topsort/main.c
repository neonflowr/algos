#include "17_03_2023.c"
#include <assert.h>
#include <stdio.h>

void read_graph_file(char *name, graph *g, int directed) {
  FILE *fp;
  if ((fp = fopen(name, "r")) == NULL) {
    fprintf(stderr, "グラフのファイルが読み込んでません\n");
    exit(1);
  } else {
    int edge_count;
    int x, y;

    char *line = malloc(10);
    char *
        tmp; // グラフのファイルを読み込んだ時はlineポインタがもうNULLになったから

    fgets(line, 10, fp);
    if (line == NULL) {
      fprintf(stderr, "グラフのファイルが読み込んでません\n");
      exit(1);
    }

    sscanf(line, "%d %d", &(g->nvertices), &edge_count);

    while ((line = fgets(line, 10, fp)) != NULL) {
      sscanf(line, "%d %d", &x, &y);
      insert_edge(g, x, y, directed);
      tmp = line;
    }

    fclose(fp);
    free(tmp);
  }
}

int main() {
  printf("グラフの探索をテスト中\n");

  graph *g = create(1);

  read_graph_file("graph/topsort/topsort_graph", g, g->directed);

  printf("グラフ:\n");
  print(g);

  assert(g->directed);

  printf("トポロジカルソート中\n");
  int *order = topsort(g);

  printf("値: ");
  for(int i = 0; i < g->nvertices; i++) {
	printf("%d ", order[i]);
  }
  printf("\n");

  assert(order[0] == 8);
  assert(order[1] == 1);
  assert(order[2] == 2);
  assert(order[3] == 4);
  assert(order[4] == 5);
  assert(order[5] == 3);
  assert(order[6] == 7);
  assert(order[7] == 6);
  assert(order[8] == 9);
  assert(order[9] == 10);

  free(order);

  free_graph(g);
  
  getchar();

  return 0;
}
