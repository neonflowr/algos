#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXV 100

typedef struct Graph {
  int edges[MAXV+1][MAXV+1];
  int nedges;
  int nvertices;
  int directed;
} graph;

graph *create(int directed) {
  graph *g = malloc(sizeof(graph));

  g->nedges = g->nvertices = 0;
  g->directed = directed;

  return g;
}

void insert_edge(graph *g, int x, int y, int directed) {
  if (g == NULL) return;

  g->edges[x][y] = 1;

  if (!directed) {
	insert_edge(g, y, x, 1);
  } else {
	g->nedges++;
  }
}

void read_graph(graph *g, int directed) {
  int m;
  int x, y;

  scanf("%d %d", &(g->nvertices), &m);

  for (int i = 1; i <= m; i++) {
	scanf("%d %d", &x, &y);
	insert_edge(g, x, y, directed);
  }
}

void print(graph *g) {
  printf("   ");
  for (int i = 1; i <= g->nvertices; i++) {
	printf("%d ", i);
  }
  printf("\n");

  for (int i = 1; i <= g->nvertices; i++) {
	printf("%d: ", i);
	for (int j = 1; j <= g->nvertices; j++) {
	  printf("%d ", g->edges[j][i]);
	}
	printf("\n");
  }
}

void free_graph(graph *g) { free(g); }
