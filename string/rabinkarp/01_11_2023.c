#include <stdio.h>
#include <math.h>
#include <string.h>

long long hhash(char* s) {
  int n = strlen(s);
  printf("strlen %d\n", n);

  const int a = 26;
  const int m = 1e9 + 9;

  long long h = pow(a, n);

  for(int i = 0; i < n; i++) {
    h = (h + ((long long)pow(a, n - (i + 1)) * (s[i] - 'a' + 1)));
  }

  return h % m;
}

long long sub_hash(char *s, int n, int sub_n, int j) {
  const int a = 26;
  const int m = 1e9 + 9;

  long long h = 0;

  for(int i = 0; i <= n - sub_n; i++) {
    printf("Current char %c\n", s[i+j]);
    h = h + ( (long long) pow(a, sub_n - (i + 1)) * (s[i+j] - 'a'  + 1));
  }

  return h % m;
}

int match(char *s, char *p, int n1, int n2) {

}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    printf("Not enough args\n");
    return 0;
  }
  char* s = argv[1];
  int j = 2;

  int n = strlen(s);
  printf("strlen %d\n", n);
  
  printf("Rabin-Karp. For string %s, hash to: %d\n", s, hhash(s));
  printf("Sub-has. For string %s at j = %d, hash to: %d\n", s, j, sub_hash(s,n, n, j));
  return 0;
}
