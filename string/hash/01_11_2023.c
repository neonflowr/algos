#include <stdio.h>
#include <math.h>
#include <string.h>

long long hhash(char* s) {
  int n = strlen(s);
  printf("strlen %d\n", n);

  /* const int p = 31; // Pick careful constants */
  const int p = 53; // 53 for both upper case and lower case
  const int m = 1e9 + 9; // Pick careful constants

  long long ppow = 1;
  long long sum = 0;

  for(int i = 0; i < n; i++) {
    sum = (sum + (s[i] - 'a' + 1) * ppow) % m;
    ppow = (ppow * p) % m;
  }
  
  return sum;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    printf("Not enough args\n");
    return 0;
  }
  char* s = argv[1];
  printf("For string %s, hash to: %d", s, hhash(s));
  return 0;
}
