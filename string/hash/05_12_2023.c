#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>

long long hash(char *s, int n) {
  const long long p = 53; // or 31
  const long long m = 1e9 + 9;

  long long size = (long long) n;
  long long sum = 0;
  long long pw = 1;

  for(int i = 0; i < size; i++) { 
    sum = (sum + s[i] * pw) % m;
    pw = (pw * p) % m;
  }
  return sum;
}

void readf(char *s, char **out, long *size) {
  FILE *f;
  if ((f = fopen(s, "rb")) == NULL) {
    perror("Can't open file");
    return;
  }

  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  *size = fsize;
  fseek(f, 0, SEEK_SET);
  int n;

  *out = (char *)malloc(sizeof(fsize + 1));
  n   = fread(*out, 1, fsize, f);
  fclose(f);
  (*out)[fsize] = 0;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    perror("Usage: hash filename");
    return 0;
  }

  char *buf;
  long size;
  readf(argv[1], &buf, &size);

  printf("Buf %ld\n", size);
  printf(buf);
  long long h = hash(buf, size);
  printf("Hash: %lld\n", h);
  printf("Hexa Hash: %llx\n", h);
  
  return 0;
}
