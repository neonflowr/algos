;; 1.3
(define (square a) (* a a))
(define (sum-squares a b) (+ (square a) (square b)))

(define (max a b) (if (> a b) a b))
(define (min a b) (if (< a b) a b))

(define (tmax a b c) (
	max (max a b) c
))
(define (tmin a b c) (
	min (min a b) c
))

(define (three-sum-sub m a b c) 
	(cond 
		((= m a) (sum-squares b c))
		((= m b) (sum-squares a c))
		((= m c) (sum-squares a b))
	)
)

(define (three-sum a b c) (
	three-sum-sub (tmin a b c) a b c
))

(display (three-sum 1 2 3)) ;; 13
(newline)
(display (three-sum 2 4 1)) ;; 20
(newline)

;; 1.6
(define (square a) (* a a))

(define (abs x)
	(cond 
		((< x 0) (- x))
		(else x)
	)
)

(define (average x y) (/ (+ x y) 2))

(define (improve guess x) (average guess (/ x guess)))

(define (good-enough? guess x) 
	(< (abs (- (square guess) x)) 0.001)
)

(define (sqrt-iter guess x)
	(if (good-enough? guess x)
		guess
		(sqrt-iter (improve guess x) x)
	))

(define (new-if predicate then-clause else-clause)
	(cond (predicate then-clause)
		(else else-clause))
)

(define (nsqrt-iter guess x)
	(new-if (good-enough? guess x)
		guess
		(nsqrt-iter (improve guess x) x)
	)
)

(define (sqrt x) (nsqrt-iter 1.0 x))
(sqrt 2)

;; 1.8
(define (square a) (* a a))
(define (cube a) (* a a a))

(define (abs x)
	(cond 
		((< x 0) (- x))
		(else x)
	)
)

(define (improve guess x) 
	(/ (+ (/ x (square guess)) (* 2 guess)) 3)
	)

(define (good-enough? guess x) 
	(< (abs (- (cube guess) x)) 0.0001)
)

(define (cube-iter guess x)
	(if (good-enough? guess x)
		guess
		(cube-iter (improve guess x) x)
	))

(define (cb x) (cube-iter 3.0 x))
(cb 27)

;; 1.10
(define (A x y)
	(cond ((= y 0) 0)
		((= x 0) (* 2 y))
		((= y 1) 2)
		(else (A (- x 1) (A x (- y 1))))
	)
)

;; 1.11 правильно
(define (foo n) 
	(if (< n 3) 
		n 
		(+ (foo (- n 1))
			(* 2 (foo (- n 2)))
			(* 3 (foo (- n 3)))
		)
	)
)
(foo 3)

(define (nfoo-iter n i a b c)
	(if (> i n) 
		a 
		(nfoo-iter 
			n
			(+ i 1)
			(+ a (* 2 b) (* 3 c))
			a 
			b
			)
		)
)

(define (nfoo n)
	(nfoo-iter n 3 2 1 0)
)
(nfoo 32)

;; 1.16
;; Iterative exponentiation by successive squaring
(define (square x) (* x x))
(define (even? x)
	(= (remainder x 2) 0))
(define (exp-iter b n)
	(cond ((= n 1) b)
		((= n 0) 1)
		((even? n) (exp-iter (square b) (/ n 2))) ;; Ok so this is iterative
		(else (* b (exp-iter b (- n 1)))) ;; But this line isn't, because it's gonna be waiting around for the recursive call's result
	)
)

(define (exp b n)
	(exp-iter b n 1)
)
;; Really just think in the call stack depth
;; Iterative processes don't deepen the call stack at all
;; The interpreter once it finishes with a call of the procedure can just discard the 'frame' instead of keeping it around like recursive processes do

;; 1.17
;; Basically create log(x) multiplication algorithm
(define (max a b) (if (> a b) a b))
(define (min a b) (if (< a b) a b))

(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x)
	(= (remainder x 2) 0))

(define (mul-iter a b)
	(cond ((= b 1) a)
		((even? b) (mul-iter (double a) (halve b)))
		(else (+ a (mul-iter a (- b 1)))
		)
	)
)


(define (mulme a b)
	(mul-iter (max a b) (min a b))
)

;; 1.18
;; Basically iterative 1.17

(define (max a b) (if (> a b) a b))
(define (min a b) (if (< a b) a b))

(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x)
	(= (remainder x 2) 0))

(define (mul-iter a b remain)
	(cond ((= b 1) (+ a remain))
		((even? b) (mul-iter (double a) (halve b) remain))
		(else (mul-iter a (- b 1) (+ remain a)))
		)
	)

(define (mulme-iter a b)
	(mul-iter (max a b) (min a b) 0)
)

;; 1.21
(define (smallest-divisor n)
	(find-divisor n 2))

(define (find-divisor n test-divisor)
	(cond ((> (square test-divisor) n) n)
		((divides? test-divisor n) test-divisor)
		(else (find-divisor n (+ test-divisor 1)))
	)
)

(define (divides? a b)
	(= (remainder b a) 0))

(define (prime? n)
	(= (smallest-divisor n) n))

;; 1.22
(define (prime? n)
	(= (smallest-divisor n) n))

(define (timed-prime-test n)
	(newline)
	(display n)
	(start-prime-test n (runtime))
)

(define (start-prime-test n start-time)
	(if (prime? n)
		(report-prime (- (runtime) start-time))
	)
)

(define (report-prime elapsed-time)
	(display " *** ")
	(display elapsed-time)
)

(define (sfp-sub min max n time)
	(cond ((= n max) (- (runtime) time))
		((prime? n) 
			; (display n)
			; (newline)
			(sfp-sub min max (+ n 1) time)
		)
		(else (sfp-sub min max (+ n 1) time))
	)
)

(define (search-for-primes min max)
	(display "(OLD) Search for primes in range from ")
	(display min)
	(display " to ")
	(display max)
	(newline)
	(display "Time taken: ")
	(display (sfp-sub min max min (runtime)))
	(newline)
)

;; 1.23
;; From the hypothesis, we suspect that the performance should be better since we only have to check half
;; as many numbers from before
;; but really, those numbers that we skipped are even numbers
;; so even if we didn't skip them, it would only be a short check (since 
;; you'd know they're not prime as soon as you divide it by 2)
;; So in practice, we do get a speed boost, but not a doubling speed boost

(define (smallest-divisor n)
	(find-divisor n 2))

(define (next test-divisor)
	(if (= test-divisor 2) 3 (+ test-divisor 2))
)

(define (find-divisor n test-divisor)
	(cond ((> (square test-divisor) n) n)
		((divides? test-divisor n) test-divisor)
		(else (find-divisor n (next test-divisor)))
	)
)

(define (divides? a b)
	(= (remainder b a) 0))

(define (prime? n)
	(= (smallest-divisor n) n))

(define (timed-prime-test n)
	(newline)
	(display n)
	(start-prime-test n (runtime))
)

(define (start-prime-test n start-time)
	(if (prime? n)
		(report-prime (- (runtime) start-time))
	)
)

(define (report-prime elapsed-time)
	(display " *** ")
	(display elapsed-time)
)

(define (sfp-sub min max n time)
	(cond ((= n max) (- (runtime) time))
		((prime? n) 
			; (display n)
			; (newline)
			(sfp-sub min max (+ n 1) time)
		)
		(else (sfp-sub min max (+ n 1) time))
	)
)

(define (search-for-primes-half min max)
	(display "(NEW) Search for primes in range from ")
	(display min)
	(display " to ")
	(display max)
	(newline)
	(display "Time taken: ")
	(display (sfp-sub min max min (runtime)))
	(newline)
)

;; 1.24
;; Very little growth
;; (sfp 10000) is 0.08
;; (sfp 10000000) is 0.089
;; Though there is a slight discrepancy between the expected log_2(1000) growth
;; But maybe that's just because prime? always just test 20 times since it's a probability test
(define (smallest-divisor n)
	(find-divisor n 2))

(define (next test-divisor)
	(if (= test-divisor 2) 3 (+ test-divisor 2))
)

(define (find-divisor n test-divisor)
	(cond ((> (square test-divisor) n) n)
		((divides? test-divisor n) test-divisor)
		(else (find-divisor n (next test-divisor)))
	)
)

(define (divides? a b)
	(= (remainder b a) 0))

;;
(define (square n) (* n n))

(define (expmod base exp m)
	(cond ((= exp 0) 1)
		((even? exp) 
			(remainder (square (expmod base (/ exp 2) m)) m)
		)
		(else (remainder (* base (expmod base (- exp 1) m)) m))
	)
)

(define (fermat-test n)
	(define (try-it a)
		(= (expmod a n n) a))
	(try-it (+ 1 (random (- n 1))))
)

(define (fast-prime? n times)
	(cond ((= times 0) true)
		((fermat-test n) (fast-prime? n (- times 1)))
		(else false)
	)
)

; (define (prime? n)
; 	(= (smallest-divisor n) n))
(define (prime? n)
	(fast-prime? n 20)
)

(define (timed-prime-test n)
	(newline)
	(display n)
	(start-prime-test n (runtime))
)

(define (start-prime-test n start-time)
	(if (prime? n)
		(report-prime (- (runtime) start-time))
	)
)

(define (report-prime elapsed-time)
	(display " *** ")
	(display elapsed-time)
)

(define (sfp-sub min max n time)
	(cond ((= n max) (- (runtime) time))
		((prime? n) 
			; (display n)
			; (newline)
			(sfp-sub min max (+ n 1) time)
		)
		(else (sfp-sub min max (+ n 1) time))
	)
)

(define (sfp min)
	(display "(NEW) Search for primes in range from ")
	(display min)
	(display " to ")
	(display (+ min 1000))
	(newline)
	(display "Time taken: ")
	(display (sfp-sub min (+ min 1000) min (runtime)))
	(newline)
)
