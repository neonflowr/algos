(define (abs x)
  (cond
   ((< x 0) (- x))
   (else x)
   )
  )

;; Yes, you can use wacky stuff as procedure names
(define (>= x y)
  (or (> x y) (= x y)))


;; 1.2
(/  (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5)))))
	(* 3 (- 6 2) (- 2 7))
	)


;; 1.3
(define (square x) (* x x))

(define (larger_sum x y z)
  (define num1 (if (> x y) x y))
  (define num2
	(if (= x num1) (if (> y z) y z)  (if (> x z) x z ))
	)
  (+ (square num1) (square num2))
 )

;; 1.4
(define (a-plus-abs-b a b)
		((if (> b 0) + -) a b)) ;; You can return operators from expressions

;; 1.5

;; Normal order evaluation:
;; As operands aren't evaluated until they are needed, the value of (p) is never
;; evaluated as x is 0
;; Applicative order evaluation:
;; The operands 0 and (p) is evaluated when (test) is called, however since (p) returns itself, it results in an endless loop.

(define (p) (p))
(define (test x y)
        (if (= x 0) x y)
  )
(test 0 (p))
