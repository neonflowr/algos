#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct __list {
  int data;
  pthread_mutex_t lock;
  struct __list *next;
} list_node;

typedef struct {
  list_node **nodes;
  pthread_mutex_t g_lock;
} list;

typedef struct __thread_insert_args {
  list *head;
  int data;
} thread_insert_args;

list* create_list() {
  list *head  = malloc(sizeof(list));
  head->nodes = malloc(sizeof(list_node *));
  pthread_mutex_init(&head->g_lock, NULL);
  return head;
}

void insert(list *head, int data) {
  list_node *node = malloc(sizeof(list_node));
  node->data = data;
  node->next = NULL;
  pthread_mutex_init(&node->lock, NULL);

  if (*head->nodes == NULL) {
    pthread_mutex_lock(&head->g_lock);
    printf("Insert at head %d\n", data);
    *head->nodes = node;
    pthread_mutex_unlock(&head->g_lock);
    return;
  } else {
    // Insert at end
    pthread_mutex_lock(&head->g_lock);
    /* printf("Insert %d\n", data); */
    list_node *p = *head->nodes;
    list_node *prev;
    pthread_mutex_unlock(&head->g_lock);

    pthread_mutex_lock(&p->lock);
    while(p != NULL) {
	prev = p;
	p = p->next;
	if (p != NULL)
	  pthread_mutex_lock(&p->lock);
	pthread_mutex_unlock(&prev->lock);
    }

    pthread_mutex_lock(&prev->lock);
    prev->next = node;
    pthread_mutex_unlock(&prev->lock);
  }
}

void print(list *l) {
  pthread_mutex_lock(&l->g_lock);
  list_node *p = *l->nodes;
  list_node *prev;
  while(p != NULL) {
    pthread_mutex_lock(&p->lock);
    printf("%d ", p->data);
    prev = p;
    p = p->next;
    pthread_mutex_unlock(&prev->lock);
  }
  printf("\n");
  pthread_mutex_unlock(&l->g_lock);
}

void *thread_insert(void *args) {
  thread_insert_args* arg = (thread_insert_args*) args;
  insert(arg->head, arg->data);
  return NULL;
}

void test_threads(int t_count) {
  list *head = create_list();
  pthread_t *threads = calloc(t_count, sizeof(pthread_t));
  thread_insert_args *args = calloc(t_count, sizeof(thread_insert_args));

  for(int i = 0; i < t_count; i++) {
    args[i].data = i + 1;
    args[i].head = head;
    pthread_create(&threads[i], NULL, thread_insert, &args[i]);
  }

  for(int i = 0; i < t_count; i++) {
    pthread_join(threads[i], NULL);
  }

  /* print(head); */

  free(threads);
  free(args);
  free(head);
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Not enough args\n");
    return 0;
  }

  printf("INDIVIDUAL LOCK VERSION\n");

  int thread_count = atoi(argv[1]);

  printf("Thread count %d\n", thread_count);

  float start_time = (float) clock() / CLOCKS_PER_SEC;
  test_threads(thread_count);
  float end_time = (float) clock() / CLOCKS_PER_SEC;
  float time_elapsed = end_time - start_time;
  printf("Time taken: %f\n", time_elapsed);

  return 0;
}

