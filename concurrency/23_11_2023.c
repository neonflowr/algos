#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

typedef struct __ctr {
  int data;
  pthread_mutex_t lock;
} ctr;

typedef struct {
  ctr *c;
  int target;
  int id;
} myarg_t;

ctr *ctr_create(int initial) {
  ctr *c = (ctr *)malloc(sizeof(ctr));
  c->data = initial;
  int rc = pthread_mutex_init(&c->lock, NULL);
  assert(rc == 0);
  return c;
}

void* ctr_increment(void *arg) {
  ctr *c = (ctr *) arg;
  pthread_mutex_lock(&c->lock);
  c->data = c->data + 1;
  pthread_mutex_unlock(&c->lock);
  return &c->data;
}

int ctr_get(ctr *c) {
  pthread_mutex_lock(&c->lock);
  int data = c->data;
  pthread_mutex_unlock(&c->lock);
  return data;
}

void free_ctr(ctr *c) {
  pthread_mutex_destroy(&c->lock);
  free(c);
}

void* thread_increment(void *args) {
  myarg_t *arg = (myarg_t *) args;
  for(int i = 0; i < arg->target; i++) {
    ctr_increment(arg->c);
  }
  return NULL;
}

void test_ctr(ctr* c, int thread_count, int target) {
  pthread_t *threads = calloc(thread_count, sizeof(pthread_t));
  for (int i = 0; i < thread_count; i++) {
    int id = i + 1;
    printf("Creating thread %d\n", id);
    pthread_t t;
    myarg_t args = {c, target, id};
    int rc = pthread_create(&threads[i], NULL, thread_increment, &args);
    assert(rc == 0);
    printf("Finished creating thread %d, code %d\n", id, rc);
  }

  for(int i = 0; i < thread_count; i++) {
    int id = i + 1;
    assert(pthread_join(threads[i], NULL) == 0);
    printf("Thread %d finished\n", id);
  }

  printf("Final counter: %d\n", ctr_get(c));
  free(threads);
}

int main(int argc, char **argv) {
  if (argc < 4) {
    printf("Not enough args\n");
    return 0;
  }

  int init         = atoi(argv[1]);
  int thread_count = atoi(argv[2]);
  int target       = atoi(argv[3]);
  ctr *c           = ctr_create(init);

  printf("Initial counter value: %d\n", ctr_get(c));
  printf("Thread count: %d\n", thread_count);
  printf("Target: %d\n", target);

  test_ctr(c, thread_count, target);

  free_ctr(c);
  
  return 0;
}
