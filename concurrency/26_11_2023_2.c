#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

#define NUMCPU 4

typedef struct __ctr {
  int data;
  pthread_mutex_t lock;
} ctr_t;

typedef struct {
  ctr_t *ctr;
  int target;
} arg_thread_increment_t;

void increment(ctr_t *ctr) {
  pthread_mutex_lock(&ctr->lock);
  ctr->data++;
  pthread_mutex_unlock(&ctr->lock);
}

void decrement(ctr_t *ctr) {
  pthread_mutex_lock(&ctr->lock);
  ctr->data--;
  pthread_mutex_unlock(&ctr->lock);
}

ctr_t* create_ctr() {
  ctr_t* c = malloc(sizeof(ctr_t));
  if (c == NULL) {
    perror("malloc");
    return NULL;
  }
  c->data = 0;
  pthread_mutex_init(&c->lock, NULL);
  return c;
}

int get_ctr(ctr_t *ctr) {
  pthread_mutex_lock(&ctr->lock);
  int val = ctr->data;
  pthread_mutex_unlock(&ctr->lock);
  return val;
}

void* thread_increment(void *args) {
  arg_thread_increment_t *arg = (arg_thread_increment_t *) args;
  ctr_t* ctr = (ctr_t *) arg->ctr;
  for(int i = 0; i < arg->target; i++) {
    increment(ctr);
  }
  return NULL;
}

void test_threads(int thread_count, int target) {
  if (thread_count > 4) thread_count = 4;

  printf("Target %d\nCount: %d\n", target, thread_count);

  pthread_t threads[NUMCPU];

  ctr_t *ctr = create_ctr();
  if (ctr == NULL) return;

  for(int i = 0; i < thread_count; i++) {
    arg_thread_increment_t arg = {ctr, target};
    int rc = pthread_create(&threads[i], NULL, thread_increment, &arg);
    assert(rc == 0);
  }
  for(int i = 0; i < thread_count; i++) {
    int rc = pthread_join(threads[i], NULL);
    assert(rc == 0);
  }
  printf("Final counter value: %d\n", get_ctr(ctr));

  free(ctr);
}

int main(int argc, char **argv){
  int target, thread_count;
  if (argc < 3) {
    target       = 1000;
    thread_count = 4;
  } else {
    thread_count = atoi(argv[1]);
    target       = atoi(argv[2]);
  }

  float start_time = (float) clock() / CLOCKS_PER_SEC;
  test_threads(thread_count, target);
  float end_time = (float) clock() / CLOCKS_PER_SEC;
  float time_elapsed = end_time - start_time;
  printf("Time elapsed: %f\n", time_elapsed);

  return 0;
}
