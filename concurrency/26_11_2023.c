#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

typedef struct __node_t {
  int data;
  struct __node_t *next;
} node_t;

typedef struct {
  node_t *nodes;
  pthread_mutex_t lock;
} list_t;

list_t* create_list() {
  list_t *l = malloc(sizeof(list_t));
  l->nodes = NULL;
  pthread_mutex_init(&l->lock, NULL);
  return l;
}

void insert(list_t *l, int data) {
  node_t *new = malloc(sizeof(node_t));
  if (new == NULL) {
    perror("malloc");
    return;
  }
  new->data = data;
  pthread_mutex_lock(&l->lock);
  new->next = l->nodes;
  l->nodes = new;
  pthread_mutex_unlock(&l->lock);
  return;
}

int lookup(list_t *l, int x) {
  int rv = -1;
  pthread_mutex_lock(&l->lock);
  node_t *current = l->nodes;
  while(current != NULL) {
    if (current->data == x) {
      rv = 0;
      break;
    }
    current = current->next;
  }
  pthread_mutex_unlock(&l->lock);
  return rv;
}

int main(int argc, char **argv) {

  return 0;
}
