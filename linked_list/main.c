#include "21_02_2023.c"
#include "init.h"
#include <assert.h>

void sorted();
void unsorted();

int main(int argc, char **argv) {
  printf("連結リストをテスト中\n");

  unsorted();
  /* sorted(); */

  return 0;
}

/* void sorted() { */
/*   struct List **head; */
/*   *head = NULL; */

/*   insert(head, 1); */
/*   insert(head, 2); */
/*   insert(head, 3); */
/*   insert(head, 4); */

/*   print(*head); */

/*   assert(search(*head, 2)->data == 2); */
/*   assert(search(*head, 5) == NULL); */

/*   assert(item_ahead(*head, search(*head, 4))->data == 3); */

/*   struct List *node = search(*head, 4); */
/*   delete_node(head, &node); */

/*   assert(*head != NULL && (*head)->data == 1); */

/*   print(*head); */

/*   node = search(*head, 2); */
/*   delete_node(head, &node); */
/*   print(*head); */

/*   node = search(*head, 1); */
/*   delete_node(head, &node); */
/*   print(*head); */

/*   insert(head, 1); */
/*   insert(head, 2); */
/*   insert(head, 4); */
/*   print(*head); */

/*   assert((*head)->data == 1); */
/*   assert((*head)->next->data == 2); */
/*   assert((*head)->next->next->data == 3); */
/*   assert((*head)->next->next->next->data == 4); */
/* } */

void unsorted() {
  struct List **head;
  *head = NULL;
  insert(head, 1);
  insert(head, 2);
  insert(head, 3);
  insert(head, 4);

  print(*head);

  assert(search(*head, 2)->data == 2);
  assert(search(*head, 5) == NULL);

  assert(item_ahead(*head, search(*head, 3))->data == 4);

  struct List *node = search(*head, 4);
  delete_node(head, &node);

  assert(*head != NULL && (*head)->data == 3);
  assert(item_ahead(*head, search(*head, 3)) == NULL);

  print(*head);
}
