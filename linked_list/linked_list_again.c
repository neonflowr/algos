#include <stdio.h>
#include <stdlib.h>

typedef struct list {
  int data;
  struct list *next;
} list;

list *search(list *l, int item);
void insert(list **head, int item);
void delete_by_value(list **head, int item);
void delete_by_node(list **head, list **x);
list *item_ahead(list *l, list *node_to_find);
void print_list(list *l);

int main() {
  printf("Testing\n");

  list start_node = {1, NULL};
  list *p_start_node = &start_node;

  list **head = &p_start_node;

  printf("Beginning\n");
  print_list(*head);

  printf("Insert\n");

  insert(head, 2);
  insert(head, 3);
  insert(head, 4);

  printf("Inserted\n");
  print_list(*head);

  printf("Remove 2 by value\n");
  delete_by_value(head, 2);

  printf("2 removed\n");
  print_list(*head);

  printf("Remove 4 by node\n");
  list *node_4 = search(*head, 4);
  delete_by_node(head, &node_4);

  printf("4 removed\n");
  print_list(*head);

  p_start_node = NULL;
  head = NULL;
  node_4 = NULL;

  return 0;
}

list *search(list *l, int item) {
  if (l->next == NULL) {
    return NULL;
  }

  if (l->data == item) {
    return l;
  } else {
    return search(l->next, item);
  }
}

// Insert item at head of the list
void insert(list **head, int item) {
  list *node = malloc(sizeof(list));
  node->data = item;
  node->next = *head;

  // Change the head to point at the new node
  *head = node;
}

list *item_ahead(list *l, list *node_to_find) {
  if ((l == NULL) || (l->next == NULL)) {
    return NULL;
  }

  if (l->next == node_to_find) {
    return l;
  } else {
    return item_ahead(l->next, node_to_find);
  }
}

void delete_by_value(list **head, int item) {
  // Need to get the item before the item being deleted somehow
  list *node_to_delete = search(*head, item);

  list *preceding_node = item_ahead(*head, node_to_delete);

  if (preceding_node == NULL) { // node to find is at the top of the linked list
    *head = node_to_delete->next;
  } else {
    preceding_node->next = node_to_delete->next;
  }

  free(node_to_delete);
}

void delete_by_node(list **head, list **x) {
  list *p;
  list *pre;

  p = *head;

  pre = item_ahead(p, *x);

  if (pre == NULL) { // node to find is at the top of the linked list
    *head = p->next;
  } else {
    pre->next = (*x)->next;
  }

  free(*x);
  x = NULL;
}

void print_list(list *l) {
  if (l->next == NULL) {
    printf("%d\n", l->data);
    return;
  } else {
    printf("%d -> ", l->data);
    print_list(l->next);
  }
}
