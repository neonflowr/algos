#include <stdio.h>
#include <stdlib.h>

typedef struct List  {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *n = malloc(sizeof(list));
  n->data = value;
  n->next = *head;
  *head = n;
}

list *search(list *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;

  return search(l->next, value);
}

list *item_ahead(list *l, list *x) {
  if (l == NULL)
	return NULL;

  while(l != NULL && l->next != x) {
	l = l->next;
  }

  return l;
}

void delete_node(list **head, list **x) {
  list *head_tmp = *head;
  list *pre;

  printf("What\n");
  printf("data: %d\n", head_tmp->data);
  pre = item_ahead(head_tmp, *x);
  if (pre == NULL) {
    // At head of the list
	*head = (*head)->next;
	free(head_tmp);
	head_tmp = NULL;
  } else {
	pre->next = (*x)->next;
	free(*x);
	*x = NULL;
  }
}

void print(list *l) {
  printf("List: ");
  while (l != NULL) {
    printf("%d ", l->data);
    l = l->next;
  }
  printf("\n");
}
