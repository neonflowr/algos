#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;
  *head = l;
}

list *search(list *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
    return l;
  return search(l->next, value);
}

list *item_ahead(list *l, list *x) {
  if (l == NULL)
	return NULL;

  while(l != NULL && l->next != x) {
	l = l->next;
  }

  return l;
}

void delete_node(list **head, list **x) {
  list *tmp = *head;

  list *pred = item_ahead(*head, *x);
  if (pred == NULL) {
	*head = tmp->next;
	free(tmp);
	tmp = NULL;
  } else {
	pred->next = (*x)->next;
	free(*x);
	*x = NULL;
  }
}

void print(list *l) {
  while(l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
