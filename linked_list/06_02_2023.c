#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
  int data;
  struct List *next;
} list;

list *find_middle(list *head) {
  list *slow = head;
  list *fast = head;
  while(fast->next != NULL) {
	fast = fast->next;
	if (fast->next)
	  fast = fast->next;
	else
	  break;
	slow = slow->next;
  }

  return slow;
}


void insert(list **head, int value){
  if (*head == NULL) {
	list *l = malloc(sizeof(list));
	l->data = value;
	l->next = NULL;
	*head = l;
	return;
  }

  list *current = *head;
  list *prev;
  list *next;

  while (current != NULL) {
	next = current->next;
	if (next) {
	  if (next->data > value) {
		list *l = malloc(sizeof(list));
		l->data = value;
		l->next = next;
		current->next = l;
		return;
	  } else {
		prev = current;
		current = next;
		continue;
	  }
	}

	if (current == *head && current->data > value) {
	  // Change head
	  list *l = malloc(sizeof(list));
	  l->data = value;
	  l->next = current;
	  *head = l;
	  return;
	} else {
	  list *l = malloc(sizeof(list));
	  l->data = value;
	  l->next = NULL;
	  current->next = l;
	  return;
	}
  }
}

list *search(list *l, int value) {
  while (l != NULL) {
	if (l->data == value)
	  return l;
	l = l->next;
  }

  return NULL;
}

list *item_ahead(list *l, list *x) {
  while (l != NULL && l->next != x) {
	l = l->next;
  }
  return l;
}

void delete_node(list **l, list **x) {
  list *tmp = *l;
  list *pred = item_ahead(*l, *x);
  if (pred == NULL) {
	*l = (*x)->next;
	free(tmp);
	tmp = NULL;
  } else {
	pred->next = (*x)->next;
	free(*x);
	*x = NULL;
  }
}

void print(list *l) {
  while (l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
