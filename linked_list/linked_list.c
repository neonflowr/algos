#include <stdio.h>
#include <stdlib.h>

typedef struct list {
  int item;
  struct list *next;
} list;

list *search(list *l, int x);
void insert(list **l, int x);
list *item_ahead(list *l, list *x);
void delete_list(list **l, list **x);

int main() {
  return 0;
}

// Recursively search a singly linked list for node with value of x
list *search(list *l, int x) {
  if (l == NULL) {
    return (NULL);
  }

  // struct->property
  if (l->item == x) {
    return (l);
  } else {
    return (search(l->next, x));
  }
}

// Insert x into the list at the beginning
// **list is a pointer pointing to the head of the linked list
void insert(list **l, int x) {
  struct list *p;

  // Create a new node p
  p = malloc(sizeof(list));
  p->item = x;
  p->next = *l; // Points to the head of the linked list
  *l = p; // Changing the tracking pointer to the newly created p node
}

// Find the node that points to the node x
list *item_ahead(list *l, list *x) {
  if ((l == NULL) || (l-> next == NULL)) {
    return (NULL);
  }

  if ((l->next) == x) {
    return(l);
  } else {
    return(item_ahead(l->next, x));
  }
}

void delete_list(list **l, list **x) {
  list *p; // Item pointer
  list *pred; // Point to the predecessor of the node to be deleted

  p = *l;
  pred = item_ahead(*l, *x);

  if (pred == NULL) { // Node to delete is the beginning of the list
    *l = p->next;
  } else {
    pred->next = (*x)->next;
  }
  free(*x);
}
