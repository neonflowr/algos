// Maybe write an automatic test ?

#include <stdio.h>
#include <stdlib.h>

typedef struct list {
  int data;
  struct list *next;
} list;

list *search(list *l, int value) {
  if (l == NULL)
    return NULL;

  if (l->data == value)
    return l;

  return search(l->next, value);
}

void insert(list **head, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = *head;

  *head = l;
}

list *item_ahead(list *l, list *node_to_find) {
  if (l == NULL) {
    return NULL;
  }
  if (l->next == node_to_find) {
    return l;
  } else {
    return item_ahead(l->next, node_to_find);
  }
}

void delete (list **head, list **node) {
  // You have to realize C passes in copy of pointers too
  // If you pass in a pointer, you can think of it as passing in the reference
  // to the value of the pointer But the pointer itself isn't passed in, so you
  // cannot change the pointer. You can change the value through the passed in
  // pointer but not the pointer itself.

  list *p;
  list *pred = item_ahead(*head, *node);

  p = *head;

  if (pred == NULL) {
    *l = p->next;
  } else {
    pred->next = *node->next;
  }
  free(*node);
}

int main() { return 0; }
