#include <stdio.h>
#include <stdlib.h>

struct List {
  int data;
  struct List *next;
};

void insert(struct List **head, int value) {
  struct List *node = malloc(sizeof(struct List));
  node->data = value;
  node->next = *head;
  *head = node;
}

struct List *search(struct List *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;
  return search(l->next, value);
}

struct List *item_ahead(struct List *l, struct List *x) {
  if (l == NULL)
	return NULL;
  if (l->next == x)
	return l;

  return item_ahead(l->next, x);
}

void delete(struct List **head, struct List **x) {
  struct List *tmp = *head; // Need to store the head because if the head is replaced with a new one, we want to free the old head
  struct List *pred = item_ahead(*head, *x);
  if (pred == NULL)
	*head = tmp->next;
  else
	pred->next = (*x)->next;
  free(*x);
  *x = NULL;
}
