#include "init.h"
#include "23_02_2023.c"
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

void string_table_test() {
  struct HashTable *t = create(TABLE_SIZE);

  insert(t, "Some", 69);
  insert(t, "Simple", 65);
  insert(t, "String", 32);

  print(t);

  assert(get(t, "Some") == 69);
  assert(get(t, "Simple") == 65);
  assert(get(t, "Invalid") == -1);
  assert(get(t, "Stuff") == -1);

  print(t);

  // Test collision
  insert(t, "Some", 12);
  insert(t, "Some", 32);
  insert(t, "omes", 69);
  insert(t, "oems", 232);
  insert(t, "eoms", 332);

  assert(get(t, "omes") == 69);
  assert(get(t, "oesm") == -1);
  assert(get(t, "oems") == 232);
  assert(get(t, "eoms") == 332);
  assert(get(t, "osme") == -1);

  printf("Test collision\n");
  print(t);

  // Test deletion
  delete_ht(t, "Some");
  delete_ht(t, "oems");

  printf("Test delete 1\n");
  print(t);

  assert(get(t, "Some") == -1);
  assert(get(t, "oems") == -1);
  assert(get(t, "omes") == 69);
  assert(get(t, "eoms") == 332);
  delete_ht(t, "omes");
  assert(get(t, "omes") == -1);
  assert(get(t, "eoms") == 332);
  delete_ht(t, "eoms");
  assert(get(t, "eoms") == -1);

  printf("Test delete 1 finished\n");

  printf("Test delete nonexistent items\n");
  print(t);

  assert(get(t, "Nonexistent") == -1);
  assert(get(t, "noexist") == -1);

  delete_ht(t, "Nonexistent");
  delete_ht(t, "noexist");

  assert(get(t, "Nonexistent") == -1);
  assert(get(t, "noexist") == -1);

  free_table(&t);

  assert(t == NULL);
}


int main() {
  printf("Test hash table\n");

  /* int_key_test(); */
  string_table_test();

  return 0;
}

/* void int_key_test() { */
/*   struct HashTable *t = create(TABLE_SIZE); */

/*   insert(t, 10, 69); */
/*   insert(t, 15, 65); */
/*   insert(t, 245, 32); */

/*   print(t); */

/*   assert(((struct List *)get(t, 10))->value == 69); */
/*   assert(((struct List *)get(t, 15))->value == 65); */
/*   assert(get(t, 445) == NULL); */
/*   assert(get(t, 534) == NULL); */

/*   print(t); */

/*   // Test collision */
/*   insert(t, 432, 12); */
/*   insert(t, 232, 22); */
/*   insert(t, 132, 32); */
/*   insert(t, 32, 420); */

/*   assert(((struct List *)get(t, 432))->value == 12); */
/*   assert(get(t, 532) == NULL); */
/*   assert(((struct List *)get(t, 132))->value == 32); */
/*   assert(((struct List *)get(t, 232))->value == 22); */
/*   assert(((struct List *)get(t, 32))->value == 420); */
/*   assert(get(t, 332) == NULL); */

/*   print(t); */

/*   // Test deletion */
/*   delete_ht(t, 432); */
/*   delete_ht(t, 532); */
/*   delete_ht(t, 132); */
/*   delete_ht(t, 232); */
/*   delete_ht(t, 432); */
/*   print(t); */

/*   assert(get(t, 432) == NULL); */
/*   assert(get(t, 532) == NULL); */
/*   assert(get(t, 132) == NULL); */
/*   assert(get(t, 232) == NULL); */
/*   assert(((struct List *)get(t, 32))->value == 420); */
/*   assert(get(t, 332) == NULL); */

/*   print(t); */

/*   assert(get(t, 69) == NULL); */
/*   assert(get(t, 65) == NULL); */

/*   delete_ht(t, 69); */
/*   delete_ht(t, 65); */

/*   assert(get(t, 69) == NULL); */
/*   assert(get(t, 65) == NULL); */

/*   print(t); */

/*   free_table(&t); */

/*   assert(t == NULL); */
/* } */
