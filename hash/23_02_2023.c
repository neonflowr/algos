#include "init.h"
#include <stdio.h>
#include <stdlib.h>

#define TABLE_SIZE 100

typedef struct HashItem {
  char *key;
  int value;
} hi;

typedef struct HashTable {
  hi *items[TABLE_SIZE];
  unsigned int capacity;
} ht;

ht *create() {
  ht *t = malloc(sizeof(ht));
  t->capacity = TABLE_SIZE;
  for (int i = 0; i < t->capacity; i++) {
    t->items[i] = NULL;
  }
  return t;
}

int hash(char *key) {
  int total = 0;
  while (*key) {
    total += *key;
    key++;
  }
  return total % (TABLE_SIZE - 1);
}

int is_cell_empty(ht *t, int i) { return t->items[i] == NULL; }

int find_key_in_run(ht *t, int i, char *key) {
  while (!is_cell_empty(t, i)) {
    if (t->items[i]->key == key)
      return i;
    i++;
  }
  return -1;
}

int find_open_cell_in_run(ht *t, int i) {
  while (!is_cell_empty(t, i)) {
    i++;
  }
  return i;
}

void insert(ht *t, char *key, int value) {
  int hashed = hash(key);

  if (is_cell_empty(t, hashed)) {
    hi *item = malloc(sizeof(hi));
    item->key = key;
    item->value = value;
    t->items[hashed] = item;
  } else {
    int existing_index = find_key_in_run(t, hashed, key);
    if (existing_index != -1) {
      // Overwrite existing value
      t->items[existing_index]->value = value;
    } else {
      // Insert at end of run
      int open_cell = find_open_cell_in_run(t, hashed);
      hi *item = malloc(sizeof(hi));
      item->key = key;
      item->value = value;
      t->items[open_cell] = item;
    }
  }
}

int get(ht *t, char *key) {
  int hashed = hash(key);
  if (is_cell_empty(t, hashed)) {
    return -1;
  } else {
    int i = find_key_in_run(t, hashed, key);
    if (i == -1) {
      return -1;
    } else {
      return t->items[i]->value;
    }
  }
}

void delete_ht(ht *t, char *key) {
  int hashed = hash(key);
  if (is_cell_empty(t, hashed)) {
    return;
  } else {
    int i = find_key_in_run(t, hashed, key);
    if (i == -1)
      return;
    else {
      // Delete and rebuild run
      hi *tmp = t->items[i];
      while (!is_cell_empty(t, i)) {
        t->items[i] = t->items[i + 1];
        i++;
      }
      free(tmp);
      tmp = NULL;
    }
  }
}

void free_table(ht **t) {
  for (int i = 0; i < (*t)->capacity; i++) {
    free((*t)->items[i]);
  }
  free(*t);
  *t = NULL;
}

void print(ht *t) {
  for (int i = 0; i < t->capacity; i++) {
    printf("%d: ", i);
    if (t->items[i] != NULL)
      printf("%s-%d", t->items[i]->key, t->items[i]->value);
    printf("\n");
  }
}
