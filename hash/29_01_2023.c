#include <stdio.h>
#include <stdlib.h>

#define TABLE_SIZE 100

struct List {
  int data;
  struct List *next;
};

void insert_list(struct List **l, int value) {
  struct List *node = malloc(sizeof(struct List));
  node->data = value;
  node->next = *l;
  *l = node;
}

struct List *search_list(struct List *l, int value) {
  if (l == NULL)
	return NULL;
  if (l->data == value)
	return l;
  return search_list(l->next, value);
}

struct List *item_ahead(struct List *l, struct List *node) {
  if (l == NULL)
	return NULL;
  struct List *p = l;
  while(p->next != node) {
	p = p->next;
  }
  return p;
}

void delete_list(struct List **l, struct List **node) {
  struct List *pred = item_ahead(*l, *node);
  if (pred == NULL) {
	*l = (*node)->next;
  } else {
    pred->next = (*node)->next;
  }

  free(*node);
  *node = NULL;
}

struct HashTable {
  struct List *items[TABLE_SIZE];
  unsigned int capacity;
};

int hash(int item) {
  return item % TABLE_SIZE;
}

struct HashTable *create() {
  struct HashTable *t = malloc(sizeof(struct HashTable));
  t->capacity = TABLE_SIZE;
  return t;
}

void insert_item(struct HashTable *t, int item) {
  int key = hash(item);
  printf("Insert item %d at key %d\n", item, key);
  insert_list( &(t->items[key]), item);
}

struct List *get_item(struct HashTable *t, int item) {
  int key = hash(item);
  struct List *node = search_list(t->items[key], item);
  if (node != NULL)
	printf("Item at key %d: %d\n", key, node->data);
  else
    printf("Item %d at key %d: NULL\n", item, key);
  return node;
}

struct List *delete_item(struct HashTable *t, int item) {
  int key = hash(item);
  struct List *node = search_list(t->items[key], item);
  if (node != NULL) {
    printf("Delete item %d at key %d\n", node->data, key);
    delete_list(&(t->items[key]), &node);
    return node;
  }
  printf("Can't delete non-existent item %d at key %d\n", item, key);
  return NULL;
}

void print_list(struct List *l) {
  if (l == NULL) {
	printf("Empty list\n");
	return;
  }

  while (l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}

void print_table(struct HashTable *t) {
  for (int i = 0; i < t->capacity; i++) {
	printf("Key %d: ", i);
	print_list(t->items[i]);
  }
}

void free_table(struct HashTable *t) {
  for (int i = 0; i < t->capacity; i++) {
	free(t->items[i]);
	t->items[i] = NULL;
  }
  free(t);
  t = NULL;
}

int main() {
  printf("Test hash table\n");

  struct HashTable *t = create();


  insert_item(t, 10);
  insert_item(t, 15);
  insert_item(t, 532);


  get_item(t, 10);
  get_item(t, 15);
  get_item(t, 532);
  get_item(t, 534);

  // Test collision
  insert_item(t, 432);
  insert_item(t, 232);
  insert_item(t, 132);
  insert_item(t, 32);

  get_item(t, 432);
  get_item(t, 532);
  get_item(t, 132);
  get_item(t, 232);
  get_item(t, 32);
  get_item(t, 332);

  print_table(t);

  // Test deletion
  delete_item(t, 432);
  delete_item(t, 532);
  delete_item(t, 132);
  delete_item(t, 232);
  delete_item(t, 432);

  get_item(t, 432);
  get_item(t, 532);
  get_item(t, 132);
  get_item(t, 232);
  get_item(t, 32);
  get_item(t, 332);

  print_table(t);

  free_table(t);

  return 0;
}
