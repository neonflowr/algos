#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct HashItem {
  char *key;
  int value;
} hi;

typedef struct HashTable {
  hi *items;
  unsigned int capacity;
  int size;
} ht;

int hash(char key[]) {
  int total = 0;

  while(*key) {
    total = total + (*key);
    key++;
  }

  return total % TABLE_SIZE;
}

ht *create(unsigned int capacity) {
  ht *t = malloc(sizeof(ht));
  t->capacity = capacity;
  t->size = 0;
  t->items = malloc(capacity * sizeof(hi));
  return t;
}

void free_table(ht **t) {
  free((*t)->items);
  (*t)->items = NULL;
  free(*t);
  *t = NULL;
}

int is_cell_empty(hi *item) {
  if (item == NULL)
	return 1;
  return item->key == NULL && item->value == 0;
}

hi *find_next_open_cell(hi *cell) {
  while(!is_cell_empty(cell)) {
	cell++;
  }
  return cell;
}

hi *find_key_in_run(hi *cell, char *key) {
  while(!is_cell_empty(cell) && cell->key != key) {
	cell++;
  }

  if (is_cell_empty(cell) || cell->key != key)
	return NULL;

  return cell;
}

void insert(ht *t, char *key, int value) {
  int hashed_key = hash(key);

  hi item = {key, value};
  hi *current_cell = &t->items[hashed_key];
  if (is_cell_empty(current_cell)) {
    t->items[hashed_key] = item;
  }
  else {
    hi *existing_key = find_key_in_run(current_cell, key);
	if (!is_cell_empty(existing_key)) {
	  printf("Cannot insert existing key %s\n", key);
	  return;
	}
	hi *open_cell = find_next_open_cell(current_cell);
    *open_cell = item;
  }
  t->size++;
}

void *get(ht *t, char *key) {
  int hashed_key = hash(key);

  if (is_cell_empty(&t->items[hashed_key])) {
	return NULL;
  } else {
	if (t->items[hashed_key].key == key) {
	  return &t->items[hashed_key];
	} else {
	  return find_key_in_run(&t->items[hashed_key], key);
	}
  }
}

void delete_ht(ht *t, char *key) {
  int hashed_key = hash(key);

  hi *current_cell = &t->items[hashed_key];
  if (is_cell_empty(current_cell)) {
	return;
  } else {
	hi *item_cell = find_key_in_run(current_cell, key);
	if (item_cell) {
	  while (!is_cell_empty(item_cell + 1)) {
		item_cell->key = (item_cell + 1)->key;
		item_cell->value = (item_cell + 1)->value;
		item_cell++;
	  }
	  item_cell->key = NULL;
	  item_cell->value = 0;
	  t->size--;
	} else {
	  printf("Couldn't find key %s in run\n", key);
	  return;
	}
  }
}

void print(ht *t) {
  for (int i =0 ; i < t->capacity; i++) {
	printf("%d: %s-%d\n", i, t->items[i].key, t->items[i].value);
  }
}
