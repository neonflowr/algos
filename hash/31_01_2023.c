// またOpen Addressingでやる

#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct HashItem {
  int key;
  int value;
} hi;

typedef struct HashTable {
  hi *items;
  unsigned int capacity;
  int count;
} ht;

int hash(int item) { return item % TABLE_SIZE; }

int is_cell_empty(hi *cell) {
  if (cell == NULL)
    return 1;
  if ((*cell).value == 0 && (*cell).key == 0)
	return 1;
  return 0;
}

int find_next_open_cell(ht *t, int hashed_key) {
  // Need to wrap around the array here somehow with pointer arithmetic magic
  while (is_cell_empty(&t->items[hashed_key]) == 0)  {
    hashed_key++;
  }
  return hashed_key;
}

hi *find_key_in_run(ht *t, int hashed_key, int key) {
  while (is_cell_empty(&t->items[hashed_key]) == 0) {
	if (t->items[hashed_key].key == key) {
	  return &t->items[hashed_key];
	}
    hashed_key++;
  }
  return NULL;
}

ht *create(unsigned int capacity) {
  ht *t = malloc(sizeof(ht));
  t->items = malloc(capacity * sizeof(hi));
  t->capacity = capacity;
  t->count = 0;
  return t;
}

void insert(ht *t, int key, int value) {
  int hashed_key = hash(key);

  hi item = {key, value};

  if (is_cell_empty(&t->items[hashed_key])) {
    t->items[hashed_key] = item;
	printf("Insert key %d with value %d at %d\n", key, value, hashed_key);
  } else {
    int open_cell_index = find_next_open_cell(t, hashed_key);
    t->items[open_cell_index] = item;
    printf("Insert key %d with value %d at %d after run\n", key, value, open_cell_index);
  }
}

void *get(ht *t, int key) {
  int hashed_key = hash(key);
  if (is_cell_empty(&t->items[hashed_key])) {
	printf("Can't get %d at hash %d\n", key, hashed_key);
    return NULL;
  } else {
    if (t->items[hashed_key].key == key) {
      printf("Get key %d with value %d at hash %d\n", t->items[hashed_key].key, t->items[hashed_key].value, hashed_key);
      return &t->items[hashed_key];
	}

	hi* item = find_key_in_run(t, hashed_key + 1, key);
	if (is_cell_empty(item))
	  printf("Cannot to get %d at hash %d after run\n", key, hashed_key);
	else
	  printf("Get %d at hash %d after run\n", key, hashed_key);
	return item;
  }
}

void delete_ht(ht *t, int key) {
  int hashed_key = hash(key);

  if (is_cell_empty(&t->items[hashed_key]) == 1) {
	printf("Can't delete empty cell\n");
    return;
  } else {
	hi *cell = find_key_in_run(t, hashed_key, key);
	if (cell != NULL) {
	  printf("Delete cell with key %d and value %d at hash run %d\n", (*cell).key, (*cell).value, hashed_key);
	  // Need to rebuild the run
	  while(is_cell_empty(cell + 1) == 0) {
		(*cell).key = (*(cell + 1)).key;
		(*cell).value = (*(cell + 1)).value;
		cell++;
	  }

	  // Nullify last cell
	  (*cell).key = 0;
	  (*cell).value = 0;
	} else {
	  printf("Could not find cell with key %d in run\n", key);
	}
  }
}

void free_table(ht *t) {
  free(t->items);
  t->items = NULL;
  free(t);
  t = NULL;
}

void print(ht *t) {
  printf("Table: ");
  for (int i = 0; i < t->capacity; i++) {
	printf("%d-%d ", t->items[i].key, t->items[i].value);
  }
  printf("\n");
}
