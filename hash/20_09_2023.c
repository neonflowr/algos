// Perfect hashing

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#define MAX 11

typedef struct __hi {
	int key;
	int item;
	int start;
	int end;
	struct __hi* next;
} hi;

void print_list(hi* l) {
	while (l) {
		printf("%d ", l->item);
		l = l->next;
	}
	printf("\n");
}

int len_list(hi* l) {
	int i = 0;
	while (l) {
		i++;
		l = l->next;
	}
	return i;
}

void insert_list(hi** head, int key, int value) {
	hi* node = (hi*)malloc(sizeof(hi));
	node->key = key;
	node->item = value;
	node->start = node->end = 0;
	node->next = *head;
	*head = node;
}

typedef struct {
	hi** items;
	int capacity;
	int size;
} ht;

ht* create_table(int capacity) {
	ht* t = (ht*)malloc(sizeof(ht));
	t->capacity = capacity;
	t->size = 0;
	t->items = (hi**)malloc(sizeof(hi*) * capacity);
	for (int i = 0; i < capacity; i++) {
		t->items[i] = NULL;
	}
	return t;
}

void free_table(ht* t) {
	free(t->items);
	free(t);
}

void print_table(ht* t) {
	for (int i = 0; i < t->capacity; i++) {
		printf("%d ", i);
		if (t->items[i]) {
			printf("(%d -> %d): ", t->items[i]->start, t->items[i]->end);
		}
		else {
			printf(": ");
		}
		print_list(t->items[i]);
	}
}

int hash1(int key) {
	return key % MAX / 2;
}

int hash2(int key) {
	return key % 1124;
}

void insert1(ht* t, int key, int value) {
	int h = hash1(key);
	insert_list(&t->items[h], key, value);
	t->size++;
}

void insert2(ht* t, int key, int value, int start, int stop) {
	int h = start + (hash2(key) % (stop - start));
	insert_list(&t->items[h], key, value);
	t->size++;
}

int get(ht* t1, ht *t2, int key) {
	hi* i1 = t1->items[hash1(key)];
	if (i1 == NULL) return -1;
	int h2 = i1->start + (hash2(key) % (i1->end - i1->start));
	printf("For key %d, get item %d\n", key, t2->items[h2]->item);
	return t2->items[h2]->item;
}

int get_table_list_count(ht* t) {
	int count = 0;
	int n, n2;
	for (int i = 0; i < t->capacity; i++) {
		if (t->items[i]) {
			n = len_list(t->items[i]);
			n2 = n * n;
			t->items[i]->start = count;
			t->items[i]->end = count + n2;
			count += n2;
		}
	}
	return count;
}

int main() {
	ht* t = create_table(MAX);
	insert1(t, 124, 1);
	insert1(t, 125, 2);
	insert1(t, 126, 3);
	insert1(t, 121, 4);
	insert1(t, 58, 19);
	insert1(t, 69, 123);
	insert1(t, 23, 69);
	insert1(t, 34, 111);
	insert1(t, 46, 420);
	insert1(t, 19, 20);
	insert1(t, 20, 14);

	printf("First table\n");
	print_table(t);

	int n = get_table_list_count(t);
	printf("N = %d\n", n);
	if (n < 4 * MAX) {
		printf("Lists take linear space\n");
	}
	else {
		printf("Lists take quadratic space\n");
	}

	ht* t2 = create_table(n);
	for (int i = 0; i < t->capacity; i++) {
		if (!t->items[i]) continue;
		hi* item = t->items[i];
		int start = item->start;
		int end = item->end;
		while (item) {
			insert2(t2, item->key, item->item, start, end);
			item = item->next;
		}
	}

	printf("Second table\n");
	print_table(t2);

	assert(get(t, t2, 124) == 1);
	assert(get(t, t2, 20) == 14);
	assert(get(t, t2, 69) == 123);
	assert(get(t, t2, 46) == 420);

	for (int i = 0; i < t2->capacity; i++) { 
		if (t2->items[i]) {
			assert(len_list(t2->items[i]) == 1);
		}
	}

	free_table(t);
	free_table(t2);

	return 0;
}

