#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
	int value;
	int key;
	struct List* next;
} list;

void insertl(list** head, int value, int key) {
	list* node = malloc(sizeof(list));
	node->value = value;
	node->key = key;
	node->next = *head;
	*head = node;
}


list* item_ahead(list* l, int value) {
	if (l == NULL) return NULL;
	while (l != NULL) {
		if (l->next && l->next->key == value) {
			break;
		}
		l = l->next;
	}
	return l;
}

void deletel(list** l, int value) {
	list* tmp;
	list* ahead = item_ahead(*l, value);

	if (!ahead) {
		tmp = *l;
		*l = *l->next;
		free(tmp);
		tmp = NULL;
	}
	else {
		tmp = ahead->next;
		ahead->next = tmp->next;
		free(tmp);
		tmp = NULL;
	}
}

list* search(list* l, int key) {
	if (!l)
		return;
	if (l->key == key)
		return l;
	return search(l->next, key);
}

typedef struct HashTable {
	list* items[TABLE_SIZE];
	unsigned int capacity;
	unsigned int size;
} table;

int hash(int value) {
	return value % TABLE_SIZE;
}

table* create() {
	table* t = malloc(sizeof(table));
	for (int i = 0; i < TABLE_SIZE; i++) {
		t->items[i] = NULL;
	}
	t->size = 0;
	t->capacity = TABLE_SIZE;
	return t;
}

void insert(table* t, int key, int value) {
	int hashed_key = hash(key);

	list* existing = search(&t->items[hashed_key], key);
	if (existing == NULL) {
		insertl(&t->items[hashed_key], value, key);
	}
	else {
		existing->value = value;
	}
}

int get(table* t, int key) {
	int hashed_key = hash(key);
	list* item = search(&t->items[hashed_key], key);
	return item ? item->value : -1;
}


void delete(table* t, int value) {
	int hashed_key = hash(key);
	deletel(&t->items[hashed_key], key);
}

