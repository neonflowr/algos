#include <stdio.h>
#include <stdlib.h>
#include "init.h"
#include <limits.h>

#define TABLE_SIZE 100

typedef struct HashItem {
	int key;
	int value;
} hi;

typedef struct HashTable {
	hi items[TABLE_SIZE];
	int capacity;
} table;

table* create() {
	table* t = malloc(sizeof(table));
	for (int i = 0; i < TABLE_SIZE; i++) {
		t->items[i].key = INT_MIN;
		t->items[i].value = INT_MIN;
	}
	t->capacity = TABLE_SIZE;
	return t;
}

int hash(int key) {
	return key % TABLE_SIZE;
}

int rehash(int key) {
	return key + 1;
}

int is_cell_empty(table* t, int cell) {
	return t->items[cell].key == INT_MIN;

}

void insert_subroutine(table* t, int hashed, int key, int value) {
	if (is_cell_empty(t, hashed)) {
		t->items[hashed].key = key;
		t->items[hashed].value = value;
	}
	else {
		if (t->items[hashed].key == key) {
			t->items[hashed].value = value;
		}
		else {
			insert_subroutine(t, rehash(hashed), key, value);
		}
	}
}

void insert(table *t, int key, int value) {
	int hashed = hash(key);
	insert_subroutine(t, hashed, key, value);
}

int get_subroutine(table* t, int hash, int key, int value) {
	if (is_cell_empty(t, hashed))
		return INT_MIN;
	if (t->items[hashed].key == key) {
		return t->items[hashed].value;
	}
	return get_subroutine(t, rehash(hash), key, value);
}

int get(table *t, int key, int value) {
	int hashed = hash(key);
	return get_subroutine(t, hashed, key, value);
}

void delete_subroutine(table* t, int hashed, int key, int value) {
	if (is_cell_empty(t, hashed))
		return;

	if (t->items[hashed].key == key) {
		// delete and rebuild
		int i = hashed;
		while (!is_cell_empty(t, i)) {
			t->items[i].key = t->items[i + 1].key;
			t->items[i].value = t->items[i + 1].value;
			i++;
		}
	}
	else {
		delete_subroutine(t, rehash(hashed), key, value);
	}
}

void delete(table* t, int key, int value) {
	int hashed = hash(key);

	delete_subroutine(t, hashed, key, value);
}
