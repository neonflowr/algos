// Open Addressingでやる

#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct HashTable {
  int *items;
  unsigned int capacity;
} hb;

int hash(int item) { return item % TABLE_SIZE; }

hb *create(unsigned int capacity) {
  hb *t = malloc(sizeof(capacity));
  t->items = malloc(capacity * sizeof(int));
  t->capacity = capacity;
  return t;
}

void insert(hb *t, int item) {
  int hashed_key = hash(item);
  if (t->items[hashed_key] != 0) {
    // Find the next available empty cell
    hashed_key++;
    while (t->items[hashed_key] != 0) {
      hashed_key++;
    }
    t->items[hashed_key] = item;
  } else {
    t->items[hashed_key] = item;
  }
  printf("Insert item %d with key %d\n", item, hashed_key);
}

void *get(hb *t, int item) {
  int hashed_key = hash(item);
  if (t->items[hashed_key] != 0) {
    if (t->items[hashed_key] == item) {
      printf("Get item %d at key %d\n", item, hashed_key);
      return &t->items[hashed_key];
    } else {
      // Go through the run following the cell
      int i = hashed_key + 1;
      while (t->items[i] != 0) {
        if (t->items[i] == item) {
          printf("Get item %d at key %d after run\n", item, hashed_key);
          return &t->items[i];
        }
        i++;
      }
    }
  }
  printf("Could not get item %d at key %d\n", item, hashed_key);
  return NULL;
}

void delete_cell_and_reorder_run(hb *t, int *cell) {
  while (*(cell + 1) != 0) {
    *cell = *(cell + 1);
    cell++;
  }
  // Delete the trailing cell
  *cell = 0;
}

void delete_ht(hb *t, int item) {
  int hashed_key = hash(item);

  int *cell = (int *)get(t, item);
  if (cell != NULL) {
    printf("Delete item %d at key %d\n", item, hashed_key);
    delete_cell_and_reorder_run(t, cell);
  }
}

void free_table(hb *t) {
  free(t->items);
  t->items = NULL;
  free(t);
  t = NULL;
}

void print(hb *t) {
  printf("Table: ");
  for (int i = 0; i < t->capacity; i++) {
    printf("%d ", t->items[i]);
  }
  printf("\n");
}
