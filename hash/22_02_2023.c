#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  char *key;
  int value;
  struct List *next;
} list;

void insert_list(list **head, char *key, int value) {
  list *l = malloc(sizeof(list));
  l->key = key;
  l->value = value;
  l->next = *head;
  *head = l;
}

list *search(list *l, char *key) {
  if (l == NULL)
    return NULL;
  if (l->key == key)
    return l;
  return search(l->next, key);
}

list *item_ahead(list *l, list *x) {
  if (l == NULL)
    return NULL;
  while (l != NULL && l->next != x) {
    l = l->next;
  }
  return l;
}

void delete_list(list **head, list **x) {
  list *tmp = *head;
  list *pred = item_ahead(*head, *x);
  if (pred == NULL) {
    *head = tmp->next;
  } else {
    pred->next = (*x)->next;
  }
  free(*x);
  *x = NULL;
}

void free_list(list *l) {
  if (l == NULL)
    return;
  list *tmp = l->next;
  free(l);
  l = NULL;
  free_list(tmp);
}

typedef struct HashTable {
  list *items[TABLE_SIZE];
  int size;
  unsigned int capacity;
} hb;

hb *create(unsigned int capacity) {
  hb *t = malloc(sizeof(hb));
  for (int i = 0; i < capacity; i++) {
    t->items[i] = NULL;
  }
  t->size = 0;
  t->capacity = capacity;
  return t;
}

void free_table(hb **t) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    free_list((*t)->items[i]);
  }
  free(*t);
  *t = NULL;
}

int hash(char *key) {
  int val = 0;
  while (*key) {
    val = val + *key;
    key++;
  }
  return val & (TABLE_SIZE - 1);
}

void insert(hb *t, char *key, int value) {
  int hashed = hash(key);
  list *existing_node = search(t->items[hashed], key);
  if (existing_node != NULL) {
    existing_node->value = value;
  } else {
    insert_list(&t->items[hashed], key, value);
  }
}

int get(hb *t, char *key) {
  int hashed = hash(key);
  list *node = search(t->items[hashed], key);
  if (node == NULL)
    return -1;
  else {
    return node->value;
  }
}

void delete_ht(hb *t, char *key) {
  int hashed = hash(key);
  list *node = search(t->items[hashed], key);
  if (node == NULL)
    return;
  delete_list(&t->items[hashed], &node);
}

void print_list(list *l) {
  while(l != NULL) {
	printf("%s-%d ", l->key, l->value);
	l = l->next;
  }
}

void print(hb *t) {
  printf("Table:\n");
  printf("%d\n", t->capacity);
  for (int i = 0; i < TABLE_SIZE; i++) {
	printf("%d: ", i);
	print_list(t->items[i]);
	printf("\n");
  }
}
