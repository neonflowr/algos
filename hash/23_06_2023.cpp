#define SIZE 100

class list {
private:
  list* item_ahead(int x) {
    list *p = this;
    while(p->next && p->next->key != x) {
      p = p->next;
    }
    return p;
  }

public:
  int key = 0;
  int value = 0;
  list *next = nullptr;

  list(int key, int value, list *next) {
    this->key = key;
    this->value = value;
    this->next = next;
  }

  static void insert(list **head, int key, int value) {
    list *node = new list(key, value, *head);
    *head = node;
  }

  list *search(int x) {
    list *p = this;
    while(p) {
      if (p->key == x)
        return p;
      p = p->next;
    }
    return nullptr;
  }

  static void delete_val(list **head, int x) {
    list *prev;
    list *tmp = *head;
    prev = tmp->item_ahead(x);
    if (prev == nullptr) {
      *head = tmp->next;
      delete tmp;
    } else {
      tmp = prev->next;
      prev->next = tmp->next;
      delete tmp;
    }
  }
};

class table {
private:

public:
  const static int capacity = SIZE;

  list *items[SIZE] = { nullptr };
  int size = 0;

  static int get_hash(int key) { return key % table::capacity; }

  void insert(int key, int value) {
    int hashed = table::get_hash(key);
    list::insert(&items[hashed], key, value);
  }

  bool get(int key, int *result) {
    int hashed = table::get_hash(key);

    list *node = items[hashed]->search(key);
    if (node) {
      *result = node->value;
      return true;
    }
    return false;
  }

  void delete_key(int key) {
    int hashed = table::get_hash(key);
    list *node = items[hashed]->search(key);
    if (node) {
      items[hashed]->delete_val(&items[hashed], key);
    }
  }
};
