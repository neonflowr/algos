#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int key;
  int value;
  struct List *next;
} list;

void insert_list(list **head, int key, int value) {
  list *l = malloc(sizeof(list));
  l->key = key;
  l->value = value;
  l->next = *head;
  *head = l;
}

list *search(list *l, int key) {
  if (l == NULL)
	return NULL;
  if (l->key == key)
	return l;
  return search(l->next, key);
}

list *item_ahead(list *l, list *x) {
  if (l == NULL)
	return NULL;
  while (l != NULL && l->next != x) {
	l = l->next;
  }
  return l;
}

void delete(list **head, list **x) {
  list *head_tmp = *head;
  list *pred = item_ahead(*head, *x);
  if (pred == NULL) {
	*head = (*head)->next;
	free(head_tmp);
	head_tmp = NULL;
  } else {
    pred->next = (*x)->next;
    free(*x);
    *x = NULL;
  }
}

void free_list(list **l) {
  list *current = *l;
  while(current != NULL) {
    list *tmp = current;
	current = current->next;
	free(tmp);
	tmp = NULL;
  }
}

typedef struct HashTable {
  list **items;
  unsigned int capacity;
  int size;
} ht;

int hash(int key) {
  return key % 100;
}

ht *create(unsigned int capacity) {
  ht *t = malloc(sizeof(ht));

  t->items = malloc(capacity * sizeof(list *));

  for (int i = 0; i < t->capacity; i++) {
	t->items[i] = NULL;
  }

  t->capacity = capacity;
  t->size = 0;
  return t;
}

void free_table(ht **t) {
  for (int i = 0; i < (*t)->capacity; i++) {
	free_list(&(*t)->items[i]);
  }
  free(*t);
  *t = NULL;
}

void insert(ht *t, int key, int value) {
  int hashed_key = hash(key);
  insert_list(&t->items[hashed_key], key, value);
}

void *get(ht *t, int key) {
  int hashed_key = hash(key);

  list *node = search(t->items[hashed_key], key);
  if (node != NULL) {
	printf("Return node with key %d value %d\n", node->key, node->value);
    return node;
  }

  printf("Cannot get key %d\n", key);

  return NULL;
}

void delete_ht(ht *t, int key) {
  int hashed_key = hash(key);
  list *node = search(t->items[hashed_key], key);
  if (node == NULL) return;
  delete(&t->items[hashed_key], &node);
}

void print_list(list *l) {
  while(l != NULL) {
	if (l->next != NULL)
	  printf("%d-%d -> ", l->key, l->value);
	else
	  printf("%d-%d\n", l->key, l->value);
	l = l->next;
  }
}

void print(ht *t) {
  printf("Table:\n");
  for (int i = 0; i < t->capacity; i++) {
	printf("%d: ", i);
	print_list(t->items[i]);
	printf("\n");
  }
  printf("\n");
}
