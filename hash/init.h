#define TABLE_SIZE 100

struct HashItem;
struct HashTable;

int hash();
struct HashTable *create();
void insert();
void delete_ht();
void free_table();
int get();
void *successor();
void *predecessor();
void *min();
void *max();
void print();
