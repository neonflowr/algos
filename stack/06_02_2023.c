#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct Stack {
  int *items;
  int top;
  unsigned int capacity;
} stack;

stack *create(unsigned int capacity) {
  stack *s = malloc(sizeof(capacity));
  s->items = malloc(capacity * sizeof(int));
  s->top = -1;
  s->capacity = capacity;
  return s;
}

int is_full(stack *s) {
  return s->top == s->capacity - 1;
}

int is_empty(stack *s) {
  return s->top == -1;
}

void push(stack *s, int value) {
  if (is_full(s))
	return;
  s->items[++s->top] = value;
}

int pop(stack *s) {
  if (is_empty(s))
	return -1;

  return s->items[s->top--];
}

void print(stack *s) {
  printf("Stack: ");
  for (int i = 0; i < s->top; i++) {
    printf("%d ", s->items[i]);
  }
  printf("\n");
}

int top(stack *s) {
  if (is_empty(s))
	return -1;
  return s->items[s->top];
}
