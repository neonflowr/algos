#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
	int data;
	struct List* next;
} list;

void insertl(list** head, int val) {
	list* node = malloc(sizeof(list));
	node->data = val;
	node->next = *head;
	*head = node;
}

void printl(list* l) {
	while (l) {
		printf("%d ", l->data);
		l = l->next;
	}
	printf("\n");
}

typedef struct Stack {
	list* top;
} stack;

struct Stack* create() {
	stack* s = malloc(sizeof(stack));
	s->top = NULL;
	return s;
}

void push(stack *s, int val) {
	insertl(s->top, val);
}

int pop(stack *s) {
	list *node = s->top;
	int val = node->data;
	s->top = node->next;
	free(node);
	return val;
}

void print(struct Stack* s) {
	printl(s->top);
}

int top(struct Stack *s) {
	return s->top ? s->top->data : NULL;
}

