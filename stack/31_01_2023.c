#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **head, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;
  node->next = *head;
  *head = node;
}

typedef struct Stack {
  list *items;
} stack;

stack *create() {
  stack *s = malloc(sizeof(stack));
  s->items = NULL;
  return s;
}

void push(stack *s, int value) {
  insert(&s->items, value);
}

int pop(stack *s) {
  list *tmp = s->items;
  s->items = s->items->next;
  int val = tmp->data;
  free(tmp);
  tmp = NULL;
  return val;
}

int top(stack *s) {
  if (s->items == NULL)
	return -1;
  return s->items->data;
}

void print(stack *s) {
  list *p = s->items;
  while(p != NULL) {
	printf("%d ", p->data);
	p = p->next;
  }
  printf("\n");
}
