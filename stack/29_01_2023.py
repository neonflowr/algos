class Stack:
    top: int = -1
    items: list = []

    def push(self, value: int) -> None:
        self.top += 1
        self.items.append(value)

    def pop(self) -> int:
        if self.top == -1:
            return None
        val: int =  self.items[self.top]
        self.top -= 1
        return val

    def __str__(self) -> str:
        string: str = ""
        for i in range(0, self.top + 1):
            string = string + str(self.items[i]) + " "
        return string

stack: Stack = Stack()
stack.push(1)
stack.push(2)
stack.push(3)

print(str(stack))
stack.pop()
stack.pop()

print(str(stack))
