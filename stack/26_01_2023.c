#include <stdio.h>
#include <stdlib.h>

struct Stack {
  int *items;
  int top;
  unsigned int capacity;
};

struct Stack *create_stack(unsigned int capacity) {
  struct Stack *s = malloc(sizeof(struct Stack));
  s->items = (int *)malloc(capacity * sizeof(int));
  s->top = -1;
  s->capacity = capacity;
  return s;
}

void push(struct Stack *s, int value) {
  if (s->top == s->capacity - 1) {
	printf("Can't push to full stack\n");
    return;
  }
  s->items[++s->top] = value;
}

int pop(struct Stack *s) {
  if (s->top == -1) {
	printf("Can't pop empty stack\n");
    return -1;
  }

  int val = s->items[s->top--];
  printf("Pop %d\n", val);

  return val;
}

void print_stack(struct Stack *s) {
  if (s->top == -1) {
	printf("stack is empty\n");
	return;
  }

  printf("Stack: ");
  for (int i = 0; i <= s->top; i++) {
	printf("%d ", s->items[i]);
  }
  printf("\n");
}

int main(int argc, char** argv) {
  struct Stack *s = create_stack(4);
  printf("Init stack\n");
  push(s, 1);
  push(s, 2);
  push(s, 3);
  push(s, 4);
  push(s, 5);
  print_stack(s);

  printf("Pop 3 items\n");
  pop(s);
  pop(s);
  pop(s);
  print_stack(s);

  printf("Pop to empty\n");
  pop(s);
  pop(s);
  pop(s);
  print_stack(s);

  free(s);
  s = NULL;

  return 0;
}
