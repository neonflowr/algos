#include <stdio.h>
#include <stdlib.h>

typedef struct list {
  int data;
  struct list *next;
} list;

typedef struct stack {
  list **head;
} stack;

// Linked list operations
list *search(list *l, int item);
void insert(list **head, int item);
void delete_by_value(list **head, int item);
void delete_by_node(list **head, list **x);
list *item_ahead(list *l, list *node_to_find);
void print_list(list *l);

// Stack operations
void push(stack *items, int item);
list *pop(stack *items);
void print_stack(stack *s);

int main() {
  list start_node = {1, NULL};
  list *p_start_node = &start_node;

  list **head = &p_start_node;

  stack s = {head};
  stack *ps = &s;

  printf("Starting stack\n");
  print_stack(ps);

  printf("Push 2 and 3\n");
  push(ps, 2);
  push(ps, 3);
  print_stack(ps);

  printf("Pop: %d\n", pop(ps)->data);
  printf("Pop: %d\n", pop(ps)->data);
  printf("Pop: %d\n", pop(ps)->data);
  pop(ps);

  printf("Push again\n");
  push(ps, 69);
  push(ps, 780);
  push(ps, 12);

  printf("final stack\n");
  print_stack(ps);

  return 0;
}

list *search(list *l, int item) {
  if (l->next == NULL) {
    return NULL;
  }

  if (l->data == item) {
    return l;
  } else {
    return search(l->next, item);
  }
}

// Insert item at head of the list
void insert(list **head, int item) {
  list *node = malloc(sizeof(list));
  node->data = item;
  node->next = *head;

  // Change the head to point at the new node
  *head = node;
}

list *item_ahead(list *l, list *node_to_find) {
  if ((l == NULL) || (l->next == NULL)) {
    return NULL;
  }

  if (l->next == node_to_find) {
    return l;
  } else {
    return item_ahead(l->next, node_to_find);
  }
}

void delete_by_value(list **head, int item) {
  // Need to get the item before the item being deleted somehow
  list *node_to_delete = search(*head, item);

  list *preceding_node = item_ahead(*head, node_to_delete);

  if (preceding_node == NULL) { // node to find is at the top of the linked list
    *head = node_to_delete->next;
  } else {
    preceding_node->next = node_to_delete->next;
  }

  free(node_to_delete);
}

void delete_by_node(list **head, list **x) {
  list *p;
  list *pre;

  p = *head;

  pre = item_ahead(p, *x);

  if (pre == NULL) { // node to find is at the top of the linked list
    *head = p->next;
  } else {
    pre->next = (*x)->next;
  }

  free(*x);
  x = NULL;
}

void print_list(list *l) {
  if (l == NULL) {
    printf("\n");
    return;
  }
  if (l->next == NULL) {
    printf("%d\n", l->data);
    return;
  } else {
    printf("%d -> ", l->data);
    print_list(l->next);
  }
}

void push(stack *s, int item) { insert(s->head, item); }

list *pop(stack *s) {
  if (*s->head == NULL) {
    printf("cannot pop empty stack\n");
    return NULL;
  }

  list *tmp = *(s->head);
  if ((*s->head)->next == NULL) {
    *s->head = NULL;
    return tmp;
  }
  *(s->head) = (*s->head)->next;
  return tmp;
}

void print_stack(stack *s) { print_list(*s->head); }
