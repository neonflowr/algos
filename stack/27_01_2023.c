#include <stdio.h>
#include <stdlib.h>


struct List {
  int data;
  struct List *next;
};

struct Stack {
  struct List *top;
};

void push(struct Stack *s, int value) {
  struct List *node = malloc(sizeof(struct List));
  node->data = value;
  node->next = s->top;
  s->top = node;
  printf("Push %d\n", value);
}

int pop(struct Stack *s) {
  if (s->top == NULL) {
	printf("Can't pop empty stack\n");
	return -1;
  }

  struct List *tmp = s->top;
  int val = s->top->data;

  s->top = s->top->next;

  free(tmp);
  tmp = NULL;

  printf("Pop %d\n", val);

  return val;
}

void print_stack(struct Stack *s) {
  if (s->top == NULL) {
	printf("empty stack\n");
	return;
  }

  struct List *node = s->top;
  while(node != NULL) {
	printf("%d ", node->data);
	node = node->next;
  }
  printf("\n");
}

int main() {
  struct Stack *s = malloc(sizeof(struct Stack));
  s->top = NULL;
  push(s, 1);
  push(s, 2);
  push(s, 3);
  print_stack(s);
  printf("Top: %d\n", s->top->data);

  pop(s);
  printf("Top: %d\n", s->top->data);
  pop(s);
  printf("Top: %d\n", s->top->data);
  print_stack(s);

  pop(s);
  print_stack(s);

  pop(s);
  pop(s);
  pop(s);
  pop(s);


  push(s, 4);
  push(s, 5);
  push(s, 6);
  print_stack(s);
  printf("Top: %d\n", s->top->data);

  pop(s);
  print_stack(s);

  return 1;
}
