#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "init.h"
#include "10_03_2023.c"


int main(int argc, char *argv[]) {
  struct Stack *s = create(14);

  push(s, 1);
  push(s, 2);
  push(s, 3);
  push(s, 5);

  assert(top(s) == 5);

  print(s);

  assert(pop(s) == 5);
  assert(pop(s) == 3);

  print(s);

  assert(pop(s) == 2);
  assert(pop(s) == 1);

  // assert(s->items == NULL);

  print(s);

  push(s, 1);
  push(s, 2);

  assert(top(s) == 2);
  print(s);

  free(s->items);
  free(s);
  s = NULL;

  return 0;
}

