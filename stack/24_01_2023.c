#include <stdio.h>
#include <stdlib.h>

typedef struct list {
  int data;
  struct list* next;
} list;

typedef struct stack {
  list* top;
} stack;

void push(stack* s, int value) {
  list* node = malloc(sizeof(list));
  node->data = value;
  if (s->top == NULL) {
    node->next = NULL;
    s->top = node;
    return;
  }
  node->next = s->top;
  s->top = node;
}

int pop(stack *s) {
  if (s->top == NULL) {
	printf("Can't pop empty stack\n");
	return -1;
  }
  list* tmp = s->top;

  s->top = s->top->next;

  int val = tmp->data;
  free(tmp);

  return val;
}

void print_stack(stack *s) {
  if (s->top == NULL) {
	printf("Empty stack\n");
	return;
  }

  list* node = s->top;
  while(node != NULL) {
	if (node->next != NULL)
	  printf("%d -> ", node->data);
	else
	  printf("%d", node->data);

        node = node->next;
  }
  printf("\n");
}

int main(int argc, char** argv) {
  stack* s = malloc(sizeof(stack));
  printf("Initial stack: \n");
  print_stack(s);

  printf("Push some times\n");
  push(s, 1);
  push(s, 2);
  push(s, 3);
  print_stack(s);

  printf("Pop 2 items\n");
  printf("Popped: %d\n", pop(s));
  printf("Popped: %d\n", pop(s));
  print_stack(s);

  printf("Pop to empty\n");
  pop(s);
  print_stack(s);

  printf("Attempt to pop\n");
  pop(s);

  printf("Push again\n");
  push(s, 1);
  push(s, 2);
  push(s, 3);
  print_stack(s);

  return 0;
}
