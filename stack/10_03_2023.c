#include <stdio.h>
#include "init.h"
#include <stdlib.h>

typedef struct Stack {
	int* items;
	int top;
	int size;
	int capacity;
} stack;

stack* create(int capacity) {
	stack* s = malloc(sizeof(stack));
	s->items = calloc(capacity, sizeof(int));
	s->top = -1;
	s->size = 0;
	s->capacity = capacity;
	return s;
}

int is_full(stack* s) {
	return s->size == s->capacity;
}
int is_empty(stack* s) {
	return s->size == 0;
}


void push(stack* s, int value) {
	if (is_full(s)) return;
	s->items[++s->top] = value;
	s->size++;
}

int pop(stack* s) {
	if (is_empty(s)) return -1;
	int val = s->items[s->top--];
	s->size--;
	return val;
}

int top(stack* s) {
	if (is_empty(s)) return -1;
	return  s->items[s->top];
}

void print(stack* s) {
	for (int i = 0; i <= s->top; i++) {
		printf("%d ", s->items[i]);
	}
	printf("\n");
}
