struct Stack;
struct Stack *create();
void push();
int pop();
void print(struct Stack *s);
int top();
