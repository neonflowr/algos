#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

typedef struct Stack {
  list *items;
} stack;

void insert(list **l, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;
  node->next = *l;
  *l = node;
}

stack *create() {
  stack *s = malloc(sizeof(stack));
  s->items = NULL;
  return s;
}

void push(stack *s, int value) {
  insert(&s->items, value);
}

int top(stack *s) {
  if (s->items == NULL) return -1;
  return s->items->data;
}

int pop(stack *s) {
  if(s->items == NULL) return -1;
  list *node = s->items;
  s->items = node->next;
  int val = node->data;
  free(node);
  node = NULL;
  return val;
}

void print(stack *s) {
  list *l = s->items;
  while (l != NULL) {
	printf("%d ", l->data);
	l = l->next;
  }
  printf("\n");
}
