#include <stdio.h>
#include <stdlib.h>

typedef struct stack {
  int size;
  int current_index;
  int data[4];
} stack;

void push(stack *items, int item);
int pop(stack *items);
void print_stack(stack *items);

int main() {
  printf("Example\n");

  struct stack example = {4, 0, {0}};
  struct stack *p_example = &example;

  // Add values
  push(p_example, 1);
  push(p_example, 2);
  push(p_example, 4);
  push(p_example, 3);

  printf("Full stack: \n");
  print_stack(p_example);

  // Popping
  pop(p_example);
  pop(p_example);
  pop(p_example);

  printf("Popped stack: \n");
  print_stack(p_example);

  // Add again
  push(p_example, 69);

  printf("Final stack: \n");
  print_stack(p_example);

  p_example = NULL;
}

void push(stack *items, int item) {
  if (items->current_index == items->size) {
	printf("max stack size reached\n");
	return;
  }
  (items->data)[items->current_index] = item;
  items->current_index++;
  return;
}

int pop(stack *items) {
  if (items->current_index == 0) {
	printf("Stack is empty\n");
	return -1;
  }
  items->current_index--;
  int tmp = (items->data)[items->current_index];
  (items->data)[items->current_index] = 0;
  return tmp;
}

void print_stack(stack *items) {
  for (int i = 0; i < items->current_index; i++) {
	printf("%d ", (items->data)[i]);
  }
  printf("\n");
}
