#include <stdio.h>
#include <stdlib.h>

typedef struct stack {
  int *items;
  int top;
  int size;
} stack;

void push(stack *s, int value) {
  if (s->top == s->size) {
    printf("Stack is full\n");
    return;
  }

  printf("Push %d\n", value);

  s->items[s->top] = value;
  s->top++;
}

int pop(stack *s) {
  if (s->top == 0) {
    printf("Can't pop empty stack\n");
    return -1;
  }

  s->top--;

  printf("Pop %d\n", s->items[s->top]);

  return s->items[s->top];
}

void print_stack(stack *s) {
  printf("Stack: ");
  for (int i = 0; i < s->top; i++) {
    printf("%d ", s->items[i]);
  }
  printf("\n");
}

int main() {
  stack *s = malloc(sizeof(stack));
  int items[4] = {0};

  s->items = &items[0];
  s->top = 0;
  s->size = 4;

  push(s, 1);
  push(s, 2);
  push(s, 4);

  printf("Init stack\n");
  print_stack(s);

  pop(s);
  pop(s);
  pop(s);
  pop(s);

  print_stack(s);

  push(s, 69);
  push(s, 420);
  push(s, 322);
  push(s, 622);
  push(s, 233);
  push(s, 124);

  print_stack(s);

  pop(s);
  pop(s);
  pop(s);

  print_stack(s);

  return 0;
}
