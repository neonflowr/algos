#include <stdio.h>
#include <stdlib.h>
#include "init.h"

struct Stack {
  int *items;
  int top;
  unsigned int capacity;
};

struct Stack *create(unsigned int capacity) {
  struct Stack *s = malloc(sizeof(struct Stack));
  s->items = malloc(capacity * sizeof(int));
  s->top = -1;
  s->capacity = capacity;
  return s;
}

void push(struct Stack *s, int value) {
  if (s->top == s->capacity - 1)
	return;
  s->items[++s->top] = value;
}

int pop(struct Stack *s) {
  if (s->top == -1)
	return -1;

  return s->items[s->top--];
}

void print(struct Stack *s) {
  for (int i = 0; i <= s->top; i++) {
	printf("%d ", s->items[i]);
  }
  printf("\n");
}
