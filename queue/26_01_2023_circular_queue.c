#include <stdio.h>
#include <stdlib.h>

struct Queue {
  int *items;
  int front;
  int back;
  unsigned capacity;
  int size;
};

void enqueue(struct Queue *q, int value) {
  if (q->size == q->capacity) {
    printf("Queue is full\n");
    return;
  }

  q->back = (q->back + 1) % q->capacity;
  q->items[q->back] = value;
  q->size++;
}

int dequeue(struct Queue *q) {
  if (q->size == 0) {
    printf("Cannot dequeue. Queue is empty\n");
    return -1;
  }

  int value = q->items[q->front];

  q->front = (q->front + 1) % q->capacity;
  q->size--;

  return value;
}

struct Queue *create(unsigned capacity) {
  struct Queue *q = malloc(sizeof(struct Queue));
  q->items = (int *)malloc(capacity * sizeof(int));
  q->front = q->size = 0;
  q->back = capacity - 1;
  q->capacity = capacity;
  return q;
}

void print_queue(struct Queue *q) {
  if (q->size == 0) {
    printf("Queue is empty\n");
    return;
  }

  int i = q->front;
  while (i != q->back) {
    printf("%d -> ", q->items[i]);
    i = (i + 1) % q->capacity;
  }
  printf("%d", q->items[q->back]);
  printf("\n");
}

int main() {
  struct Queue *q = create(4);

  enqueue(q, 1);
  enqueue(q, 3);
  enqueue(q, 4);
  enqueue(q, 9);

  printf("Testing\n");
  print_queue(q);

  printf("Dequeue twice\n");
  dequeue(q);
  dequeue(q);
  print_queue(q);

  printf("Dequeue to empty\n");
  dequeue(q);
  dequeue(q);
  dequeue(q);
  dequeue(q);
  print_queue(q);

  printf("Re-init\n");
  enqueue(q, 1);
  enqueue(q, 3);
  enqueue(q, 4);
  enqueue(q, 9);
  print_queue(q);

  return 0;
}
