#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

typedef struct Queue {
  list *front;
  list *back;
} queue;

queue *create() {
  queue *q = malloc(sizeof(queue));
  q->front = NULL;
  q->back = NULL;
  return q;
}

void free_queue(queue *q) {
  if (q->front != NULL) {
    list *current = q->front;
    while (current != NULL) {
      list *tmp = current;
      current = current->next;
      free(tmp);
      tmp = NULL;
    }
  }
  free(q);
  q = NULL;
}

void enqueue(queue *q, int value) {
  list *l = malloc(sizeof(list));
  l->data = value;
  l->next = NULL;
  if (q->front == NULL) {
    q->back = q->front = l;
  } else {
    q->back->next = l;
    q->back = l;
  }
}

int dequeue(queue *q) {
  if (q->front == NULL)
    return -1;

  list *tmp = q->front;
  q->front = q->front->next;
  int val = tmp->data;
  free(tmp);
  tmp = NULL;

  return val;
}

void print(queue *q) {
  if (q->front == NULL) {
    printf("Empty queue\n");
    return;
  }
  list *tmp = q->front;
  while (tmp != NULL) {
    printf("%d ", tmp->data);
    tmp = tmp->next;
  }
  printf("\n");
}
