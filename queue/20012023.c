// Postmortem: Seems like tracking the start and end of the queue within the queue struct itself is the way to go
// for implemetation


#include <stdio.h>
#include <stdlib.h>


// LL

typedef struct list {
  int data;
  struct list *next;
} list;

list *search(list *l, int item) {
  if (l->next == NULL) {
    return NULL;
  }

  if (l->data == item) {
    return l;
  } else {
    return search(l->next, item);
  }
}

// Insert item at head of the list
void insert(list **head, int item) {
  list *node = malloc(sizeof(list));
  node->data = item;
  node->next = *head;

  // Change the head to point at the new node
  *head = node;
}

list *item_ahead(list *l, list *node_to_find) {
  if ((l == NULL) || (l->next == NULL)) {
    return NULL;
  }

  if (l->next == node_to_find) {
    return l;
  } else {
    return item_ahead(l->next, node_to_find);
  }
}

void delete(list **head, list **x) {
  list *p;
  list *pre;

  p = *head;

  pre = item_ahead(p, *x);

  if (pre == NULL) { // node to find is at the top of the linked list
    *head = p->next;
  } else {
    pre->next = (*x)->next;
  }

  free(*x);
  x = NULL;
}


void print_list(list *l) {
  if (l == NULL) {
	printf("Empty list\n");
	return;
  }

  if (l->next == NULL) {
    printf("%d\n", l->data);
    return;
  } else {
    printf("%d -> ", l->data);
    print_list(l->next);
  }
}

// LL

// Q

typedef struct queue {
  list* items;
} queue;

void enqueue(queue* q, int val) {
  insert(&q->items, val);
}

int dequeue(queue* q) {
  if (q->items == NULL) {
	printf("Queue is empty\n");
	return 0;
  }

  list* node = q->items;

  list* pred;

  while(node->next != NULL) {
	pred = node;
	node = node->next;
  }

  int val;
  if (pred != NULL) {
    pred->next = NULL;
    val = node->data;
    free(node);
    node = NULL;
  } else {
	val = q->items->data;
	free(q->items);
	q->items = NULL;
  }

  return val;
}

void print_queue(queue* q) {
  print_list(q->items);
}


// Q

int main() {
  list* l = NULL;

  queue* q = malloc(sizeof(queue));
  q->items = l;

  // 3 2 1 queue
  enqueue(q, 3);
  enqueue(q, 2);
  enqueue(q, 1);

  printf("Init queue\n");
  print_queue(q);

  printf("Dequeue once\n");
  dequeue(q);
  print_queue(q);

  printf("Dequeue twice\n");
  dequeue(q);
  print_queue(q);

  printf("Dequeue thrice\n");
  int final_val = dequeue(q);
  printf("Final val dequeue: %d\n", final_val);
  print_queue(q);

  printf("Dequeue on empty queue attempts...\n");
  dequeue(q);
  dequeue(q);

  return 0;
}
