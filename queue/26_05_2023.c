#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct Queue {
	int* items;
	int front;
	int back;
	int size;
	int capacity;
} queue;

void enqueue(queue* q, int val) {
	if (q->size == q->capacity)
		return;

	q->back = (q->back + 1) % q->capacity;
	q->items[q->back] = val;
	q->size++;
}


int dequeue(queue* q) {
	if (q->size == 0)
		return;

	int val = q->items[q->front];
	q->front = (q->front + 1) % q->capacity;
	q->size--;
	return val;
}


struct Queue* create(int capacity) {
	queue* q = malloc(sizeof(queue));
	q->items = malloc(sizeof(int) * capacity);
	q->front = q->size = 0;
	q->capacity = capacity;
	q->back = q->capacity - 1;
	return q;
}


void print();
void free_queue(queue* q) {
	free(q->items);
	free(q);
}
int front(queue* q) {
	return q->items[q->front];
}
int back(queue *q) {
	return q->items[q->back];
}
