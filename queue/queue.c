#include <stdio.h>
#include <stdlib.h>

typedef struct queue {
  int current_index;
  int size;
  int * data;
} queue;

void enqueue(queue * items, int item);
int dequeue(queue * items);
void print_queue(queue * items);

int main() {
  int test_data[4] = {1};

  struct queue * items = malloc(sizeof(queue));
  items->size = 4;
  items->current_index = 0;
  items->data = &test_data[0];

  print_queue(items);

  // Remove
  dequeue(items);

  printf("Dequeue\n");
  print_queue(items);

  printf("Dequeue to the end\n");
  dequeue(items);
  print_queue(items);

  // Add
  enqueue(items, 7);
  enqueue(items, 8);

  printf("Enqueue\n");
  print_queue(items);

  dequeue(items);
  dequeue(items);

  printf("Dequeue to the end\n");
  dequeue(items);
  print_queue(items);

  free(items);

  return 0;
}

void enqueue(queue * items, int item) {
  if(items->current_index == items->size - 1) {
	printf("Queue is full\n");
	return;
  }

  items->current_index++;
  items->data[items->current_index] = item;
}

int dequeue(queue * items) {
  if(items->current_index == -1) {
	printf("Stack is empty\n");
	return -1;
  }

  int val = items->data[0];

  // Re-arrange the array
  for (int i = 0; i < items->current_index; i++) {
	items->data[i] = items->data[i+1];
  }

  items->current_index--;

  return val;
}

void print_queue(queue *items) {
  printf("Queue: ");
  for (int i = 0; i <= items->current_index; i++) {
	printf("%d ", items->data[i]);
  }
  printf("\n");
}
