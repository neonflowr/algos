#include <stdio.h>
#include <stdlib.h>

typedef struct Queue {
  int *items;
  int front;
  int back;
  unsigned int capacity;
  int size;
} Queue;

Queue *create(unsigned int capacity) {
  Queue *q = malloc(sizeof(Queue));
  q->items = malloc(capacity * sizeof(int));
  q->capacity = capacity;
  q->size = 0;
  q->front = 0;
  q->back = q->capacity - 1;
  return q;
}

int is_full(Queue *q) { return q->size == q->capacity; }

int is_empty(Queue *q) { return q->size == 0; }

void enqueue(Queue *q, int value) {
  if (is_full(q))
    return;
  q->back = (q->back + 1) % q->capacity;
  q->items[q->back] = value;
  q->size++;
}

int dequeue(Queue *q) {
  if (is_empty(q))
    return -1;

  int val = q->items[q->front];
  q->front = (q->front + 1) % q->capacity;
  q->size--;
  return val;
}

void print(Queue *q) {
  if (is_empty(q)) {
    printf("Empty queue\n");
    return;
  }
  int i = q->front;
  while (i != q->back) {
    printf("%d ", q->items[i]);
	i = (i + 1) % q->capacity;
  }
  printf("%d\n", q->items[i]);
}
