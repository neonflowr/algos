#include <stdio.h>
#include <stdlib.h>

struct queue {
  int *items;
  int front;
  int back;
  int size;
  unsigned capacity;
};

struct queue *create_queue(int capacity) {
  int *items = malloc(capacity * sizeof(int));
  struct queue *q = malloc(sizeof(struct queue));
  q->items = &items[0];
  q->front = 0;
  q->size = 0;

  // IMPORTANT: This is so the queue can be circular
  q->back = capacity - 1;

  q->capacity = capacity;
  return q;
}

void enqueue(struct queue *q, int value) {
  if (q->size == q->capacity) {
    printf("Full queue\n");
    return;
  }

  q->back = (q->back + 1) % q->capacity;
  q->items[q->back] = value;
  q->size++;
}

int dequeue(struct queue *q) {
  if (q->size == 0) {
    printf("Queue is empty\n");
    return -1;
  }
  int val = q->items[q->front];
  q->front = (q->front + 1) % q->capacity;
  q->size--;
  return val;
}

void print_queue(struct queue *q) {
  printf("Queue: ");
  if (q->size == 0) {
    printf("Empty struct queue\n");
    return;
  }

  int i = q->front;
  while (i != q->back) {
    printf("%d -> ", q->items[i]);
    i = (i + 1) % q->capacity;
  }
  printf("%d", q->items[q->back]);
  printf("\n");
}

int main() {
  struct queue *q = create_queue(4);

  enqueue(q, 5);
  enqueue(q, 3);
  enqueue(q, 4);
  enqueue(q, 1);

  print_queue(q);

  dequeue(q);
  dequeue(q);

  print_queue(q);

  return 0;
}
