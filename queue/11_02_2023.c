#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct Queue  {
  int *items;
  int front;
  int back;
  int size;
  unsigned int capacity;
} queue;

queue *create(unsigned int capacity) {
  queue *q = malloc(sizeof(queue));
  q->capacity = capacity;
  q->items = malloc(q->capacity * sizeof(int));
  q->front = 0;
  q->size = 0;
  q->back = q->capacity - 1;
  return q;
}

void free_queue(queue **q) {
  free((*q)->items);
  (*q)->items = NULL;
  free(*q);
  *q = NULL;
}

int is_empty(queue *q) {
  return q->size == 0;
}

int is_full(queue *q) {
  return q->size == q->capacity;
}

void enqueue(queue *q, int value) {
  if (is_full(q)) return;

  q->back = (q->back + 1) % q->capacity;
  q->items[q->back] = value;
  q->size++;
}

int dequeue(queue *q) {
  if (is_empty(q)) return -1;

  int val = q->items[q->front];
  q->front = (q->front + 1) % q->capacity;
  q->size--;
  return val;
}

void print(queue *q) {
  int i = q->front;
  for(int j = 0; j < q->size; j++) {
    printf("%d ", q->items[i]);
    i = (i + 1) % q->capacity;
  }
  printf("\n");
}
