#include <stdio.h>
#include <stdlib.h>


typedef struct list {
  int data;
  struct list* next;
} list;

typedef struct queue {
  list* front;
  list* back;
} queue;

void enqueue(queue* q, int value) {
  list* node = malloc(sizeof(list));
  node->data = value;
  if (q->back == NULL) {
	q->front = node;
	q->back = node;
	return;
  }

  q->back->next = node;
  q->back = node;
}

int dequeue(queue *q) {
  if (q->front == NULL) {
	printf("Can't dequeue empty queue\n");
	return -1;
  }

  list* tmp = q->front;
  q->front = q->front->next;

  if (q->front == NULL)
	q->back = NULL;

  int val = tmp->data;
  free(tmp);

  return val;
}

void print_queue(queue *q) {
  if (q->back == NULL) {
	printf("Empty\n");
	return;
  }
  list *node = q->front;
  while(node != NULL) {
	if (node->next != NULL)
	  printf("%d -> ", node->data);
	else
	  printf("%d", node->data);
	node = node->next;
  }
  printf("\n");
}

int main() {
  printf("Queue linked list version 2\n");

  printf("Initial queue: ");
  queue *q = malloc(sizeof(queue));
  print_queue(q);

  printf("Init queue: \n");
  enqueue(q, 1);
  enqueue(q, 2);
  enqueue(q, 3);
  print_queue(q);

  printf("Dequeue twice: \n");
  dequeue(q);
  dequeue(q);
  printf("Front: %d\n", q->front->data);
  printf("Back: %d\n", q->back->data);
  print_queue(q);

  printf("Final dequeue\n");
  dequeue(q);
  print_queue(q);

  printf("Attempt to dequeue\n");
  dequeue(q);

  printf("Init queue: \n");
  enqueue(q, 1);
  enqueue(q, 2);
  enqueue(q, 3);
  print_queue(q);

  return 0;
}
