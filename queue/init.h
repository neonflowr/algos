struct Queue;

void enqueue();
int dequeue();
struct Queue *create();
void print();
void free_queue();
int front();
int back();
