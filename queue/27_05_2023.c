#include <stdio.h>
#include <stdlib.h>
#include "init.h"

typedef struct List {
	int data;
	struct List* next;
} list;

void freel(list* l) {
	list* tmp;
	while (l) {
		tmp = l->next;
		free(l);
		l = tmp;
	}
}

typedef struct Queue {
	list* front;
	list* back;
	int size;
} queue;

void enqueue(queue* q, int value) {
	list* node = malloc(sizeof(list));
	node->data = value;
	node->next = NULL;

	if (!q->front && !q->back) {
		q->front = q->back = node;
	}
	else {
		q->back->next = node;
		q->back = node;
	}
}
int dequeue(queue* q) {
	if (!q->front)
		return 0;


	list* tmp = q->front;
	q->front = q->front->next;
	int val = tmp->data;
	free(tmp);
	return val;
}

struct Queue* create() {
	queue* q = malloc(sizeof(queue));
	q->front = q->back = NULL;
	q->size = 0;
	return q;
}

int front(queue* q) {
	return q->front ? q->front->data : 0;
}

int back(queue* q) {
	return q->back ? q->back->data : 0;
}

void free_queue(queue* q) {
	freel(q->front);
	q->front = NULL;
	q->back = NULL;
	free(q);
}

void print(queue* q) {
	list* l = q->front;
	while (l) {
		printf("%d ", l->data);
		l = l->next;
	}
	printf("\n");
}

