#include <stdio.h>
#include <stdlib.h>

struct List {
  int data;
  struct List *next;
};

struct Queue {
  struct List *front;
  struct List *back;
};

void enqueue(struct Queue *q, int value) {
  struct List *node = malloc(sizeof(struct List));
  node->data = value;
  node->next = NULL;

  printf("Enqueue: %d\n", value);

  if (q->front == NULL) {
	q->front = q->back = node;
	return;
  }

  q->back->next = node;
  q->back = node;
}

int dequeue(struct Queue *q) {
  if (q->front == NULL) {
	printf("can't dequeue empty queue\n");
	return -1;
  }

  struct List *tmp = q->front;
  q->front = q->front->next;

  int val = tmp->data;

  free(tmp);
  tmp = NULL;

  printf("Dequeue: %d\n", val);

  return val;
}

void print_queue(struct Queue *q) {
  if (q->front == NULL) {
	printf("empty queue\n");
	return;
  }

  struct List *node = q->front;
  printf("Queue: ");
  while(node != NULL) {
	printf("%d ", node->data);
	node = node->next;
  }
  printf("\n");
}

int main() {
  struct Queue *q = malloc(sizeof(struct Queue));
  enqueue(q, 1);
  enqueue(q, 2);
  enqueue(q, 3);
  enqueue(q, 4);
  print_queue(q);

  dequeue(q);
  dequeue(q);

  print_queue(q);

  enqueue(q, 1);
  enqueue(q, 2);

  print_queue(q);

  dequeue(q);
  print_queue(q);

  enqueue(q, 3);
  print_queue(q);

  dequeue(q);
  dequeue(q);
  dequeue(q);
  dequeue(q);
  print_queue(q);

  dequeue(q);
  dequeue(q);

  print_queue(q);

  enqueue(q, 1);
  enqueue(q, 2);
  enqueue(q, 3);
  enqueue(q, 4);
  print_queue(q);

  return 0;
}
