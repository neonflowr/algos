#include "init.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct List {
  int data;
  struct List *next;
} list;

void insert(list **l, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;
  node->next = *l;
  *l = node;
}

typedef struct Queue {
  list *front;
  list *back;
} queue;

queue *create() {
  queue *q = malloc(sizeof(queue));
  q->front = NULL;
  q->back = NULL;
  return q;
}


void enqueue(queue *q, int value) {
  list *node = malloc(sizeof(list));
  node->data = value;

  if (q->front == NULL) {
	q->front = q->back = node;
	node->next = NULL;
  } else {
	q->back->next = node;
	q->back = node;
  }
}

int dequeue(queue *q) {
  if (q->front == NULL) return -1;

  list *tmp = q->front;
  q->front = q->front->next;
  int val = tmp->data;
  free(tmp);
  tmp = NULL;
  return val;
}

void print(queue *q) {
  list *tmp = q->front;
  while(tmp != NULL) {
	printf("%d ", tmp->data);
	tmp = tmp->next;
  }
  printf("\n");
}

void free_queue(queue **q) {
  free(*q);
  *q = NULL;
}

int front(queue *q) {
  if (q->front == NULL) return -1;
  return q->front->data;
}

int back(queue *q) {
  if (q->back == NULL)
    return -1;
  return q->back->data;
}
