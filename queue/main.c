#include "21_02_2023.c"
#include "init.h"
#include <stdio.h>
#include <assert.h>

int main() {
  printf("キューをテスト中\n");
  struct Queue *q = create(10);

  enqueue(q, 1);
  enqueue(q, 3);
  enqueue(q, 4);
  enqueue(q, 9);

  printf("Testing\n");
  print(q);

  assert(front(q) == 1);
  assert(back(q) == 9);

  printf("Dequeue twice\n");
  dequeue(q);
  dequeue(q);
  print(q);

  assert(front(q) == 4);
  assert(back(q) == 9);

  printf("Dequeue to empty\n");
  dequeue(q);
  dequeue(q);
  dequeue(q);
  dequeue(q);

  print(q);

  printf("Re-init\n");
  enqueue(q, 1);
  enqueue(q, 3);
  enqueue(q, 4);
  enqueue(q, 9);
  print(q);

  assert(front(q) == 1);
  assert(back(q) == 9);

  free_queue(&q);

  assert(q == NULL);

  return 0;
}
